# Courier

This is the node service responsible for:

- scraping/crawling content sources, queueing up their indexing as tasks in a queue, and inserting into the database and search engine

## Develop

Begin development server on localhost:

```sh
yarn install && yarn dev
```

### Testing

Testing is powered by [Jest](https://facebook.github.io/jest/). This project also uses [supertest](https://github.com/visionmedia/supertest) for routing smoke testing.

Start Jest in watch mode

```sh
yarn test-watch
```

Or run a single time with:

```sh
yarn test
```

### Linting

```sh
yarn lint
```

### Deployment and Release

Deployment is specific to hosting platform/provider but generally to compile your `/src` to `/dist`:

```sh
yarn compile
```

Then from your hosting provider, to serve just run:

```sh
yarn start
```

import { getDownloadSet, uploadFromLocal, transformText } from '../src/helpers';

describe('getDownloadSet', () => {
  test('should be defined and a function', () => {
    expect(getDownloadSet).toBeDefined();
    expect(typeof getDownloadSet).toEqual('function');
  });


test.each([
    { p1: null, p2: 'string', p3: 'string' },
    { p1: 'string', p2: 'string', p3: 'string' },
    { p1: undefined , p2: '', p3: 'string' },
    { p1: 1, p2: '', p3: 'string' },
    { p1: true, p2: '', p3: 'string' },
    { p1: new Date(), p2: '', p3: 'string' },
    { p1: [], p2: '', p3: 'string' },
    { p1: {}, p2: null, p3: 'string' },
    { p1: {}, p2: {}, p3: 'string' },
    { p1: {}, p2: undefined, p3: 'string' },
    { p1: {}, p2: 1, p3: 'string' },
    { p1: {}, p2: true, p3: 'string' },
    { p1: {}, p2: new Date(), p3: 'string' },
    { p1: {}, p2: [], p3: 'string' },
    { p1: {}, p2: 'string', p3: null },
    { p1: {}, p2: 'string', p3: {} },
    { p1: {}, p2: 'string', p3: undefined },
    { p1: {}, p2: 'string', p3: 1 },
    { p1: {}, p2: 'string', p3: true },
    { p1: {}, p2: 'string', p3: new Date() },
    { p1: {}, p2: 'string', p3: [] }
  ])(
    'should require first param to be an object',
    async (param) => {
    expect.assertions(1);
    try {
      await getDownloadSet(param.p1, param.p2, param.p3);
      throw new Error('should have thrown in this test but did not... fix it.');
    } catch(error) {
      expect(error).toBeDefined();
    }
  });

  
  test('should not throw when params are correct type', async () => {
    expect.assertions(1);
    const databaseStub = {
      listAllFiles: async () => {
        return await [{ name: 'docOne' }, { name: 'docTwo' }, { name: 'docThree'}];
      },
      downloadFile: async () => {
        return await {};
      }
    };
    try {
      const test = await getDownloadSet(databaseStub, 'docThree', 'someString');
      expect(test).toBeDefined();
    } catch(error) {
      throw new Error('should not have thrown, but did.');
    }
  });

  test('should call downloadFile for each number relevant files per pdf', async () => {
    expect.assertions(2);
    const expectedHitOutput = { 
      testProp: 'testVal'
    };
    const databaseStub = {
      listAllFiles: async () => {
        return await [{
          name: 'docOne'
        },
        {
          name: 'docTwo'
        },
        {
          name: 'docThree'
        }]
      },
      downloadFile: async () => {
        return await expectedHitOutput;
      }
    };
    const correlationId = 'docThree';
    const fileName = 'TestName.pdf';
    try {
      const result = await getDownloadSet(databaseStub, correlationId, fileName);
      expect(result.length).toEqual(1);
      expect(result[0]).toEqual(expectedHitOutput);
    } catch(error) {
      throw new Error(error);
    }
  });

  test('should call throw parser not found error if downloadFile finds no hits', async () => {
    expect.assertions(1);
    const expectedHitOutput = { 
      testProp: 'testVal'
    };
    const databaseStub = {
      listAllFiles: async () => {
        return await [{
          name: 'docOne'
        },
        {
          name: 'docTwo'
        },
        {
          name: 'docThree'
        }]
      },
      downloadFile: async () => {
        return await expectedHitOutput;
      }
    };
    const correlationId = 'SomeIDNotPresent';
    const fileName = 'TestName.pdf';
    try {
      await getDownloadSet(databaseStub, correlationId, fileName);
      throw new Error('should have thrown in this test but did not!');
    } catch(error) {
      expect(error.message).toEqual('dw-courier: Parsed output for TestName.pdf could not be found!');
    }
  });

});

describe('uploadFromLocal', async () => {
  
  test('should exist', () => {
    expect(uploadFromLocal).toBeDefined();
    expect(typeof uploadFromLocal).toEqual('function');
  });

  test.each([
    { p1: null, p2: '', p3: {} },
    { p1: 'string', p2: '', p3: {} },
    { p1: undefined , p2: '', p3: {} },
    { p1: 1, p2: '', p3: {} },
    { p1: true, p2: '', p3: {} },
    { p1: new Date(), p2: '', p3: {} },
    { p1: [], p2: '', p3: {} },
    { p1: {}, p2: null, p3: {} },
    { p1: {}, p2: {}, p3: {} },
    { p1: {}, p2: undefined, p3: {} },
    { p1: {}, p2: 1, p3: {} },
    { p1: {}, p2: true, p3: {} },
    { p1: {}, p2: new Date(), p3: {} },
    { p1: {}, p2: [], p3: {} },
    { p1: {}, p2: 'string', p3: null },
    { p1: {}, p2: 'string', p3: 'string' },
    { p1: {}, p2: 'string', p3: undefined },
    { p1: {}, p2: 'string', p3: 1 },
    { p1: {}, p2: 'string', p3: true },
    { p1: {}, p2: 'string', p3: new Date() },
    { p1: {}, p2: 'string', p3: [] }
  ])(
    'should require first param to be an object, second string, third json',
    async (param) => {
    expect.assertions(1);
    try {
      await uploadFromLocal(param.p1, param.p2, param.p3);
      throw new Error('should have thrown in this test but did not... fix it.');
    } catch(error) {
      expect(error).toBeDefined();
    }
  });

  test('should not throw when params are correct type', async () => {
    expect.assertions(2);
    const uploadCallMock = jest.fn().mockResolvedValue({});
    const databaseStub = {
      listAllFiles: async () => {
        return await [{ name: 'docOne' }, { name: 'docTwo' }, { name: 'docThree'}];
      },
      uploadLocalFile: uploadCallMock
    };
    try {
      const invoke = async () => {
        await uploadFromLocal(databaseStub, 'docThree', {});
      }
      expect(invoke).not.toThrow();
      expect(uploadCallMock).toHaveBeenCalled();
    } catch(error) {
      throw new Error('should not have thrown, but did.');
    }
  });

});

describe('transformText', () => {
  
  test('should exist', () => {
    expect(transformText).toBeDefined();
    expect(typeof transformText).toEqual('function');
  });

  test.each([
    { p1: null, p2: [] },
    { p1: undefined, p2: [] },
    { p1: 1, p2: [] },
    { p1: true, p2: [] },
    { p1: [], p2: [] },
    { p1: new Date(), p2: [] },
    { p1: {}, p2: [] },
    { p1: 'string', p2: null },
    { p1: 'string', p2: undefined },
    { p1: 'string', p2: 1 },
    { p1: 'string', p2: true },
    { p1: 'string', p2: new Date() },
    { p1: 'string', p2: 'string' },
    { p1: 'string', p2: {} }
  ])(
    'should require correct param type',
    async (param) => {
    expect.assertions(1);
    try {
      await transformText(param.p1, param.p2);
      throw new Error('should have thrown in this test but did not... fix it.');
    } catch(error) {
      expect(error).toBeDefined();
    }
  });

   test('should not throw when params are correct type', async () => {
    expect.assertions(2);
    const fileNameMock = '1234';
    const filesArrayMock = [
      JSON.stringify({
        responses: [
          {
            fullTextAnnotation: {
              text: 'some text here'
            }
          }
        ]
      })
    ];
    try {
      const result = await transformText(fileNameMock, filesArrayMock);
      expect(result.name).toEqual('1234');
      expect(result.text).toContain('some text here');
    } catch(error) {
      throw new Error('should not have thrown, but did.');
    }
  });
});
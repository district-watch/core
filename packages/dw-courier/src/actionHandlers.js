import { getDownloadSet, uploadFromLocal, transformText } from './helpers';
import { Search } from 'dw-search';
import { Parser } from 'dw-parser';
import { Logger } from 'dw-logger';
const parser = new Parser();
const search = new Search();
const logger = new Logger();

async function init() {
  try {
    await parser.initialize();
    await search.initialize();
  } catch(error) {
    logger.error(error);
  }
}

export const handleDownload = async (database, queue, message) => {
  let { publicUrl, fileName } = message.body;
  let { correlationId } = message.properties;

  logger.log('debug', `uploading ${fileName} to raw bucket.`);
  try {
    await database.uploadFile(publicUrl, `${correlationId}.pdf`, 'raw');
    logger.log('success', `uploaded ${fileName}!`);
    await queue.publish('district-watch-parse-exchange', { fileName, sourceBucketUrl: 'gs://district-watch-dev-raw-docs', destBucketUrl: 'gs://district-watch-dev-json-docs', mimeType: 'application/pdf' }, correlationId);
  } catch(error) {
    logger.error(error);
  }
};

export const handleParse = async (database, queue, message) => {
  let { fileName, sourceBucketUrl, destBucketUrl, mimeType } = message.body;
  let { correlationId } = message.properties;
  let fullSourceUrl = `${sourceBucketUrl}/${correlationId}.pdf`;
  let fullDestUrl = `${destBucketUrl}/${correlationId}.pdf`;
  
  logger.log('debug', `parsing ${fileName}.`);
  try {
    await parser.parse(fullSourceUrl, fullDestUrl, mimeType);
    logger.log('success', `parsed ${fileName}!`);
    return await queue.publish('district-watch-assemble-exchange', message.body, correlationId);
  } catch(error) {
    logger.error(error);
  }
};

export const handleAssemble = async (database, queue, message) => {
  let { fileName } = message.body;
  let { correlationId } = message.properties;

  try {
    const filesArray = await getDownloadSet(database, correlationId, fileName);
    logger.log('debug', `assembling ${filesArray.length} json files into final file.`);
    const finalJson = transformText(fileName, filesArray);
    await uploadFromLocal(database, correlationId, finalJson);
    logger.log('success', `assembled ${fileName} into final text file!`);
    return await queue.publish('district-watch-index-exchange', message.body, correlationId);
  } catch(error) {
    logger.error(error);
  }
};

export const handleIndex = async (database, message) => {
  let { fileName } = message.body;
  let { correlationId } = message.properties;
  
  logger.log('debug', `indexing ${fileName}.`);
  try {
    const file = await database.downloadFile(`${correlationId}.json`, 'assembled')
    await search.ensureIndexExists('publicdocs');
    return await search.index('publicdocs', correlationId, 'original', JSON.parse(file));
  } catch(error) {
    logger.error(error);
  }
}

init();

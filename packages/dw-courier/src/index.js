import _ from './env'; // eslint-disable-line no-unused-vars
// import uuidv4 from 'uuid/v4';
import connectDatabase from './db';
import {
  handleDownload,
  handleParse,
  handleAssemble,
  handleIndex
} from './actionHandlers';
import {
  Queue
} from 'dw-queue';
import {
  Logger
} from 'dw-logger';
import {
  Utils
} from 'dw-utils';
const utils = new Utils();
const queue = new Queue();
const logger = new Logger();

async function init() {
  try {
    const db = await connectDatabase();
    await queue.initializeConnection();
    setupQueueSubscriptions(db);
    // await testPostMessage();
  } catch (error) {
    logger.error(error);
  }
}

function setupQueueSubscriptions(db) {
  queue.subscribe('dw-retrieve-tasks-queue', (msg) => {
    logger.log('debug', `message for ${msg.body.fileName} received from retrieve queue.`);
    (async () => {
      try {
        await createDocumentStatus(db, msg.properties.correlationId, {
          id: msg.properties.correlationId,
          fileName: msg.body.fileName,
          publicUrl: msg.body.publicUrl,
          status: 'downloading',
          phase: 'retrieve'
        });
        await handleDownload(db, queue, msg);
        await logger.log('debug', `publishing ${msg.body.fileName} message to the parse exchange.`);
        await msg.ack();
        await updateDocumentStatus(db, msg.properties.correlationId, {
          status: 'uploaded',
          phase: 'retrieve'
        });
      } catch (error) {
        msg.nack();
        logger.error(error);
        await updateDocumentStatus(db, msg.properties.correlationId, {
          status: 'error retrieving',
          error
        });
      }
    })();
  });

  queue.subscribe('dw-parse-tasks-queue', (msg) => {
    logger.log('debug', `message for ${msg.body.fileName} received from parse queue.`);
    (async () => {
      try {
        await updateDocumentStatus(db, msg.properties.correlationId, {
          status: 'parsing',
          phase: 'parse'
        });
        await handleParse(db, queue, msg);
        await logger.log('debug', `publishing ${msg.body.fileName} message to the assemble exchange.`);
        await msg.ack();
        await updateDocumentStatus(db, msg.properties.correlationId, {
          status: 'parsed',
          phase: 'parse'
        });
      } catch (error) {
        msg.nack();
        logger.error(error);
        await updateDocumentStatus(db, msg.properties.correlationId, {
          status: 'error parsing',
          error
        });
      }
    })();
  });

  queue.subscribe('dw-assemble-tasks-queue', (msg) => {
    logger.log('debug', `message for ${msg.body.fileName} received from assemble queue.`);
    (async () => {
      try {
        await updateDocumentStatus(db, msg.properties.correlationId, {
          status: 'assembling',
          phase: 'assemble'
        });
        await handleAssemble(db, queue, msg);
        await logger.log('debug', `publishing ${msg.body.fileName} message to the index exchange.`);
        await msg.ack();
        await updateDocumentStatus(db, msg.properties.correlationId, {
          status: 'assembled',
          phase: 'assemble'
        });
      } catch (error) {
        msg.nack();
        logger.error(error);
        await updateDocumentStatus(db, msg.properties.correlationId, {
          status: 'error assembling',
          error
        });
      }
    })();
  });

  queue.subscribe('dw-index-tasks-queue', (msg) => {
    logger.log('debug', `message for ${msg.body.fileName} received from index queue.`);
    (async () => {
      try {
        await updateDocumentStatus(db, msg.properties.correlationId, {
          status: 'indexing',
          phase: 'index'
        });
        await handleIndex(db, msg);
        await logger.log('success', `indexed ${msg.body.fileName}!`);
        await msg.ack();
        await updateDocumentStatus(db, msg.properties.correlationId, {
          status: 'complete',
          phase: 'complete'
        });
      } catch (error) {
        msg.nack();
        logger.error(error);
        await updateDocumentStatus(db, msg.properties.correlationId, {
          status: 'error indexing',
          error
        });
      }
    })();
  });
}

// async function testPostMessage() {
//   return await queue.publish('district-watch-retrieve-exchange', { publicUrl: 'http://www.africau.edu/images/default/sample.pdf', fileName: 'sample.pdf' }, uuidv4());
// }

async function updateDocumentStatus(database, docId, data) {
  utils.validateString('dw-courier', docId, 'updateDocumentStatus', 'docId');
  utils.validateObject('dw-courier', data, 'updateDocumentStatus', 'data');
  return await database.updateOne('documents', data, docId);
}

async function createDocumentStatus(database, docId, data) {
  utils.validateString('dw-courier', docId, 'updateDocumentStatus', 'docId');
  utils.validateObject('dw-courier', data, 'updateDocumentStatus', 'data');
  return await database.createOne('documents', data, docId);
}

init();
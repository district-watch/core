import {
  DatabaseWrapper
} from 'dw-data';
import {
  Utils
} from 'dw-utils';
const utils = new Utils();

const connectDatabase = () => {
  const db = new DatabaseWrapper(utils.getEnvironment(process.env.NODE_ENV));
  return db.init().then(() => {
    return db;
  }).catch((error) => {
    console.error('Failed to establish connection to District Watch database.'); // eslint-disable-line no-console
    console.log(error); // eslint-disable-line no-console
  });
};

export default connectDatabase;
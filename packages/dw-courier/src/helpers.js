import * as fs from 'fs';
import path from 'path';
import { Utils } from 'dw-utils';
const utils = new Utils();

export const getDownloadSet = async (database, correlationId, fileName) => {
  utils.validateObject('dw-courier', database, 'getDownloadSet', 'database');
  utils.validateString('dw-courier', correlationId, 'getDownloadSet', 'correlationId');
  utils.validateString('dw-courier', fileName, 'getDownloadSet', 'fileName');
  let downloadSet = [];
  const files = await database.listAllFiles('json');
  const relevantFiles = files.filter(file => file.name.indexOf(correlationId) > -1);
  if (relevantFiles.length > 0) {
    relevantFiles.forEach(file => {
      downloadSet.push(database.downloadFile(file.name, 'json'));
    });
  } else {
    throw new Error(`dw-courier: Parsed output for ${fileName} could not be found!`);
  }
  
  return Promise.all(downloadSet);
}

export const uploadFromLocal = async (database, correlationId, finalJson) => {
  utils.validateObject('dw-courier', database, 'uploadFromLocal', 'database');
  utils.validateString('dw-courier', correlationId, 'uploadFromLocal', 'correlationId');
  utils.validateObject('dw-courier', finalJson, 'uploadFromLocal', 'finalJson');
  fs.writeFileSync(path.resolve(__dirname, `${correlationId}.json`), JSON.stringify(finalJson));
  await database.uploadLocalFile(path.resolve(__dirname, `${correlationId}.json`), `${correlationId}.json`, 'assembled');
  return fs.unlinkSync(path.resolve(__dirname, `${correlationId}.json`));
}

export const transformText = (fileName, filesArray) => {
  utils.validateString('dw-courier', fileName, 'transformText', 'fileName');
  utils.validateArray('dw-courier', filesArray, 'transformText', 'filesArray');
  const jsonOut = filesArray.reduce((fileAccumulator,file) => {
    const jsonFile = JSON.parse(file);
    const pagesTextContent = jsonFile.responses.reduce((pageAccumulator, page) => pageAccumulator + '\n' + page.fullTextAnnotation.text, '');
    return fileAccumulator + '\n' + pagesTextContent;
  }, '');

  return {
    name: fileName,
    text: jsonOut
  };
}
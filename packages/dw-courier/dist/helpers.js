'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.transformText = exports.uploadFromLocal = exports.getDownloadSet = undefined;

var _fs = require('fs');

var fs = _interopRequireWildcard(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _dwUtils = require('dw-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

const utils = new _dwUtils.Utils();

const getDownloadSet = exports.getDownloadSet = async (database, correlationId, fileName) => {
  utils.validateObject('dw-courier', database, 'getDownloadSet', 'database');
  utils.validateString('dw-courier', correlationId, 'getDownloadSet', 'correlationId');
  utils.validateString('dw-courier', fileName, 'getDownloadSet', 'fileName');
  let downloadSet = [];
  const files = await database.listAllFiles('json');
  const relevantFiles = files.filter(file => file.name.indexOf(correlationId) > -1);
  if (relevantFiles.length > 0) {
    relevantFiles.forEach(file => {
      downloadSet.push(database.downloadFile(file.name, 'json'));
    });
  } else {
    throw new Error(`dw-courier: Parsed output for ${fileName} could not be found!`);
  }

  return Promise.all(downloadSet);
};

const uploadFromLocal = exports.uploadFromLocal = async (database, correlationId, finalJson) => {
  utils.validateObject('dw-courier', database, 'uploadFromLocal', 'database');
  utils.validateString('dw-courier', correlationId, 'uploadFromLocal', 'correlationId');
  utils.validateObject('dw-courier', finalJson, 'uploadFromLocal', 'finalJson');
  fs.writeFileSync(_path2.default.resolve(__dirname, `${correlationId}.json`), JSON.stringify(finalJson));
  await database.uploadLocalFile(_path2.default.resolve(__dirname, `${correlationId}.json`), `${correlationId}.json`, 'assembled');
  return fs.unlinkSync(_path2.default.resolve(__dirname, `${correlationId}.json`));
};

const transformText = exports.transformText = (fileName, filesArray) => {
  utils.validateString('dw-courier', fileName, 'transformText', 'fileName');
  utils.validateArray('dw-courier', filesArray, 'transformText', 'filesArray');
  const jsonOut = filesArray.reduce((fileAccumulator, file) => {
    const jsonFile = JSON.parse(file);
    const pagesTextContent = jsonFile.responses.reduce((pageAccumulator, page) => pageAccumulator + '\n' + page.fullTextAnnotation.text, '');
    return fileAccumulator + '\n' + pagesTextContent;
  }, '');

  return {
    name: fileName,
    text: jsonOut
  };
};
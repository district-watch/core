'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.handleIndex = exports.handleAssemble = exports.handleParse = exports.handleDownload = undefined;

var _helpers = require('./helpers');

var _dwSearch = require('dw-search');

var _dwParser = require('dw-parser');

var _dwLogger = require('dw-logger');

const parser = new _dwParser.Parser();
const search = new _dwSearch.Search();
const logger = new _dwLogger.Logger();

async function init() {
  try {
    await parser.initialize();
    await search.initialize();
  } catch (error) {
    logger.error(error);
  }
}

const handleDownload = exports.handleDownload = async (database, queue, message) => {
  let { publicUrl, fileName } = message.body;
  let { correlationId } = message.properties;

  logger.log('debug', `uploading ${fileName} to raw bucket.`);
  try {
    await database.uploadFile(publicUrl, `${correlationId}.pdf`, 'raw');
    logger.log('success', `uploaded ${fileName}!`);
    await queue.publish('district-watch-parse-exchange', { fileName, sourceBucketUrl: 'gs://district-watch-dev-raw-docs', destBucketUrl: 'gs://district-watch-dev-json-docs', mimeType: 'application/pdf' }, correlationId);
  } catch (error) {
    logger.error(error);
  }
};

const handleParse = exports.handleParse = async (database, queue, message) => {
  let { fileName, sourceBucketUrl, destBucketUrl, mimeType } = message.body;
  let { correlationId } = message.properties;
  let fullSourceUrl = `${sourceBucketUrl}/${correlationId}.pdf`;
  let fullDestUrl = `${destBucketUrl}/${correlationId}.pdf`;

  logger.log('debug', `parsing ${fileName}.`);
  try {
    await parser.parse(fullSourceUrl, fullDestUrl, mimeType);
    logger.log('success', `parsed ${fileName}!`);
    return await queue.publish('district-watch-assemble-exchange', message.body, correlationId);
  } catch (error) {
    logger.error(error);
  }
};

const handleAssemble = exports.handleAssemble = async (database, queue, message) => {
  let { fileName } = message.body;
  let { correlationId } = message.properties;

  try {
    const filesArray = await (0, _helpers.getDownloadSet)(database, correlationId, fileName);
    logger.log('debug', `assembling ${filesArray.length} json files into final file.`);
    const finalJson = (0, _helpers.transformText)(fileName, filesArray);
    await (0, _helpers.uploadFromLocal)(database, correlationId, finalJson);
    logger.log('success', `assembled ${fileName} into final text file!`);
    return await queue.publish('district-watch-index-exchange', message.body, correlationId);
  } catch (error) {
    logger.error(error);
  }
};

const handleIndex = exports.handleIndex = async (database, message) => {
  let { fileName } = message.body;
  let { correlationId } = message.properties;

  logger.log('debug', `indexing ${fileName}.`);
  try {
    const file = await database.downloadFile(`${correlationId}.json`, 'assembled');
    await search.ensureIndexExists('publicdocs');
    return await search.index('publicdocs', correlationId, 'original', JSON.parse(file));
  } catch (error) {
    logger.error(error);
  }
};

init();
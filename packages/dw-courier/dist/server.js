'use strict';

const favicon = require('serve-favicon');
const express = require('express');
const path = require('path');
const courier = require('./index'); //eslint-disable-line no-unused-vars
const app = express();
const port = process.env.PORT || 3000;

app.use(favicon(path.join(__dirname, '..', 'public', 'images', 'favicon.ico')));

app.get('/', (request, response) => {
  response.send(`<!DOCTYPE html>
    <html>
    <head>
      <meta charset="UTF-8">
      <title>District Watch Courier</title>
    </head>
    <body>
      <div style="text-align:center; margin-top:40px;">
        <p>The Courier Service is online!</p>
      <div>
      </body>
    </html>`);
});

app.get('/heartbeat', (request, response) => {
  response.status(200).end();
});

app.listen(port, err => {
  if (err) {
    return console.log('something bad happened', err); //eslint-disable-line no-console
  }

  console.log(`server is listening on ${port}`); //eslint-disable-line no-console
});
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _dwData = require('dw-data');

var _dwUtils = require('dw-utils');

const utils = new _dwUtils.Utils();

const connectDatabase = () => {
  const db = new _dwData.DatabaseWrapper(utils.getEnvironment(process.env.NODE_ENV));
  return db.init().then(() => {
    return db;
  }).catch(error => {
    console.error('Failed to establish connection to District Watch database.'); // eslint-disable-line no-console
    console.log(error); // eslint-disable-line no-console
  });
};

exports.default = connectDatabase;
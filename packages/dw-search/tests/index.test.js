import { Search } from "../src/index";
const sinon = require('sinon');
let sandbox = null;

beforeEach(() => {
  sandbox = sinon.createSandbox();
});

afterEach(() => {
  sandbox.restore();
});

test('should exist and export a default', () => {
  expect(Search).toBeDefined();
});

test('should be a constructor', () => {
  const invoke = () => {
    new Search();
  };
  expect(invoke).not.toThrow();
  expect(invoke).toBeDefined();
});

describe('initialize', () => {
  test('shoud have an asyncronous initialization method called initialize', async () => {
    const search = new Search();
    expect(search.initialize).toBeDefined();
    try {
      const response = await search.initialize();
      expect(response).toBeDefined();
    } catch(error) {
      throw new Error('FAILED: shoud have an asyncronous initialization method called init.');
    }
  });

  test('should log message when inititialize successfully called', async () => {
    const search = new Search();
    jest.spyOn(search._logger, 'log');
    try {
      await search.initialize();
      expect(search._logger.log).toHaveBeenCalled();
      expect(search._logger.log).toHaveBeenCalledWith('star', 'connected to District Watch search.');
    } catch(error) {
      throw new Error('FAILED: should log message when initialize successfully called.');
    }
  });

  test('should log failure message when initialize fails', async () => {
    const mockError = new Error('some error');
    const search = new Search();
    jest.spyOn(search._elasticSearch, 'connect').mockImplementationOnce(() => { throw mockError});
    jest.spyOn(search._logger, 'error');
    try {
      await search.initialize();
      throw new Error('FAILED: should log failure message when initialize fails but did not');
    } catch(error) {
      expect(search._logger.error).toHaveBeenCalled();
      expect(search._logger.error).toHaveBeenCalledWith(mockError);
      expect(error).toEqual(mockError);
    }
  });
});

describe('ensureIndexExists', () => {
  test('should ensure the proper indexes exist in the search engine instance', async () => {
    const search = new Search();
    jest.spyOn(search._elasticSearch, 'connect').mockImplementationOnce(() => { return Promise.resolve(); });
    jest.spyOn(search._elasticSearch, 'indexExists').mockImplementationOnce(() => { return Promise.resolve(true); });
    jest.spyOn(search._elasticSearch, 'createIndex').mockImplementationOnce(() => { return Promise.resolve(); });
    const ensureMethodSpy = jest.spyOn(search, 'ensureIndexExists');
    try {
      await search.initialize();
      expect(ensureMethodSpy).toHaveBeenCalledWith('documents');
      expect(search._elasticSearch.createIndex).not.toHaveBeenCalled();
    } catch(error) {
      throw new Error('FAILED: should ensure the proper indexes exist in the search engine instance');
    }
  });

  test('should create documents index if it does not exist', async () => {
    const search = new Search();
    jest.spyOn(search._elasticSearch, 'connect').mockImplementationOnce(() => { return Promise.resolve(); });
    jest.spyOn(search._elasticSearch, 'indexExists').mockImplementationOnce(() => { return Promise.resolve(false); });
    jest.spyOn(search._elasticSearch, 'createIndex').mockImplementationOnce(() => { return Promise.resolve(); });
    try {
      await search.initialize();
      expect(search._elasticSearch.createIndex).toHaveBeenCalledWith('documents');
    } catch(error) {
      throw new Error('FAILED: should create documents index if it does not exist');
    }
  });

  test.each([undefined, null, 3, true, [], {}, new Date()])(
    'should throw if incorrect data type for param - should be string',
    async (param) => {
      try {
        const search = new Search();
        await search.ensureIndexExists(param);
        throw new Error('this should throw into the catch for this test but did not, fix it!');
      } catch (error) {
        expect(error).toEqual(Error('dw-search: ensureIndexExists() method requires indexName to be a string.'));
      }
    }
  );
});

describe('index', () => {
  test('should call to create a new item in index via elasticSearch', async () => {
    const search = new Search();
    jest.spyOn(search._elasticSearch, 'index').mockImplementationOnce(() => { return Promise.resolve({ some: 'prop' }); });
    try {
      await search.initialize();
      const response = await search.index('documents', '1234', 'document', { odd: 'birdie' });
      expect(response.some).toEqual('prop');
      expect(search._elasticSearch.index).toHaveBeenCalledWith('documents', '1234', 'document', { odd: 'birdie' });
    } catch(error) {
      throw new Error('FAILED: should create documents index if it does not exist');
    }
  });

  test.each([undefined, null, 3, true, [], {}, new Date()])(
    'should throw if incorrect data type for indexName - should be string',
    async (param) => {
      const search = new Search();
      jest.spyOn(search._elasticSearch, 'index').mockImplementationOnce(() => { return Promise.resolve({ some: 'prop' }); });
      try {
        await search.index(param, 'string', 'string', {});
        throw new Error('this should throw into the catch for this test but did not, fix it!');
      } catch (error) {
        expect(search._elasticSearch.index).not.toHaveBeenCalled();
        expect(error).toEqual(Error('dw-search: index() method requires indexName to be a string.'));
      }
    }
  );

  test.each([undefined, null, 3, true, [], {}, new Date()])(
    'should throw if incorrect data type for documentId - should be string',
    async (param) => {
      const search = new Search();
      jest.spyOn(search._elasticSearch, 'index').mockImplementationOnce(() => { return Promise.resolve({ some: 'prop' }); });
      try {
        await search.index('string', param, 'string', {});
        throw new Error('this should throw into the catch for this test but did not, fix it!');
      } catch (error) {
        expect(search._elasticSearch.index).not.toHaveBeenCalled();
        expect(error).toEqual(Error('dw-search: index() method requires documentId to be a string.'));
      }
    }
  );

  test.each([undefined, null, 3, true, [], {}, new Date()])(
    'should throw if incorrect data type for type - should be string',
    async (param) => {
      const search = new Search();
      jest.spyOn(search._elasticSearch, 'index').mockImplementationOnce(() => { return Promise.resolve({ some: 'prop' }); });
      try {
        const search = new Search();
        await search.index('string', 'string', param, {});
        throw new Error('this should throw into the catch for this test but did not, fix it!');
      } catch (error) {
        expect(search._elasticSearch.index).not.toHaveBeenCalled();
        expect(error).toEqual(Error('dw-search: index() method requires type to be a string.'));
      }
    }
  );

  test.each([undefined, null, 3, true, [], 'string', new Date()])(
    'should throw if incorrect data type for data - should be object',
    async (param) => {
      const search = new Search();
      jest.spyOn(search._elasticSearch, 'index').mockImplementationOnce(() => { return Promise.resolve({ some: 'prop' }); });
      try {
        await search.index('string', 'string', 'string', param);
        throw new Error('this should throw into the catch for this test but did not, fix it!');
      } catch (error) {
        expect(search._elasticSearch.index).not.toHaveBeenCalled();
        expect(error).toEqual(Error('dw-search: index() method requires data to be an object.'));
      }
    }
  );

});

describe('getConnectionUrl', () => {
  test('should return url of the search instance connection string', () => {
    const search = new Search();
    const testUrl = 'https://www.google.com';
    jest.spyOn(search._elasticSearch, 'getBonsaiUrl').mockImplementationOnce(() => { return testUrl });
    const response = search.getConnectionUrl();
    expect(response).toEqual(testUrl);
  });
});

describe('query', () => {
  test('should accept a query and return hits', async () => {
    const query = {
      match: { "SomePropertyTest": "SomeValueTest" }
    };
    const search = new Search();
    jest.spyOn(search._elasticSearch, 'query').mockImplementationOnce(() => { return Promise.resolve([{ _id: '1234' }]); });
    try {
      await search.query(query);
      expect(search._elasticSearch.query).toHaveBeenCalledWith(query);
    } catch (error) {
      throw new Error('this should not throw into the catch for this test but did, fix it!');
    }
  });

  test.each([undefined, null, 3, true, [], 'string', new Date()])(
    'should throw if incorrect data type for query - should be object',
    async (param) => {
      const search = new Search();
      jest.spyOn(search._elasticSearch, 'query').mockImplementationOnce(() => { return Promise.resolve([{ some: 'prop' }]); });
      try {
        await search.query(param);
        throw new Error('this should throw into the catch for this test but did not, fix it!');
      } catch (error) {
        expect(search._elasticSearch.query).not.toHaveBeenCalled();
        expect(error).toEqual(Error('dw-search: query() method requires query to be an object.'));
      }
    }
  );
});

import { Elasticsearch } from "../src/elasticSearch";

test('should exist and export a default', () => {
  expect(Elasticsearch).toBeDefined();
});

test('should be a constructor', () => {
  const invoke = () => {
    new Elasticsearch();
  };
  expect(invoke).not.toThrow();
  expect(invoke).toBeDefined();
});

test('shoud have an asyncronous setup method called connect', async () => {
  const elasticSearch = new Elasticsearch();
  expect(elasticSearch.connect).toBeDefined();
  try {
    const response = await elasticSearch.connect()
    expect(response).toBeDefined();
  } catch(error) {
    throw new Error('FAILED: shoud have an asyncronous setup method called connect');
  }
});

test('connect should create the elasticSearch connection on a class member called client, client defaulted to null', async () => {
  const elasticSearch = new Elasticsearch();
  expect(elasticSearch.client).toEqual(null);
  try {
    await elasticSearch.connect()
    expect(elasticSearch.client).not.toEqual(null);
    expect(elasticSearch.client.indices).toBeDefined();
  } catch(error) {
    throw new Error('FAILED: connect should create the elasticSearch connection on a class member called client, client defaulted to null');
  }
});

test('getHealth method returns info about the connection', async () => {
  const elasticSearch = new Elasticsearch();
  try {
    await elasticSearch.connect();
    const healthResponse = await elasticSearch.getHealth();
    expect(typeof healthResponse.timed_out).toEqual('boolean');
    expect(typeof healthResponse.status).toEqual('string');
    expect(typeof healthResponse.cluster_name).toEqual('string');
  } catch(error) {
    throw new Error('FAILED: getHealth method returns info about the connection');
  }
});

test('createIndex method returns acknowledgement', async () => {
  const elasticSearch = new Elasticsearch();
  const expectedResponse =   {
    "acknowledged": true,
    "shards_acknowledged": true,
    "index": "footest"
  };
  try {
    await elasticSearch.connect();
    const response = await elasticSearch.createIndex('footest');
    expect(response).toEqual(expectedResponse);
    await elasticSearch.deleteIndex('footest');
  } catch(error) {
    throw new Error('FAILED: createIndex method returns acknowledgement');
  }
});

test('deleteIndex method returns acknowledgement', async () => {
  const elasticSearch = new Elasticsearch();
  const expectedResponse =   {
    "acknowledged": true
  };
  try {
    await elasticSearch.connect();
    await elasticSearch.createIndex('footest');
    const response = await elasticSearch.deleteIndex('footest');
    expect(response).toEqual(expectedResponse);
  } catch(error) {
    throw new Error('FAILED: deleteIndex method returns acknowledgement');
  }
});

test('indexExists method returns false or true if index exists or not', async () => {
  const elasticSearch = new Elasticsearch();
  try {
    await elasticSearch.connect();
    const existsResponse = await elasticSearch.indexExists('fooDoesNotExist');
    expect(existsResponse).toEqual(false);
    await elasticSearch.createIndex('footesty');
    const affirmResponse = await elasticSearch.indexExists('footesty');
    expect(affirmResponse).toEqual(true);
    await elasticSearch.deleteIndex('footesty');
  } catch(error) {
    throw new Error('FAILED: indexExists method returns false or true if index exists or not');
  }
});

test('index method will index a document', async () => {
  const elasticSearch = new Elasticsearch();
  try {
    await elasticSearch.connect();
    await elasticSearch.createIndex('footestz');
    const response = await elasticSearch.index('footestz', '1234', 'foo test type', { SomePropertyTest: "SomeValueTest" });
    expect(response._id).toEqual('1234');
    expect(response._index).toEqual('footestz');
    expect(response.result).toEqual('created');
    await elasticSearch.deleteIndex('footestz');
  } catch(error) {
    throw new Error('FAILED: index method will index a document');
  }
});

test('delete method will delete document', async () => {
  const elasticSearch = new Elasticsearch();
  try {
    await elasticSearch.connect();
    await elasticSearch.createIndex('footestz');
    await elasticSearch.index('footestz', '1234', 'foo test type', { SomePropertyTest: "SomeValueTest" });
    const response = await elasticSearch.delete('footestz', '1234', 'foo test type');
    expect(response._id).toEqual('1234');
    expect(response._index).toEqual('footestz');
    expect(response.result).toEqual('deleted');
    await elasticSearch.deleteIndex('footestz');
  } catch(error) {
    throw new Error('FAILED: delete method will delete document');
  }
});

test('batch method will put a batch of documents', async () => {
  const elasticSearch = new Elasticsearch();
  const testBatchCreate = [
    { index:  { _index: 'footestz', _type: 'foo test type', _id: 1234 } },
    { SomePropertyTest: "SomeValueTest" },
    { index:  { _index: 'footestz', _type: 'foo test type', _id: 5678 } },
    { SomePropertyTestTwo: "SomeValueTestTwo" }
  ];
  const testBatchDelete = [
    { delete:  { _index: 'footestz', _type: 'foo test type', _id: 1234 } },
    { delete:  { _index: 'footestz', _type: 'foo test type', _id: 5678 } }
  ];
  try {
    await elasticSearch.connect();
    await elasticSearch.createIndex('footestz');
    const createResponse = await elasticSearch.bulk(testBatchCreate);    
    expect(createResponse.items.length).toEqual(2);
    expect(createResponse.items[0].index._id).toEqual('1234');
    expect(createResponse.items[0].index.status).toEqual(201);
    expect(createResponse.items[1].index._id).toEqual('5678');
    expect(createResponse.items[1].index.status).toEqual(201);
    const deleteResponse = await elasticSearch.bulk(testBatchDelete);
    expect(deleteResponse.items.length).toEqual(2);
    expect(deleteResponse.items[0].delete._id).toEqual('1234');
    expect(deleteResponse.items[0].delete.status).toEqual(200);
    expect(deleteResponse.items[1].delete._id).toEqual('5678');
    expect(deleteResponse.items[1].delete.status).toEqual(200);
    await elasticSearch.deleteIndex('footestz');
  } catch(error) {
    throw new Error('FAILED: batch method will put a batch of documents');
  }
});

test('query method will query the index for documents', async () => {
  const elasticSearch = new Elasticsearch();
  const testIndexedData = { SomePropertyTest: "SomeValueTest" };
  const testBatchCreate = [
    { index:  { _index: 'footestz', _type: 'foo test type', _id: 1234 } },
    testIndexedData,
    { index:  { _index: 'footestz', _type: 'foo test type', _id: 5678 } },
    { SomePropertyTestTwo: "SomeValueTestTwo" }
  ];
  const testBatchDelete = [
    { delete:  { _index: 'footestz', _type: 'foo test type', _id: 1234 } },
    { delete:  { _index: 'footestz', _type: 'foo test type', _id: 5678 } }
  ];
  const query = {
    match: { "SomePropertyTest": "SomeValueTest" }
  };
  try {
    await elasticSearch.connect();
    await elasticSearch.createIndex('footestz');
    await elasticSearch.bulk(testBatchCreate, true);
    const response = await elasticSearch.query(query);
    expect(response.hits.total).toEqual(1);
    expect(response.hits.hits[0]._id).toEqual('1234');
    expect(response.hits.hits[0]._source).toEqual(testIndexedData);
    await elasticSearch.bulk(testBatchDelete);
    await elasticSearch.deleteIndex('footestz');
  } catch(error) {
    throw new Error('FAILED: query method will query the index for documents');
  }
});

/**
 * District Watch Search module.
 * @module dw-search
 * @author Brian Kennedy <bpkennedy@gmail.com>
 * @desc Provides a minimal api for document indexing and querying.
 */
import _ from './env'; // eslint-disable-line no-unused-vars
import { Logger } from 'dw-logger';
import { Elasticsearch } from './elasticSearch';
import { Utils } from 'dw-utils';
const utils = new Utils();

/**
 * The search wrapper class for all interaction with search functionality in district watch.
 * @param { string } environment  the instance environment, development or production.
 * @throws Will throw an error if environment is not 'development' or 'production'.
 */
export class Search {
  constructor() {
    this._logger = new Logger();
    this._elasticSearch = new Elasticsearch();
  }

  /** Initialize starts the elasticSearch connection and ensures the documents index exists
   * @throws Will throw an error if cannot connect to elasticSearch or cannot verify if documents index exists
   * @returns {Promise} Promise object resolves with the connected client object or rejects with error.
   */
  async initialize() {
    try {
      this._logger.log('star', 'connected to District Watch search.');
      await this._elasticSearch.connect();
      await this.ensureIndexExists('documents');  
      return this._elasticSearch;
    } catch(error) {
      this._logger.error(error);
      throw error;
    }
  }

  /** Ensures a named index exists
   * @param {string} indexName the name of the index to check for existence and create if does not exist.
   * @throws Will throw an error if cannot verify if documents index exists or cannot create a new one in elasticsearch.
   * @returns {Promise} Promise object resolves with the an acknowledgement of new created or undefined if already created, rejects with error if errors.
   */
  async ensureIndexExists(indexName) {
    utils.validateString('dw-search', indexName, 'ensureIndexExists', 'indexName');
    try {
      const indexExists = await this._elasticSearch.indexExists(indexName);
      if (indexExists === false) {
        return await this._elasticSearch.createIndex(indexName);
      }
      return;
    } catch(error) {
      throw error;
    }
  }

  /** Index a document
   * @param {string} indexName The name of the index to put this new document into.
   * @param {string} documentId The id of the new document.
   * @param {string} type The type of the new document.
   * @param {object} data The new document data represented as an object.
   * @throws Will throw an error if cannot verify if documents index exists or cannot create a new one in elasticsearch or if param types are incorrect.
   * @returns {Promise} Promise object resolves with the an acknowledgement of new created or undefined if already created, rejects with error if errors.
   */
  async index(indexName, documentId, type, data) {
    utils.validateString('dw-search', indexName, 'index', 'indexName');
    utils.validateString('dw-search', documentId, 'index', 'documentId');
    utils.validateString('dw-search', type, 'index', 'type');
    utils.validateObject('dw-search', data, 'index', 'data');
    try {
      return await this._elasticSearch.index(indexName, documentId, type, data);
    } catch(error) {
      throw error;
    }
  }

  /** Query an documents by a query object.
   * @param {object} query The elasticsearch query object that will get sent with the request as the 'body' param.
   * @throws Will throw an error if query fails for some reason or if param types are wrong.
   * @returns {Promise} Promise object resolves with hits object or rejects with error
   */
  async query(query) {
    utils.validateObject('dw-search', query, 'query', 'query');
    try {
      return await this._elasticSearch.query(query);
    } catch(error) {
      throw error;
    }
  }

  /** Get the connection url from process.env
   * @throws Will throw an error if connection environment variable does not exist.
   */
  getConnectionUrl() {
    return this._elasticSearch.getBonsaiUrl();
  }
}
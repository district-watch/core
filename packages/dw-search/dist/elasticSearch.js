'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Elasticsearch = undefined;

require('dotenv/config');

var _elasticsearch = require('elasticsearch');

var _elasticsearch2 = _interopRequireDefault(_elasticsearch);

var _dwUtils = require('dw-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const utils = new _dwUtils.Utils();

/**
 * The elasticsearch adapter class handles creation of the elasticsearch connection and low level interactions with it.
 */
class Elasticsearch {
  /**
   * @throws Will throw if BONSAI_URL environment variable does not exist
   */
  constructor() {
    this.client = null;
    this._bonsai_url = this.getBonsaiUrl();
  }

  /** Get the Bonsai Url from process.env
   * @throws Will throw an error if BONSAI_URL environment variable does not exist.
   */
  getBonsaiUrl() {
    if (process.env.BONSAI_URL) {
      return process.env.BONSAI_URL;
    }
    throw new Error('Bonsai Url environment variable not defined');
  }

  /** Connect to the bonsai hosted elasticsearch instance
   * @throws Will throw an error if host url is not correctly formed/undefined/null.
   * @returns {Promise} Promise object resolves with the connected client object.
   */
  async connect() {
    this.client = await new _elasticsearch2.default.Client({
      host: this._bonsai_url,
      log: process.env.NODE_ENV && process.env.NODE_ENV.toLowerCase() === 'debug' ? 'trace' : 'info'
    });
    return this.client;
  }

  /** Get a health check of the elasticsearch instance
   * @throws Will throw an error if client instance doesn't exist
   * @returns {Promise} Promise object resolves with a status object
   */
  async getHealth() {
    try {
      return await this.client.cluster.health();
    } catch (error) {
      throw error;
    }
  }

  /** Checks to see if an index exists, by name
   * @param {string} index The name of the index to check if exists.
   * @throws Will throw an error if indexExists call fails or if param types are wrong.
   * @returns {Promise} Promise object resolves with boolean or rejects with error
   */
  async indexExists(index) {
    utils.validateString('dw-search', index, 'indexExists', 'index');
    try {
      return await this.client.indices.exists({ index });
    } catch (error) {
      throw error;
    }
  }

  /** Create a new index, by name
   * @param {string} index The name of the index to create.
   * @throws Will throw an error if index already exists or if param types are wrong.
   * @returns {Promise} Promise object resolves with acknowledged object or rejects with error
   */
  async createIndex(index) {
    utils.validateString('dw-search', index, 'createIndex', 'index');
    try {
      return await this.client.indices.create({ index });
    } catch (error) {
      throw error;
    }
  }

  /** Deletes an index, by name
   * @param {string} index The name of the index to delete.
   * @throws Will throw an error if deleteIndex fails for some reason or if param types are wrong.
   * @returns {Promise} Promise object resolves with acknowledged object or rejects with error
   */
  async deleteIndex(index) {
    utils.validateString('dw-search', index, 'deleteIndex', 'index');
    try {
      return await this.client.indices.delete({ index });
    } catch (error) {
      throw error;
    }
  }

  /** Indexes a json data item to an index
   * @param {string} index The name of the index to put this new item into.
   * @param {string} id The id of the new item.
   * @param {string} type The type of the new item.
   * @param {object} data The new item data represented as an object.
   * @param {boolean} refresh Indicate if the indexed item should be made immediately searchable in queries.
   * @throws Will throw an error if index fails for some reason or if param types are wrong.
   * @returns {Promise} Promise object resolves with acknowledged object or rejects with error
   */
  async index(index, id, type, data, refresh) {
    utils.validateString('dw-search', index, 'index', 'index');
    utils.validateString('dw-search', id, 'index', 'id');
    utils.validateString('dw-search', type, 'index', 'type');
    utils.validateObject('dw-search', data, 'index', 'data');
    if (refresh) {
      utils.validateBoolean('dw-search', refresh, 'index', 'refresh');
    }
    try {
      const arrangedParameters = Object.assign({ index, id, type, body: data }, { refresh: refresh ? refresh.toString() : 'false' });
      return await this.client.index(arrangedParameters);
    } catch (error) {
      throw error;
    }
  }

  /** Operates on a batch of new items to either index, update, or delete
   * @param {string} body The array of objects that are items and actions paired together, see example.
   * @param {boolean} refresh Indicate if the indexed item should be made immediately searchable in queries.
   * @throws Will throw an error if bulk fails for some reason or if param types are wrong.
   * @returns {Promise} Promise object resolves with acknowledged object or rejects with error
   * @example
   * 
   *   const testBatchCreate = [
   *     { index:  { _index: 'footestz', _type: 'foo test type', _id: 1234 } },
   *     { SomePropertyTest: "SomeValueTest" },
   *     { index:  { _index: 'footestz', _type: 'foo test type', _id: 5678 } },
   *     { SomePropertyTestTwo: "SomeValueTestTwo" }
   *   ];
   *   const testBatchDelete = [
   *     { delete:  { _index: 'footestz', _type: 'foo test type', _id: 1234 } },
   *     { delete:  { _index: 'footestz', _type: 'foo test type', _id: 5678 } }
   *   ];
   *
   *  await client.bulk(testBatchCreate);
   *  await client.bulk(testBatchDelete);
   */
  async bulk(body, refresh) {
    utils.validateArray('dw-search', body, 'bulk', 'body');
    if (refresh) {
      utils.validateBoolean('dw-search', refresh, 'index', 'refresh');
    }
    try {
      const arrangedParameters = Object.assign({ body }, { refresh: refresh ? refresh.toString() : 'false' });
      return await this.client.bulk(arrangedParameters);
    } catch (error) {
      throw error;
    }
  }

  /** Query an index for documents by a query object.
   * @param {object} query The elasticsearch query object that will get sent with the request as the 'body' param.
   * @throws Will throw an error if query fails for some reason or if param types are wrong.
   * @returns {Promise} Promise object resolves with hits object or rejects with error
   */
  async query(query) {
    utils.validateObject('dw-search', query, 'query', 'query');
    try {
      return await this.client.search({ body: { query } });
    } catch (error) {
      throw error;
    }
  }

  /** Delete an item from the index
   * @param {string} index The name of the index to delete.
   * @param {string} id The id of the item to delete.
   * @param {string} type The type of the item to delete.
   * @throws Will throw an error if delete fails for some reason or if param types are wrong.
   * @returns {Promise} Promise object resolves with acknowledgement object or rejects with error
   */
  async delete(index, id, type) {
    utils.validateString('dw-search', index, 'delete', 'index');
    utils.validateString('dw-search', id, 'delete', 'id');
    utils.validateString('dw-search', type, 'delete', 'type');
    try {
      return await this.client.delete({ index, id, type });
    } catch (error) {
      throw error;
    }
  }

}
exports.Elasticsearch = Elasticsearch;
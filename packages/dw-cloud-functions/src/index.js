/* eslint-disable */

/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const functions = require('firebase-functions');
const nodemailer = require('nodemailer');

const mailTransport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'thedistrictwatch@gmail.com',
    pass: 'ThisIsGreat12345!',
  },
});

function getSubscribeHtml(subscriberName, subscriberId, subscriberBeta) {
  const name = subscriberName ? subscriberName : 'there';
  const updateMessageHtml = `<p>You have successfully signed up for the District Watch Updates. We'll make sure that you get the rundown on the latest happenings.</p>`;
  let preheaderText = '';
  let betaMessageHtml = '';
  if (subscriberBeta) {
    preheaderText = 'You signed up for District Watch updates and joined the beta';
    betaMessageHtml = `<p>Thanks for also signing up for the District Watch Beta. We move pretty fast, so you'll be hearing from us soon!</p>`;
  } else {
    preheaderText = 'You signed up for District Watch updates'
  }
  // template from https://github.com/leemunroe/responsive-html-email-template
  return `<!doctype html>
  <html>
    <head>
      <meta name="viewport" content="width=device-width" />
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Simple Transactional Email</title>
      <style>
        /* -------------------------------------
            GLOBAL RESETS
        ------------------------------------- */
        img {
          border: none;
          -ms-interpolation-mode: bicubic;
          max-width: 100%; }
        body {
          background-color: #f6f6f6;
          font-family: sans-serif;
          -webkit-font-smoothing: antialiased;
          font-size: 14px;
          line-height: 1.4;
          margin: 0;
          padding: 0;
          -ms-text-size-adjust: 100%;
          -webkit-text-size-adjust: 100%; }
        table {
          border-collapse: separate;
          mso-table-lspace: 0pt;
          mso-table-rspace: 0pt;
          width: 100%; }
          table td {
            font-family: sans-serif;
            font-size: 14px;
            vertical-align: top; }
        /* -------------------------------------
            BODY & CONTAINER
        ------------------------------------- */
        .body {
          background-color: #f6f6f6;
          width: 100%; }
        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
        .container {
          display: block;
          Margin: 0 auto !important;
          /* makes it centered */
          max-width: 580px;
          padding: 10px;
          width: 580px; }
        /* This should also be a block element, so that it will fill 100% of the .container */
        .content {
          box-sizing: border-box;
          display: block;
          Margin: 0 auto;
          max-width: 580px;
          padding: 10px; }
        /* -------------------------------------
            HEADER, FOOTER, MAIN
        ------------------------------------- */
        .main {
          background: #ffffff;
          border-radius: 3px;
          width: 100%; }
        .wrapper {
          box-sizing: border-box;
          padding: 20px; }
        .content-block {
          padding-bottom: 10px;
          padding-top: 10px;
        }
        .footer {
          clear: both;
          Margin-top: 10px;
          text-align: center;
          width: 100%; }
          .footer td,
          .footer p,
          .footer span,
          .footer a {
            color: #999999;
            font-size: 12px;
            text-align: center; }
        /* -------------------------------------
            TYPOGRAPHY
        ------------------------------------- */
        h1,
        h2,
        h3,
        h4 {
          color: #000000;
          font-family: sans-serif;
          font-weight: 400;
          line-height: 1.4;
          margin: 0;
          Margin-bottom: 30px; }
        h1 {
          font-size: 35px;
          font-weight: 300;
          text-align: center;
          text-transform: capitalize; }
        p,
        ul,
        ol {
          font-family: sans-serif;
          font-size: 14px;
          font-weight: normal;
          margin: 0;
          Margin-bottom: 15px; }
          p li,
          ul li,
          ol li {
            list-style-position: inside;
            margin-left: 5px; }
        a {
          color: #3498db;
          text-decoration: underline; }
        /* -------------------------------------
            BUTTONS
        ------------------------------------- */
        .btn {
          box-sizing: border-box;
          width: 100%; }
          .btn > tbody > tr > td {
            padding-bottom: 15px; }
          .btn table {
            width: auto; }
          .btn table td {
            background-color: #ffffff;
            border-radius: 5px;
            text-align: center; }
          .btn a {
            background-color: #ffffff;
            border: solid 1px #3498db;
            border-radius: 5px;
            box-sizing: border-box;
            color: #3498db;
            cursor: pointer;
            display: inline-block;
            font-size: 14px;
            font-weight: bold;
            margin: 0;
            padding: 12px 25px;
            text-decoration: none;
            text-transform: capitalize; }
        .btn-primary table td {
          background-color: #3498db; }
        .btn-primary a {
          background-color: #3498db;
          border-color: #3498db;
          color: #ffffff; }
        /* -------------------------------------
            OTHER STYLES THAT MIGHT BE USEFUL
        ------------------------------------- */
        .last {
          margin-bottom: 0; }
        .first {
          margin-top: 0; }
        .align-center {
          text-align: center; }
        .align-right {
          text-align: right; }
        .align-left {
          text-align: left; }
        .clear {
          clear: both; }
        .mt0 {
          margin-top: 0; }
        .mb0 {
          margin-bottom: 0; }
        .preheader {
          color: transparent;
          display: none;
          height: 0;
          max-height: 0;
          max-width: 0;
          opacity: 0;
          overflow: hidden;
          mso-hide: all;
          visibility: hidden;
          width: 0; }
        .powered-by a {
          text-decoration: none; }
        hr {
          border: 0;
          border-bottom: 1px solid #f6f6f6;
          Margin: 20px 0; }
        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 620px) {
          table[class=body] h1 {
            font-size: 28px !important;
            margin-bottom: 10px !important; }
          table[class=body] p,
          table[class=body] ul,
          table[class=body] ol,
          table[class=body] td,
          table[class=body] span,
          table[class=body] a {
            font-size: 16px !important; }
          table[class=body] .wrapper,
          table[class=body] .article {
            padding: 10px !important; }
          table[class=body] .content {
            padding: 0 !important; }
          table[class=body] .container {
            padding: 0 !important;
            width: 100% !important; }
          table[class=body] .main {
            border-left-width: 0 !important;
            border-radius: 0 !important;
            border-right-width: 0 !important; }
          table[class=body] .btn table {
            width: 100% !important; }
          table[class=body] .btn a {
            width: 100% !important; }
          table[class=body] .img-responsive {
            height: auto !important;
            max-width: 100% !important;
            width: auto !important; }}
        /* -------------------------------------
            PRESERVE THESE STYLES IN THE HEAD
        ------------------------------------- */
        @media all {
          .ExternalClass {
            width: 100%; }
          .ExternalClass,
          .ExternalClass p,
          .ExternalClass span,
          .ExternalClass font,
          .ExternalClass td,
          .ExternalClass div {
            line-height: 100%; }
          .apple-link a {
            color: inherit !important;
            font-family: inherit !important;
            font-size: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
            text-decoration: none !important; }
          .btn-primary table td:hover {
            background-color: #34495e !important; }
          .btn-primary a:hover {
            background-color: #34495e !important;
            border-color: #34495e !important; } }
      </style>
    </head>
    <body class="">
      <table border="0" cellpadding="0" cellspacing="0" class="body">
        <tr>
          <td>&nbsp;</td>
          <td class="container">
            <div class="content">

              <!-- START CENTERED WHITE CONTAINER -->
              <span class="preheader">${preheaderText}</span>
              <table class="main">

                <!-- START MAIN CONTENT AREA -->
                <tr>
                  <td class="wrapper">
                    <table border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td>
                          <p>Hi ${name}!</p>
                          ${updateMessageHtml}
                          ${betaMessageHtml}
                          <p>Also, make sure to <a href="https://join.slack.com/t/districtwatch/shared_invite/enQtNDM2NTI3NjQzOTQxLWJhY2E3YTg1ZDZjNmI0OGJjZmVjMjZiOTEzOWIzY2Y2ODcyNmMxOWUxMjg2NDZjYWQwZGQ4YTg4YWNiZmJmOGI" target="_blank" rel="noopener noreferrer">Join us on Slack</a> if you have questions or would like to hang around and be a part of the conversation.</p>
                          <p>Thanks for joining!</p>
                          <p>- the District Watch Team</p>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>

              <!-- END MAIN CONTENT AREA -->
              </table>

              <!-- START FOOTER -->
              <div class="footer">
                <table border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="content-block">
                      <span class="apple-link">District Watch, St. Louis MO 63033</span>
                      <br> Don't like these emails? <a href="http://district-watch-website.firebaseapp.com/unsubscribe.html?subscriberId=${subscriberId}" target="_blank" rel="noopener noreferrer">Unsubscribe</a>.
                    </td>
                  </tr>
                </table>
              </div>
              <!-- END FOOTER -->

            <!-- END CENTERED WHITE CONTAINER -->
            </div>
          </td>
          <td>&nbsp;</td>
        </tr>
      </table>
    </body>
  </html>`
};

function getUnsubscribeHtml(subscriberName, subscriberId, subscriberBeta) {
  const name = subscriberName ? subscriberName : 'there';
  // template from https://github.com/leemunroe/responsive-html-email-template
  return `<!doctype html>
  <html>
    <head>
      <meta name="viewport" content="width=device-width" />
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Simple Transactional Email</title>
      <style>
        /* -------------------------------------
            GLOBAL RESETS
        ------------------------------------- */
        img {
          border: none;
          -ms-interpolation-mode: bicubic;
          max-width: 100%; }
        body {
          background-color: #f6f6f6;
          font-family: sans-serif;
          -webkit-font-smoothing: antialiased;
          font-size: 14px;
          line-height: 1.4;
          margin: 0;
          padding: 0;
          -ms-text-size-adjust: 100%;
          -webkit-text-size-adjust: 100%; }
        table {
          border-collapse: separate;
          mso-table-lspace: 0pt;
          mso-table-rspace: 0pt;
          width: 100%; }
          table td {
            font-family: sans-serif;
            font-size: 14px;
            vertical-align: top; }
        /* -------------------------------------
            BODY & CONTAINER
        ------------------------------------- */
        .body {
          background-color: #f6f6f6;
          width: 100%; }
        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
        .container {
          display: block;
          Margin: 0 auto !important;
          /* makes it centered */
          max-width: 580px;
          padding: 10px;
          width: 580px; }
        /* This should also be a block element, so that it will fill 100% of the .container */
        .content {
          box-sizing: border-box;
          display: block;
          Margin: 0 auto;
          max-width: 580px;
          padding: 10px; }
        /* -------------------------------------
            HEADER, FOOTER, MAIN
        ------------------------------------- */
        .main {
          background: #ffffff;
          border-radius: 3px;
          width: 100%; }
        .wrapper {
          box-sizing: border-box;
          padding: 20px; }
        .content-block {
          padding-bottom: 10px;
          padding-top: 10px;
        }
        .footer {
          clear: both;
          Margin-top: 10px;
          text-align: center;
          width: 100%; }
          .footer td,
          .footer p,
          .footer span,
          .footer a {
            color: #999999;
            font-size: 12px;
            text-align: center; }
        /* -------------------------------------
            TYPOGRAPHY
        ------------------------------------- */
        h1,
        h2,
        h3,
        h4 {
          color: #000000;
          font-family: sans-serif;
          font-weight: 400;
          line-height: 1.4;
          margin: 0;
          Margin-bottom: 30px; }
        h1 {
          font-size: 35px;
          font-weight: 300;
          text-align: center;
          text-transform: capitalize; }
        p,
        ul,
        ol {
          font-family: sans-serif;
          font-size: 14px;
          font-weight: normal;
          margin: 0;
          Margin-bottom: 15px; }
          p li,
          ul li,
          ol li {
            list-style-position: inside;
            margin-left: 5px; }
        a {
          color: #3498db;
          text-decoration: underline; }
        /* -------------------------------------
            BUTTONS
        ------------------------------------- */
        .btn {
          box-sizing: border-box;
          width: 100%; }
          .btn > tbody > tr > td {
            padding-bottom: 15px; }
          .btn table {
            width: auto; }
          .btn table td {
            background-color: #ffffff;
            border-radius: 5px;
            text-align: center; }
          .btn a {
            background-color: #ffffff;
            border: solid 1px #3498db;
            border-radius: 5px;
            box-sizing: border-box;
            color: #3498db;
            cursor: pointer;
            display: inline-block;
            font-size: 14px;
            font-weight: bold;
            margin: 0;
            padding: 12px 25px;
            text-decoration: none;
            text-transform: capitalize; }
        .btn-primary table td {
          background-color: #3498db; }
        .btn-primary a {
          background-color: #3498db;
          border-color: #3498db;
          color: #ffffff; }
        /* -------------------------------------
            OTHER STYLES THAT MIGHT BE USEFUL
        ------------------------------------- */
        .last {
          margin-bottom: 0; }
        .first {
          margin-top: 0; }
        .align-center {
          text-align: center; }
        .align-right {
          text-align: right; }
        .align-left {
          text-align: left; }
        .clear {
          clear: both; }
        .mt0 {
          margin-top: 0; }
        .mb0 {
          margin-bottom: 0; }
        .preheader {
          color: transparent;
          display: none;
          height: 0;
          max-height: 0;
          max-width: 0;
          opacity: 0;
          overflow: hidden;
          mso-hide: all;
          visibility: hidden;
          width: 0; }
        .powered-by a {
          text-decoration: none; }
        hr {
          border: 0;
          border-bottom: 1px solid #f6f6f6;
          Margin: 20px 0; }
        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 620px) {
          table[class=body] h1 {
            font-size: 28px !important;
            margin-bottom: 10px !important; }
          table[class=body] p,
          table[class=body] ul,
          table[class=body] ol,
          table[class=body] td,
          table[class=body] span,
          table[class=body] a {
            font-size: 16px !important; }
          table[class=body] .wrapper,
          table[class=body] .article {
            padding: 10px !important; }
          table[class=body] .content {
            padding: 0 !important; }
          table[class=body] .container {
            padding: 0 !important;
            width: 100% !important; }
          table[class=body] .main {
            border-left-width: 0 !important;
            border-radius: 0 !important;
            border-right-width: 0 !important; }
          table[class=body] .btn table {
            width: 100% !important; }
          table[class=body] .btn a {
            width: 100% !important; }
          table[class=body] .img-responsive {
            height: auto !important;
            max-width: 100% !important;
            width: auto !important; }}
        /* -------------------------------------
            PRESERVE THESE STYLES IN THE HEAD
        ------------------------------------- */
        @media all {
          .ExternalClass {
            width: 100%; }
          .ExternalClass,
          .ExternalClass p,
          .ExternalClass span,
          .ExternalClass font,
          .ExternalClass td,
          .ExternalClass div {
            line-height: 100%; }
          .apple-link a {
            color: inherit !important;
            font-family: inherit !important;
            font-size: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
            text-decoration: none !important; }
          .btn-primary table td:hover {
            background-color: #34495e !important; }
          .btn-primary a:hover {
            background-color: #34495e !important;
            border-color: #34495e !important; } }
      </style>
    </head>
    <body class="">
      <table border="0" cellpadding="0" cellspacing="0" class="body">
        <tr>
          <td>&nbsp;</td>
          <td class="container">
            <div class="content">

              <!-- START CENTERED WHITE CONTAINER -->
              <span class="preheader">Sorry to see you go.</span>
              <table class="main">

                <!-- START MAIN CONTENT AREA -->
                <tr>
                  <td class="wrapper">
                    <table border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td>
                          <p>Hi ${name}!</p>
                          <p>You have successfully unsubscribed from District Watch.  Sorry to see you go!</p>
                          <p>If you want to leave feedback, you can <a href="https://join.slack.com/t/districtwatch/shared_invite/enQtNDM2NTI3NjQzOTQxLWJhY2E3YTg1ZDZjNmI0OGJjZmVjMjZiOTEzOWIzY2Y2ODcyNmMxOWUxMjg2NDZjYWQwZGQ4YTg4YWNiZmJmOGI" target="_blank" rel="noopener noreferrer">Join us on Slack</a>.</p>
                          <p>Hope to see you again!</p>
                          <p>- Brian</p>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>

              <!-- END MAIN CONTENT AREA -->
              </table>

              <!-- START FOOTER -->
              <div class="footer">
                <table border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="content-block">
                      <span class="apple-link">District Watch, St. Louis MO 63033</span>
                    </td>
                  </tr>
                </table>
              </div>
              <!-- END FOOTER -->

            <!-- END CENTERED WHITE CONTAINER -->
            </div>
          </td>
          <td>&nbsp;</td>
        </tr>
      </table>
    </body>
  </html>`
};

exports.sendEmailConfirmation = functions.database.ref('/subscribers/{uid}').onWrite(async (change, context) => {
  const snapshot = change.after;
  const data = change.after.val();

  if (!change.after.exists()) {
    return null;
  }

  let mailOptions = {
    from: '"District Watch" <noreply@districtwatch.com>',
    to: data.email,
  };

  const subscribed = data.isSubscribed;

  mailOptions.subject = subscribed ? 'Signed up for District Watch updates' : 'Unsubscribed from District Watch';
  mailOptions.html = subscribed ? getSubscribeHtml(data.name, data.id, data.isBeta) : getUnsubscribeHtml(data.name, data.id, data.isBeta);
  
  try {
    await mailTransport.sendMail(mailOptions);
    console.log(`New ${subscribed ? '' : 'un'}subscription confirmation email sent to:`, data.email);
  } catch(error) {
    console.error('There was an error while sending the email:', error);
  }
  return null;
});
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _firebaseAdmin = require('firebase-admin');

var admin = _interopRequireWildcard(_firebaseAdmin);

var _vision = require('@google-cloud/vision');

var _vision2 = _interopRequireDefault(_vision);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

/* eslint-disable */
if (process.env.NODE_ENV !== "production") {
  require('dotenv').config();
}

const credentials = JSON.parse(process.env.GOOGLE_APPLICATION_CREDENTIALS);

exports.default = async () => {
  return await new _vision2.default.ImageAnnotatorClient({
    credentials
  });
};
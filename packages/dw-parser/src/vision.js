/* eslint-disable */
import * as admin from 'firebase-admin';
import Vision from '@google-cloud/vision';

if (process.env.NODE_ENV !== "production") {
  require('dotenv').config();
}

const credentials = JSON.parse(process.env.GOOGLE_APPLICATION_CREDENTIALS);

export default async () => {
  return await new Vision.ImageAnnotatorClient({
    credentials
  });
};
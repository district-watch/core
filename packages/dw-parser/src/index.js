/**
 * District Watch Parser module.
 * @module dw-parser
 * @desc Provides OCR file parsing capability for pdf files
 */
import { Logger } from 'dw-logger';
import connectVision from './vision';

/**
 * The parser class that provides OCR parsing.
 */
export class Parser {
  constructor() {
    this._logger = new Logger();
    this._vision = null;
  }

  /** Initialize the dw-parser module.
   * @returns {Promise} Promise object represents successful or failed initialization.
   * @example
   * 
   *   import { Parser } from './index';
   *   const parser = new Parser();
   * 
   *   try {
   *     await parser.initialize();
   *   } catch(error) {
   *     // oops, something went wrong
   *   }
   *   
   */
  async initialize() {
    try {
      this._vision = await connectVision();
    } catch(error) {
      this._logger.error(error);
    }
  }

  /** Remotely downloads, parses, and outputs to n number json files into destination.
   * @param {string} sourceFileUri The url to the source pdf file to parse.
   * @param {string} destinationJsonFileWithExtensionUri The destination url to save the json file to, including file extension.
   * @param {string} mimeType The mime type of the source file, only accepts 'application/pdf'.
   * @throws Will throw if parser is not initialized, param type is incorrect, or error from the google cloud vision sdk.
   * @returns {Promise} Promise object represents successful operation results or failure.
   * @example
   * 
   *   //given your instance is on a variable called 'parser'
   *   parser.parse(`gs://district-watch-dev-raw-docs/testybesty.pdf`, `gs://district-watch-dev-json-docs/testybesty.json`, `application/pdf`).then(response => {
   *     console.log('successful operation result is: ' + response);
   *   }).catch(error => {
   *     console.log('error was: ' + error);
   *   });
   *   
   */
  async parse(sourceFileUri, destinationJsonFileWithExtensionUri, mimeType) {
    const inputConfig = {
      mimeType,
      gcsSource: {
        uri: sourceFileUri
      }
    };
    const outputConfig = {
      gcsDestination: {
        uri: destinationJsonFileWithExtensionUri
      },
      batch_size: 100
    };
    const features = [{type: 'DOCUMENT_TEXT_DETECTION'}];
    const request = {
      requests: [
        {
          inputConfig: inputConfig,
          features: features,
          outputConfig: outputConfig,
        },
      ],
    };
    return await this._vision.asyncBatchAnnotateFiles(request).then(results => {
      const operation = results[0];
      return operation.promise();
    }).then(filesResponse => {
      return filesResponse;
    }).catch(err => {
      this._logger.error(err);
    });
  }

}
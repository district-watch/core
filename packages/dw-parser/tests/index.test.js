import { Parser } from '../src/index';

let instance;

describe('initialize', () => {
  test('should exist', () => {
    const invoke = () => {
      new Parser();
    };
    try {
      expect(invoke).not.toThrow();
    } catch(error) {
      throw new Error('should not have thrown, fix this.');
    }
  });

  test('should define logger and vision on the class', async () => {
    try {
      instance = new Parser();
      await instance.initialize();
      expect(instance._vision).toBeDefined();
      expect(instance._logger).toBeDefined();
    } catch(error) {
      throw new Error('should not have thrown, fix this.');
    }
  });
});

describe('parse', () => {
  beforeAll(async () => {
    try {
      instance = new Parser();
      await instance.initialize();  
    } catch(error) {
      throw new Error('should not have thrown, fix this.');
    }
  });
  
});
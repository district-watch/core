"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Utils = undefined;

var _fs = require("fs");

var fs = _interopRequireWildcard(_fs);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

/**
 * The framework utility class for common helper methods.
 */
class Utils {
  constructor() {}

  /** Determines if a string is a valid url using regex.
   * @param {string} string The string param to check as valid url.
   * @returns {boolean} True or False depending on validity.
   */
  isValidUrl(string) {
    const pattern = new RegExp("^" +
    // protocol identifier
    "(?:(?:https?|ftp)://)" +
    // user:pass authentication
    "(?:\\S+(?::\\S*)?@)?" + "(?:" +
    // IP address exclusion
    // private & local networks
    "(?!(?:10|127)(?:\\.\\d{1,3}){3})" + "(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" + "(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})" +
    // IP address dotted notation octets
    // excludes loopback network 0.0.0.0
    // excludes reserved space >= 224.0.0.0
    // excludes network & broacast addresses
    // (first & last IP address of each class)
    "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" + "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" + "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" + "|" +
    // host name
    "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
    // domain name
    "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
    // TLD identifier
    "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" +
    // TLD may end with dot
    "\\.?" + ")" +
    // port number
    "(?::\\d{2,5})?" +
    // resource path
    "(?:[/?#]\\S*)?" + "$", "i");
    if (!pattern.test(string)) {
      return false;
    } else {
      return true;
    }
  }

  /** Determines if local file path is valid, based on fs.existsSync
   * @param {string} path The string param to check as valid url.
   * @returns {boolean} True or False depending on validity.
   */
  isValidFilePath(path) {
    if (fs.existsSync(path)) {
      return true;
    }
    return false;
  }

  /** Determines if file to upload name is a valid file type
   * @param {string} fileName The file name, i.e. 'blackDuck.pdf'. Valid types are pdf, docx, doc, txt, json.
   * @returns {boolean} True or False depending on validity.
   */
  isValidUploadFileType(fileName) {
    this.validateString('dw-utils', fileName, 'isValidUploadFileType', 'fileName');
    const isValidFileType = /\.(pdf|docx|doc|txt|json)$/i.test(fileName);
    if (isValidFileType) {
      return true;
    }
    return false;
  }

  /** Determines if file to download name is a valid file type
   * @param {string} fileName The file name, i.e. 'blackDuck.json'.
   * @returns {boolean} True or False depending on validity.
   */
  isValidDownloadFileType(fileName) {
    this.validateString('dw-utils', fileName, 'isValidDownloadFileType', 'fileName');
    const isValidFileType = /\.(json)$/i.test(fileName);
    if (isValidFileType) {
      return true;
    }
    return false;
  }

  /** Determines if filter object is valid firebase query
   * @param { string } packageName The name of the managed package calling out to dw-utils (for error message clarity).
   * @param { Object[] } filter An array of objects describing the queries to make to get a subsection of a collection.
   * @param { string } filter[].propertyName The property name to query by.
   * @param { string } filter[].operand The operand to query by, only supports: '==', '>=', '<='.
   * @param { string } filter[].propertyValue The property value for the propertyName.
   * @param { string } method The caller method from the managed package (for error message clarity).
   * @throws Will throw if invalid.
   * @returns {boolean} True if a valid filter.
   */
  validateFilter(packageName, filter, method) {
    this.validateString(packageName, method, 'validateFilter', 'method');
    if (filter !== undefined && this.getType(filter) !== 'array') {
      throw new Error(`${packageName}: ${method}() method requires param filter to be an array.`);
    }
    if (filter !== undefined && !filter.length) {
      throw new Error(`${packageName}: ${method}() filter param must contain at least one object defining filter.`);
    }
    for (let query of filter) {
      if (!query.propertyName || this.getType(query.propertyName) !== 'string' || !query.operand || this.getType(query.operand) !== 'string' || !query.propertyValue || this.getType(query.propertyValue) !== 'string') {
        throw new Error(`${packageName}: ${method}() filter object must contain valid string properties: propertyName, operand, and propertyValue.`);
      }
    }
    return true;
  }

  /** Determines if is a string is a valid bucket name, 'raw' or 'json'.
   * @param { string } packageName The name of the managed package calling out to dw-utils (for error message clarity).
   * @param { string } name The name of the bucket.
   * @param { string } method The method this was called from, for error detail if this fails to validate.
   * @throws Will throw if invalid.
   * @returns {boolean} True if a valid string.
   */
  validateBucketName(packageName, name, method) {
    this.validateString(packageName, name, 'validateBucketName', 'name');
    this.validateString(packageName, method, 'validateBucketName', 'method');
    if (name !== 'raw' && name !== 'json' && name !== 'assembled') {
      throw new Error(`${packageName}: validateBucketName() requires bucket name to be 'raw', 'json', or 'assembled'.`);
    }
    return true;
  }

  /** Determines if is a valid string data type and reports error if not.
   * @param { string } packageName The name of the managed package calling out to dw-utils (for error message clarity).
   * @param { * } model A model to check for if is string.
   * @param { string } method The method this was called from, for error detail if this fails to validate.
   * @param { string } modelName The model name this in the called-from method, also for error detail if this fails to validate.
   * @throws Will throw if invalid.
   * @returns {boolean} True if a valid string.
   */
  validateString(packageName, model, method, modelName) {
    if (this.getType(modelName) !== 'string') {
      throw new Error(`${packageName}: validateString() requires modelName to be a string.`);
    }
    if (this.getType(method) !== 'string') {
      throw new Error(`${packageName}: validateString() requires method to be a string.`);
    }
    if (this.getType(model) !== 'string') {
      throw new Error(`${packageName}: ${method}() method requires ${modelName} to be a string.`);
    }
    if (this.getType(packageName) !== 'string') {
      throw new Error(`${packageName}: validateString() requires packageName to be a string.`);
    }
    return true;
  }

  /** Determines if is a valid object data type and reports error if not.
   * @param { string } packageName The name of the managed package calling out to dw-utils (for error message clarity).
   * @param { * } model A model to check for if is object.
   * @param { string } method The method this was called-from, for error detail if this fails to validate.
   * @param { string } modelName The model name this in the called-from method, also for error detail if this fails to validate.
   * @throws Will throw if invalid.
   * @returns {boolean} True if a valid filter.
   */
  validateObject(packageName, model, method, modelName) {
    if (this.getType(model) !== 'object') {
      throw new Error(`${packageName}: ${method}() method requires ${modelName} to be an object.`);
    }
  }

  /** Determines if is a valid boolean data type and reports error if not.
   * @param { string } packageName The name of the managed package calling out to dw-utils (for error message clarity).
   * @param { * } model A model to check for if is object.
   * @param { string } method The method this was called-from, for error detail if this fails to validate.
   * @param { string } modelName The model name this in the called-from method, also for error detail if this fails to validate.
   * @throws Will throw if invalid.
   * @returns {boolean} True if a valid filter.
   */
  validateBoolean(packageName, model, method, modelName) {
    if (this.getType(model) !== 'boolean') {
      throw new Error(`${packageName}: ${method}() method requires ${modelName} to be a boolean.`);
    }
  }

  /** Determines if is a valid array data type and reports error if not.
   * @param { string } packageName The name of the managed package calling out to dw-utils (for error message clarity).
   * @param { * } model A model to check for if is object.
   * @param { string } method The method this was called-from, for error detail if this fails to validate.
   * @param { string } modelName The model name this in the called-from method, also for error detail if this fails to validate.
   * @throws Will throw if invalid.
   * @returns {boolean} True if a valid filter.
   */
  validateArray(packageName, model, method, modelName) {
    if (this.getType(model) !== 'array') {
      throw new Error(`${packageName}: ${method}() method requires ${modelName} to be an array.`);
    }
  }

  /** Determines if is a valid function data type and reports error if not.
   * @param { string } packageName The name of the managed package calling out to dw-utils (for error message clarity).
   * @param { * } model A model to check for if is object.
   * @param { string } method The method this was called-from, for error detail if this fails to validate.
   * @param { string } modelName The model name this in the called-from method, also for error detail if this fails to validate.
   * @throws Will throw if invalid.
   * @returns {boolean} True if a valid filter.
   */
  validateFunction(packageName, model, method, modelName) {
    if (this.getType(model) !== 'function') {
      throw new Error(`${packageName}: ${method}() method requires ${modelName} to be a function.`);
    }
  }

  /** Determines if is a model is a javascript Error object type.
   * @param { string } packageName The name of the managed package calling out to dw-utils (for error message clarity).
   * @param { * } model A model to check for if is an Error.
   * @param { string } method The method this was called-from, for error detail if this fails to validate.
   * @param { string } modelName The model name this in the called-from method, also for error detail if this fails to validate.
   * @throws Will throw if invalid.
   * @returns {boolean} True if a valid filter.
   */
  validateError(packageName, model, method, modelName) {
    if (model instanceof Error) {
      return true;
    }
    throw new Error(`${packageName}: ${method}() method requires ${modelName} to be an Error object.`);
  }

  /** Get data type of param
   * @param { * } itemToCheck A model to check for data type.
   * @returns {string} Returns the data type of the param as a string of value: null, object, array, string, boolean, number, date, undefined.
   */
  getType(itemToCheck) {
    if (itemToCheck === undefined) {
      return 'undefined';
    }
    const types = {
      '[object Null]': 'null',
      '[object Object]': 'object',
      '[object Array]': 'array',
      '[object String]': 'string',
      '[object Boolean]': 'boolean',
      '[object Number]': 'number',
      '[object Date]': 'date',
      '[object Function]': 'function'
    };
    return types[Object.prototype.toString.call(itemToCheck)];
  }

  /** Get environment
   * @param { string } host A host name string.
   * @description looks at the host url and assumes local if development, staging if contains staging, and production otherwise.
   * @throws Will throw if param is invalid.
   * @returns {string} Returns the value of the environment - either 'development', 'staging', or 'production'.
   */
  getEnvironment(host) {
    this.validateString('utils', host, 'getEnvironment', 'host');
    if (host.indexOf('localhost') > -1 || host.indexOf('development') > -1 || host.indexOf('debug') > -1 || host.indexOf('test') > -1) {
      return 'development';
    } else if (host.indexOf('staging') > -1) {
      return 'staging';
    } else {
      return 'production';
    }
  }

}
exports.Utils = Utils; /**
                        * District Watch Utils module.
                        * @module dw-utils
                        * @author Brian Kennedy <bpkennedy@gmail.com>
                        * @version 1.0.2
                        * @desc Provides utilities for District Watch modules and apis.
                        */
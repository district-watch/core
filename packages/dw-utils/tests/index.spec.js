const {
  Utils
} = require('../src/index.js');
const utils = new Utils();

describe('isValidUrl', () => {
  test.each([
    'https://www.google.com',
    'http://www.google.com',
    'https://swc-shockball.firebaseio.com/',
    'https://swc-shockball.firebaseio.com',
    'https://swcshockball.firebaseio.com/',
    'https://swc-shock-ball.firebaseio.com/'
  ])(
    'it should validate url %i as true',
    (url) => {
      expect(utils.isValidUrl(url)).toBe(true);
    }
  );

  test.each([
    'blah',
    'notAUrl',
    'someotherNonUrl',
    {},
    [],
    null,
    undefined
  ])(
    'it should validate non-url %i as false',
    (url) => {
      expect(utils.isValidUrl(url)).toBe(false);
    }
  );
});

describe('isValidUploadFileType', () => {

  test.each(['test.pdf', 'again.doc', 'today.docx', 'another.txt', 'lastTest.json'])(
    'should return true if fileName is a valid file type.',
    (param) => {
      let result = utils.isValidUploadFileType(param);
      expect(result).toBe(true);
    }
  );

  test.each(['test.xls', 'again.xlsx', 'today.png', 'another.jpg', 'testy.ppt', 'testy2.pptx'])(
    'should return false if fileName is not a valid file type.',
    (param) => {
      let result = utils.isValidUploadFileType(param);
      expect(result).toBe(false);
    }
  );

});

describe('isValidDownloadFileType', () => {

  test.each(['test.pdf', 'again.doc', 'today.docx', 'another.txt', 'test.png', 'test.xls', 'test.ppt', 'test.jpg', 'test.csv'])(
    'should return false if fileName is not a json file type.',
    (param) => {
      let result = utils.isValidDownloadFileType(param);
      expect(result).toBe(false);
    }
  );

  test.each(['test.json'])(
    'should return true if fileName is a json file type.',
    (param) => {
      let result = utils.isValidDownloadFileType(param);
      expect(result).toBe(true);
    }
  );

});

describe('isValidFilePath', () => {

  test('should return true if param is json file', () => {
    const testParam = './__mocks__/mock-firebase-key.json';
    const result = utils.isValidFilePath(testParam);
    expect(result).toBe(true);
  });

  test.each(['./src/indexzz.js', {
      bad: 'prop'
    },
    ['more', 'bad', 'prop'], 2, null, undefined, new Date()
  ])(
    'should return false if param is not a fs readable string.',
    (param) => {
      let result = utils.isValidFilePath(param);
      expect(result).toBe(false);
    }
  );

  test('should return true if param is a fs readable string', () => {
    let result = utils.isValidFilePath('./src/index.js');
    expect(result).toBe(true);
  });

  test('should return false if param is undefined', () => {
    const testParam = undefined;
    const result = utils.isValidFilePath(testParam);
    expect(result).toBe(false);
  });

  test('should return false if param is null', () => {
    const testParam = null;
    const result = utils.isValidFilePath(testParam);
    expect(result).toBe(false);
  });
});

describe('isType', () => {
  test('should exist', () => {
    expect(utils.getType).toBeDefined();
  });

  test('should return string for a string', () => {
    const testVal = 'string';
    const result = utils.getType(testVal);
    expect(result).toBe('string');
  });

  test('should return object for object', () => {
    const testVal = {
      test: 'two'
    };
    const result = utils.getType(testVal);
    expect(result).toBe('object');
  });

  test('should return null for null', () => {
    const testVal = null;
    const result = utils.getType(testVal);
    expect(result).toBe('null');
  });

  test('should return array for array', () => {
    const testVal = [];
    const result = utils.getType(testVal);
    expect(result).toBe('array');
  });

  test('should return boolean for boolean', () => {
    const testVal = true;
    const result = utils.getType(testVal);
    expect(result).toBe('boolean');
    const testVal2 = false;
    const result2 = utils.getType(testVal2);
    expect(result2).toBe('boolean');
  });

  test('should return number for a number', () => {
    const testVal = 4;
    const result = utils.getType(testVal);
    expect(result).toBe('number');
  });

  test('should return date for a date', () => {
    const testVal = new Date();
    const result = utils.getType(testVal);
    expect(result).toBe('date');
  });

  test('should return undefined if undefined', () => {
    const testVal = undefined;
    const result = utils.getType(testVal);
    expect(result).toBe('undefined');
  });
});

describe('validateFilter', () => {
  test('should exist', () => {
    expect(utils.validateFilter).toBeDefined();
  });
  test('if filter param, should contain at least one object', () => {
    const invoke = () => {
      utils.validateFilter('testName', [], 'string');
    }
    expect(invoke).toThrow(Error(`testName: string() filter param must contain at least one object defining filter.`));
  });

  test.each([
    [], 4, {},
    true, undefined, null
  ])(
    'should have a method param that is a string',
    (param) => {
      const invoke = () => {
        utils.validateFilter('testName', [{
          propertyName: 'test',
          operand: '==',
          propertyValue: 'test'
        }], param);
      }
      expect(invoke).toThrow(Error(`testName: validateFilter() method requires method to be a string.`));
    }
  );

  test('if filter param, should have properties propertyName, operator, propertyValue', async () => {
    const invoke = () => {
      utils.validateFilter('testName', [{}], 'string');
    }
    expect(invoke).toThrow(Error(`testName: string() filter object must contain valid string properties: propertyName, operand, and propertyValue.`));
  });

  test.each([{
      badProps: 'badval'
    },
    {
      propertyName: {
        badPropVal: 'test'
      },
      operand: '==',
      propertyValue: 'test'
    },
    {
      propertyName: undefined,
      operand: '==',
      propertyValue: 'test'
    },
    {
      propertyName: 4,
      operand: '==',
      propertyValue: 'test'
    },
    {
      propertyName: true,
      operand: '==',
      propertyValue: 'test'
    },
    {
      propertyName: null,
      operand: '==',
      propertyValue: 'test'
    },
    {
      propertyName: [],
      operand: '==',
      propertyValue: 'test'
    },
    {
      propertyName: 'test',
      operand: '==',
      propertyValue: {
        badPropVal: 'test'
      }
    },
    {
      propertyName: 'test',
      operand: '==',
      propertyValue: undefined
    },
    {
      propertyName: 'test',
      operand: '==',
      propertyValue: 4
    },
    {
      propertyName: 'test',
      operand: '==',
      propertyValue: true
    },
    {
      propertyName: 'test',
      operand: '==',
      propertyValue: null
    },
    {
      propertyName: 'test',
      operand: '==',
      propertyValue: []
    },
    {
      propertyName: 'test',
      operand: {
        badProp: 'someval'
      },
      propertyValue: 'test'
    },
    {
      propertyName: 'test',
      operand: undefined,
      propertyValue: 'test'
    },
    {
      propertyName: 'test',
      operand: 4,
      propertyValue: 'test'
    },
    {
      propertyName: 'test',
      operand: true,
      propertyValue: 'test'
    },
    {
      propertyName: 'test',
      operand: null,
      propertyValue: 'test'
    },
    {
      propertyName: 'test',
      operand: [],
      propertyValue: 'test'
    }
  ])(
    'if filter param, should have properties propertyName, operator, propertyValue as strings',
    (param) => {
      const invoke = () => {
        utils.validateFilter('testName', [param], 'string');
      }
      expect(invoke).toThrow(Error(`testName: string() filter object must contain valid string properties: propertyName, operand, and propertyValue.`));
    }
  );
});

describe('validateString', () => {
  test('should exist', () => {
    expect(utils.validateString).toBeDefined();
  });

  test.each([{},
    [], 4, undefined, true, null
  ])(
    'should throw if model is not a string',
    (param) => {
      const invoke = () => {
        utils.validateString('testName', param, 'method', 'methodName');
      }
      expect(invoke).toThrowError();
    }
  );

  test.each([{},
    [], 4, undefined, true, null
  ])(
    'should throw if method is not a string',
    (param) => {
      const invoke = () => {
        utils.validateString('testName', 'model', param, 'methodName');
      }
      expect(invoke).toThrowError();
    }
  );

  test.each([{},
    [], 4, undefined, true, null
  ])(
    'should throw if modelName is not a string',
    (param) => {
      const invoke = () => {
        utils.validateString('testName', 'model', 'method', param);
      }
      expect(invoke).toThrowError();
    }
  );

  test.each([{},
    [], 4, undefined, true, null
  ])(
    'should throw if packageName is not a string',
    (param) => {
      const invoke = () => {
        utils.validateString(param, 'model', 'method', 'methodName');
      }
      expect(invoke).toThrowError();
    }
  );

});

describe('validateBucketName', () => {
  test('should exist', () => {
    expect(utils.validateBucketName).toBeDefined();
  });

  test.each([{},
    [], 4, undefined, true, null
  ])(
    'should throw if name is not a string',
    (param) => {
      const invoke = () => {
        utils.validateString('testName', param, 'name');
      }
      expect(invoke).toThrowError();
    }
  );

  test.each([{},
    [], 4, undefined, true, null
  ])(
    'should throw if method is not a string',
    (param) => {
      const invoke = () => {
        utils.validateString('testName', 'raw', param);
      }
      expect(invoke).toThrowError();
    }
  );

  test.each(['rawz', 'jsarns', 'thing', 'bucket', 'another'])(
    `should throw if name is not 'raw' or 'json'`,
    (param) => {
      const invoke = () => {
        utils.validateString('testName', param, 'name');
      }
      expect(invoke).toThrowError();
    }
  );

  test.each(['raw', 'json'])(
    `should not throw if name is 'raw' or 'json'`,
    (param) => {
      const invoke = () => {
        utils.validateString('testName', param, 'name');
      }
      expect(invoke).toThrowError();
    }
  );

});

describe('validateObject', () => {
  test('should exist', () => {
    expect(utils.validateObject).toBeDefined();
  });

  test.each([undefined, null, 3, true, [], '', new Date()])(
    'should throw when model param is not an object',
    (param) => {
      const invoke = () => {
        utils.validateObject('testName', 'test', param, 'test');
      }
      expect(invoke).toThrowError();
    }
  );

});

describe('validateFunction', () => {
  test('should exist', () => {
    expect(utils.validateFunction).toBeDefined();
  });

  test.each([undefined, null, 3, true, [], '', new Date(), {}])(
    'should throw when model param is not a function',
    (param) => {
      const invoke = () => {
        utils.validateFunction('testName', 'test', param, 'test');
      }
      expect(invoke).toThrowError();
    }
  );

});

describe('validateBoolean', () => {
  test('should exist', () => {
    expect(utils.validateBoolean).toBeDefined();
  });

  test.each([undefined, null, 3, () => {},
    [], '', new Date(), {}
  ])(
    'should throw when model param is not a boolean',
    (param) => {
      const invoke = () => {
        utils.validateBoolean('testName', 'test', param, 'test');
      }
      expect(invoke).toThrowError();
    }
  );

});

describe('validateArray', () => {
  test('should exist', () => {
    expect(utils.validateArray).toBeDefined();
  });

  test.each([undefined, null, 3, () => {}, true, '', new Date(), {}])(
    'should throw when model param is not a boolean',
    (param) => {
      const invoke = () => {
        utils.validateBoolean('testName', 'test', param, 'test');
      }
      expect(invoke).toThrowError();
    }
  );

});

describe('validateError', () => {
  test('should exist', () => {
    expect(utils.validateError).toBeDefined();
  });

  test.each([undefined, null, 3, true, [], '', new Date()])(
    'should throw when model param is not a Error object',
    (param) => {
      const invoke = () => {
        utils.validateError('testName', param, 'test', 'test');
      }
      expect(invoke).toThrowError();
    }
  );

});

describe('getEnvironment', () => {
  test('should exist', () => {
    expect(utils.getEnvironment).toBeDefined();
  });

  test.each([undefined, null, 3, true, [], {}, new Date()])(
    'should throw when model param is not a string',
    (param) => {
      const invoke = () => {
        utils.getEnvironment(param);
      }
      expect(invoke).toThrowError();
    });

  test.each(['localhost:8080', 'localhost:3000', 'localhost:4444'])(
    'should return localhost',
    (param) => {

      const result = utils.getEnvironment(param);
      expect(result).toEqual('development');
    });

  test.each(['dw-api-staging', 'dw-courier-staging'])(
    'should return staging',
    (param) => {
      const result = utils.getEnvironment(param);
      expect(result).toEqual('staging');
    });

  test.each(['dw-api', 'dw-courier-prod'])(
    'should return production',
    (param) => {
      const result = utils.getEnvironment(param);
      expect(result).toEqual('production');
    });
});
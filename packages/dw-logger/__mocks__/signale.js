module.exports = {
  Signale: class Signale {
    constructor() {}
    fatal() {
      return 'mock fatal error response';
    }
    success() {
      return 'mock success log response';
    }
    info() {
      return 'mock info log response';
    }
    debug() {
      return 'mock debug log response';
    }
  }
};
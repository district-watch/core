# District Watch Logger

A logger abstraction for use in the District Watch modules and apis

## Install

Add this to package.json dependencies (note the Release tag for package version):

```javascript
  ...
  "dependencies": {
    "dw-logger": "git+ssh://git@gitlab.com:district-watch/dw-logger.git#1.0.2"  // note the tag for package version...
  }
  ...
```

## Usage

```javascript
const { Logger } = require("dw-logger"); // <--- CommonJS
import { Logger } from "dw-logger"; // <--- ES2015
let logger = new Logger();

logger.log("success", "This is a great message!");
// [2018-9-4] [12:50:49] [index.js] › ++ SUCCESS   This is a great message!
```

## Debug/Production Mode

If `process.env.NODE_ENV` is set to `debug`, then any usage of `logger.log('debug', 'My debug message here')` will show in standard out. Otherwise, all debug level messages are not skipped.

const sinon = require('sinon');
const { Logger } = require('../src/index.js');
let logger = null;
let sandbox = null;

beforeEach(() => {
  logger = new Logger();
  sandbox = sinon.createSandbox();
});

afterEach(() => {
  sandbox.restore();
});

describe('constructor', () => {
  test('should set _debug to production if NODE_ENV environemnt variable not present', () => {
    expect(logger._debug).toEqual(false);
  });

  test('should set _debug to debug if NODE_ENV environemnt variable is set to debug or DEBUG', () => {
    expect(logger._debug).toEqual(false);
    process.env.NODE_ENV = 'debug';
    const logger2 = new Logger();
    expect(logger2._debug).toEqual(true);
    process.env.NODE_ENV = 'DEBUG';
    const logger3 = new Logger();
    expect(logger3._debug).toEqual(true);
    process.env.NODE_ENV = null;
    const logger4 = new Logger();
    expect(logger4._debug).toEqual(false);
    process.env.NODE_ENV = undefined;
    const logger5 = new Logger();
    expect(logger5._debug).toEqual(false);
  });

  test('should set levels array', () => {
    expect(logger._levels).toEqual(['success', 'debug', 'pending', 'warn', 'info', 'star', 'start', 'complete', 'log', 'error', 'fatal', 'fav', 'note']);
  });

});

describe('log', () => {
  test('should exist', () => {
    expect(logger.log).toBeDefined();
  });

  test.each([undefined, null, 3, true, [], {}, new Date()])(
    'message param should be a string or throw',
    async (param) => {
      try {
        logger.log('level', param, {});
        throw new Error('this should throw into the catch for this test but did not, fix it!');
      } catch (error) {
        expect(error).toEqual(Error('dw-logger: log() method requires message to be a string.'));
      }
    }
  );

  test.each([undefined, null, 3, true, [], {}, new Date()])(
    'level param should be a string or throw',
    async (param) => {
      try {
        logger.log(param, 'message', {});
        throw new Error('this should throw into the catch for this test but did not, fix it!');
      } catch (error) {
        expect(error).toEqual(Error('dw-logger: log() method requires level to be a string.'));
      }
    }
  );

  test('should use signale success for success level', () => {
    const result = logger.log('success', 'test message');
    expect(result).toEqual('mock success log response');
  });

  test('should use signale info for success level', () => {
    const result = logger.log('info', 'test message');
    expect(result).toEqual('mock info log response');
  });

  test('should throw if level does not exist', () => {
    const invoke = () => {
      logger.log('zzz', 'test message');
    };
    expect(invoke).toThrow(Error(`dw-logger: log() method does not support a level called 'zzz'.`));
  });

  test('should throw if messageObject param is passed but is not an object', () => {
    const invoke = () => {
      logger.log('info', 'test message', 'bad type');
    };
    expect(invoke).toThrow(Error('dw-logger: log() method requires messageObject to be an object.'));
  });

  test('should not log debug messages in production mode', () => {
    process.env.NODE_ENV = 'production';
    const logger2 = new Logger();
    const result = logger2.log('debug', 'test message');
    expect(result).toEqual(undefined);
    process.env.NODE_ENV = 'debug';
    const logger3 = new Logger();
    const result2 = logger3.log('debug', 'test message');
    expect(result2).toEqual('mock debug log response');
  });

});

describe('error', () => {
  test('should exist', () => {
    expect(logger.error).toBeDefined();
  });

  test.each([undefined, null, 3, true, [], {}, new Date()])(
    'errorObject param should be an error object or throw',
    async (param) => {
      try {
        logger.error(param);
        throw new Error('this should throw into the catch for this test but did not, fix it!');
      } catch (error) {
        expect(error).toEqual(Error('dw-logger: error() method requires errorObject to be an Error object.'));
      }
    }
  );

  test('should call signale fatal with errorObject', () => {
    const result = logger.error(new Error('test'));
    expect(result).toEqual('mock fatal error response');
  });
});

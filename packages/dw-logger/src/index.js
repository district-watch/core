/**
 * District Watch Logger module.
 * @module dw-logger
 * @author Brian Kennedy <bpkennedy@gmail.com>
 * @version 1.0.2
 * @desc Provides logging for District Watch modules and apis.
 */
import { Signale } from 'signale';
import { Utils } from 'dw-utils';
const signale = new Signale({
  types: {
    debug: {
      color: 'cyan'
    }
  },
  config: {
    coloredInterpolation: false,
    displayScope: true,
    displayBadge: true,
    displayDate: true,
    displayFilename: true,
    displayLabel: true,
    displayTimestamp: true,
    underlineLabel: true,
    underlineMessage: false,
    underlinePrefix: false,
    underlineSuffix: false,
    uppercaseLabel: true
  }
});
const utils = new Utils();

/**
 * The logger for all logging in district watch
 */
export class Logger {
  constructor() {
    this._debug = (process.env.NODE_ENV && process.env.NODE_ENV.toLowerCase() === 'debug') ? true : false;
    this._levels = ['success', 'debug', 'pending', 'warn', 'info', 'star', 'start', 'complete', 'log', 'error', 'fatal', 'fav', 'note'];
  }

  /** Log a message with a log level, string message, and optional message object
   * @param {string} level The log level, supported levels are: 'success', 'debug', 'pending', 'warn', 'info', 'star', 'start', 'complete', 'log', 'error', 'fatal', 'fav', 'note'
   * @param {string} message The log message.
   * @param {object} messageObject The optional log message object.  May be a plain object or an Error().
   * @throws Will throw if log level does not exist.
   * @returns {string} Returns the log string after printing to standard out console.
   * @example
   * 
   *   const { Logger } = require('./dist/index.js');
   *   let logger = new Logger();
   *   logger.log('success', 'This is a great message!');
   *   // [2018-9-4] [12:50:49] [index.js] › ++ SUCCESS   This is a great message!
   */
  log(level, message, messageObject) {
    utils.validateString('dw-logger', level, 'log', 'level');
    utils.validateString('dw-logger', message, 'log', 'message');
    if (!this._debug && level.toLowerCase() === 'debug') {
      return; // don't log debug messages in production mode.
    } else if (!messageObject && this._levels.indexOf(level) > -1) {
      return signale[level](message);
    } else if (this._levels.indexOf(level) > -1) {
      utils.validateObject('dw-logger', messageObject, 'log', 'messageObject');
      return signale[level](message, messageObject);
    }
    throw new Error(`dw-logger: log() method does not support a level called '${level}'.`);
  }

  /** Log an Error() with an Error object
   * @param {object} errorObject The Error object to log.
   * @throws Will throw if param is not an Error object.
   * @returns {string} Returns the log string after printing to standard out console.
   * @example
   * 
   *   const { Logger } = require('./dist/index.js');
   *   let logger = new Logger();
   *   logger.error(new Error('aww dayum!));
   *   //  [2018-9-4] [12:53:09] [index.js] › !!  fatal     Error: aww dayum! 
   *   //  at Object.<anonymous> (/Users/loaner/Projects/dw-logger/tester.js:15:14)
   *   //  at Module._compile (module.js:652:30)
   *   //  at Object.Module._extensions..js (module.js:663:10)
   *   //  at Module.load (module.js:565:32)
   *   //  at tryModuleLoad (module.js:505:12)
   *   //  at Function.Module._load (module.js:497:3)
   *   //  at Function.Module.runMain (module.js:693:10)
   *   //  at startup (bootstrap_node.js:191:16)
   *   //  at bootstrap_node.js:612:3
   */
  error(errorObject) {
    utils.validateError('dw-logger', errorObject, 'error', 'errorObject');
    return signale.fatal(errorObject);
  }
}

import React, { Component } from 'react';
import { Provider } from 'mobx-react';
import { NavigationProvider, StackNavigation } from '@expo/ex-navigation';

import { Router } from './src/router';
import Stores from './src/stores';

export default class App extends Component {
  render() {
    return (
      <Provider {...Stores}>
        <NavigationProvider router={Router}>
          <StackNavigation initialRoute={Router.getRoute('tab')} />
        </NavigationProvider>
      </Provider>
    );
  }
}

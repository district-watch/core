import { createRouter } from '@expo/ex-navigation';
import TabScreen from './containers/TabScreen';
import OfficialsScreen from './containers/OfficialsScreen';
import SearchScreen from './containers/SearchScreen';
import AboutScreen from './containers/AboutScreen';

export const Router = createRouter(() => ({
  tab: () => TabScreen,
  officials: () => OfficialsScreen,
  search: () => SearchScreen,
  about: () => AboutScreen
}));
import SearchApi from './searchApi';

export default {
  searchApi: new SearchApi()
};
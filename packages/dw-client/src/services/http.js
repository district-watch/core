import axios from 'axios';
import { apiProtocolAndFqdn } from '../config';

axios.defaults.baseURL = apiProtocolAndFqdn;

export default axios;

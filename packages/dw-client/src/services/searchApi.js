import { Alert } from 'react-native';
import Http from './http';

export default class SearchApi {
  fetchDocuments = async () => {
    try {
      return await Http.get(`/documents`);
    } catch (e) {
      Alert.alert('Connection error', 'Couldn\'t fetch documents.');
    }
  };
}
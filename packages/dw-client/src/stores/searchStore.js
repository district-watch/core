import { observable } from 'mobx';

export default class SearchStore {
  @observable query = '';
  @observable tracks = [];
  
  constructor(searchApi) {
    this.searchApi = searchApi;
  }

  get query() {
    return this.query;
  }

  set query(artist) {
    this.query = artist;
  }

  get tracks() {
    return this.tracks;
  }

  set tracks(result) {
    this.tracks = result;
  }

  async getTrackList(query) {
    if (!query) {
      this.tracks = [];
      return;
    }
    this.tracks = await this.searchApi.fetchDocuments();
  }
}
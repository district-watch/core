import { searchApi } from '../services';
import SearchStore from './searchStore';

export default {
  searchStore: new SearchStore(searchApi)
};
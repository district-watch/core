import React, { Component } from 'react';
import styled from 'styled-components/native';
import { observer, inject } from 'mobx-react';

@inject('searchStore')
@observer
export default class OfficialsScreen extends Component {
  static route = {
    navigationBar: {
      title: 'Officials',
    }
  }
  
  render() {
    return (
      <Container>
        <Welcome>District Watch Officials Screen.</Welcome>
      </Container>
    );
  }
}

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: #F5FCFF;
`;

const Welcome = styled.Text`
  font-size: 20px;
  text-align: center;
  margin: 10px;
`;

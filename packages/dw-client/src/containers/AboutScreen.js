import React, { Component } from 'react';
import styled from 'styled-components/native';

export default class AboutScreen extends Component {
  static route = {
    navigationBar: {
      title: 'About',
    }
  }

  render() {
    return (
      <Container>
        <StyledText>About District Watch</StyledText>
      </Container>
    )
  }
}

const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: #F5FCFF;
`;

const StyledText = styled.Text`
  font-size: 20px;
  text-align: center;
  margin: 10px;
`;
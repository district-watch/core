import React, { Component } from 'react';
import { MaterialCommunityIcons, Ionicons, MaterialIcons } from '@expo/vector-icons';
import { StackNavigation, TabNavigation, TabNavigationItem } from '@expo/ex-navigation';

import { Router } from '../router';

const styles = {
  selectedTab: {
    color: '#0F8C98'
  }
}

const navItems = [
  {
    id: 'search',
    iconFamily: MaterialCommunityIcons,
    activeIconName: 'feature-search',
    inactiveIconName: 'feature-search-outline'
  },
  {
    id: 'officials',
    iconFamily: MaterialIcons,
    activeIconName: 'people',
    inactiveIconName: 'people-outline'
  },
  {
    id: 'about',
    iconFamily: Ionicons,
    activeIconName: 'md-information-circle',
    inactiveIconName: 'md-information-circle-outline'
  }
];

export default class TabScreen extends Component {
  static route = {
    navigationBar: {
      visible: false,
      title: 'tab'
    }
  }
  
  renderStatefulIcon(isSelected, iconFamily, iconActiveName, iconInactiveName) {
    const IconFamily = iconFamily;
    if (isSelected) {
      return (<IconFamily name={`${iconActiveName}`} size={32} color="#017B86" />);
    } else {
      return (<IconFamily name={`${iconInactiveName}`} size={32} color="#03A1AE" />);
    }
  }
  
  renderTabItems() {
    return navItems.map(item => {
      return (
        <TabNavigationItem
        key={item.id}
        id={item.id}
        selectedStyle={styles.selectedTab}
        renderIcon={(isSelected) => this.renderStatefulIcon(isSelected, item.iconFamily, item.activeIconName, item.inactiveIconName)}
        >
          <StackNavigation id={item.id} navigatorUID={item.id} initialRoute={Router.getRoute(item.id)} />
        </TabNavigationItem>
      )
    });
  }

  render() {
    return (
      <TabNavigation
        id="main"
        navigatorUID="main"
        initialTab="search">
        {this.renderTabItems()}
      </TabNavigation>
    );
  }
}

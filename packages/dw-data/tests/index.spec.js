import * as admin from 'firebase-admin';
const sinon = require('sinon');
const {
  DatabaseWrapper
} = require('../src/index.js');
let db = null;
let sandbox = null;

beforeEach(() => {
  sandbox = sinon.createSandbox();
});

afterEach(() => {
  sandbox.restore();
});

test.each([{},
  [], undefined, null, 4, true
])(
  'should throw if environment param is %i',
  (param) => {
    const invoke = () => {
      db = new DatabaseWrapper(param);
    };
    expect(invoke).toThrow(Error('dw-data: constructor requires an environment string param.'));
  }
);

test.each(['thing', 'another', 'qa', 'prod', 'dev'])(
  'should throw if environment param string is not \'development\', \'staging\', \'test\' or \'production\'',
  (param) => {
    const invoke = () => {
      db = new DatabaseWrapper(param);
    };
    expect(invoke).toThrow(Error('dw-data: constructor environment must be \'development\', \'staging\', \'test\' or \'production\'.'));
  }
);

test.each(['development', 'test', 'staging', 'production'])(
  'should not throw if environment param is \'development\', \'staging\', \'test\' or \'production\'.',
  (param) => {
    const invoke = () => {
      db = new DatabaseWrapper(param);
    };
    expect(invoke).not.toThrowError();
  }
);

test('export should return an object', () => {
  db = new DatabaseWrapper('development');
  expect(typeof db).toBe('object');
});

test('export should have an init method', () => {
  db = new DatabaseWrapper('development');
  expect(db.init).toBeDefined();
});

test('should start with isInitialized as false and environment as constructor param', () => {
  const exampleConstructorParam = 'development';
  db = new DatabaseWrapper(exampleConstructorParam);
  expect(db._isInitialized).toEqual(false);
  expect(db._environment).toEqual(exampleConstructorParam);
  expect(db._store).toBeNull();
});

test('should set store database url to the correct environment for development', async () => {
  db = new DatabaseWrapper('development');
  try {
    await db.init();
    expect(db._store.databaseUrl).toEqual('https://district-watch-dev.firebaseio.com/');
  } catch (error) {
    throw new Error(error);
  }
});

test('should use process env variables for credentials and databaseUrl if production', async () => {
  db = new DatabaseWrapper('production');
  sandbox.stub(admin.credential, 'cert').callsFake(() => {
    return {};
  });
  process.env.FIREBASE_PROJECT_ID = 'test_project_id';
  process.env.FIREBASE_CLIENT_EMAIL = 'test_client_email';
  process.env.FIREBASE_PRIVATE_KEY = 'test_private_key';
  process.env.FIREBASE_DATABASE_URL = 'https://www.google.com';
  try {
    await db.init();
    expect(db._store.databaseUrl).toEqual('https://www.google.com');
  } catch (error) {
    throw new Error(error);
  }
});

describe('init', () => {
  test('should be a promise', async () => {
    db = new DatabaseWrapper('development');
    expect(db.init()).resolves.toBeDefined();
  });

  test('should change isInitialized to true on init', async () => {
    db = new DatabaseWrapper('development');
    await db.init();
    expect(db._isInitialized).toEqual(true);
  });

  test('should call logger.log', async () => {
    db = new DatabaseWrapper('development');
    jest.spyOn(db._logger, 'log');
    await db.init();
    expect(db._logger.log).toHaveBeenCalled();
  });

  test('should not change isInitialized to true if _store creation fails', async () => {
    db = new DatabaseWrapper('production');
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return {};
    });
    process.env.FIREBASE_PROJECT_ID = 'test_project_id';
    process.env.FIREBASE_CLIENT_EMAIL = 'test_client_email';
    process.env.FIREBASE_PRIVATE_KEY = 'test_private_key';
    // missing a required DatabaseUrl key, to make the Firebase class instantiation fail.
    try {
      await db.init();
    } catch (error) {
      expect(db._isInitialized).toEqual(false);
    }
  });

  test('should set store to instance of firebase admin', async () => {
    db = new DatabaseWrapper('production');
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return {};
    });
    process.env.FIREBASE_PROJECT_ID = 'test_project_id';
    process.env.FIREBASE_CLIENT_EMAIL = 'test_client_email';
    process.env.FIREBASE_PRIVATE_KEY = 'test_private_key';
    process.env.FIREBASE_DATABASE_URL = 'https://www.google.com';
    try {
      await db.init();
      expect(db._store).not.toBeNull();
    } catch (error) {
      throw new Error(error);
    }
  });

  test('should resolve with success message once connection is complete', async () => {
    db = new DatabaseWrapper('production');
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return {};
    });
    process.env.FIREBASE_PROJECT_ID = 'test_project_id';
    process.env.FIREBASE_CLIENT_EMAIL = 'test_client_email';
    process.env.FIREBASE_PRIVATE_KEY = 'test_private_key';
    process.env.FIREBASE_DATABASE_URL = 'https://www.google.com';
    try {
      const result = await db.init();
      expect(result).toEqual('connection to database successful.');
    } catch (error) {
      throw new Error(error);
    }
  });

  test('should reject with error if error happened', async () => {
    db = new DatabaseWrapper('production');
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return null;
    });
    jest.spyOn(db._logger, 'log');
    try {
      await db.init();
      throw new Error('promise for this test did not throw an error and should have!');
    } catch (error) {
      expect(db._logger.log).toHaveBeenCalled();
      expect(error).toEqual(Error('dw-data: firebase credential is not valid.'));
    }
  });
});

describe('query', () => {
  beforeEach(() => {
    db = new DatabaseWrapper('development');
    db._isInitialized = true;
  });
  test('query should be a promise that resolves or rejects if isInitialized is true or false', async () => {
    await db.init();
    jest.spyOn(db._store, 'query').mockImplementationOnce(() => Promise.resolve('donezo'));
    const result = await db.query('something', [{
      propertyName: 'players',
      operand: '==',
      propertyValue: 'Pele'
    }]);
    expect(result).toEqual('donezo');
    db._isInitialized = false;
    try {
      await db.query('something', [{}]);
    } catch (error) {
      expect(error).toEqual(Error('dw-data: Database is not initialized.'));
    }
  });

  test.each([{},
    [], 4, undefined, true, null
  ])(
    'should reject first param named collection when passing %i',
    async (param) => {
      try {
        await db.query(param);
      } catch (error) {
        expect(error).toEqual(Error('dw-data: query() method requires collection to be a string.'));
      }
    }
  );

  test.each(['string', 4, {}, true])(
    'should reject second param named filter when passing %i',
    async (param) => {
      try {
        await db.query('testcollection', param);
      } catch (error) {
        expect(error).toEqual(Error('dw-data: query() method requires param filter to be an array.'));
      }
    }
  );

  test.each([undefined, null])(
    'should try getGollection when second param, filter, is null or undefined %i',
    async (param) => {
      await db.init();
      jest.spyOn(db._store, 'getCollection').mockImplementationOnce(() => Promise.resolve('donezo'));
      try {
        const result = await db.query('testcollection', param);
        expect(result).toEqual('donezo');
      } catch (error) {
        throw new Error('should not have thrown in this test!');
      }
    }
  );

  test('filter param should be optional and not error if not passed', async () => {
    await db.init();
    jest.spyOn(db._store, 'getCollection').mockImplementationOnce(() => Promise.resolve('donezo'));
    try {
      const invoke = async () => {
        await db.query('testcollection');
      }
      expect(invoke).not.toThrow();
    } catch (error) {
      throw new Error('filter param should be optional, this throw should not have happened!');
    }
  });

  test('should throw if database is not initialized', async () => {
    try {
      db._isInitialized = false;
      await db.query('testcollection', [{}]);
      throw new Error('this test should throw if database is not initialized, but it does not!');
    } catch (error) {
      expect(error).toEqual(Error('dw-data: Database is not initialized.'));
    }
  });

  test('should call store.query method with filter', async () => {
    const filterMock = {
      propertyName: 'test',
      operand: '==',
      propertyValue: 'test'
    }
    try {
      await db.init();
      jest.spyOn(db._store, 'query').mockImplementationOnce(() => 'full process done.');
      const spy = jest.spyOn(db._store, 'query');
      const results = await db.query('testcollection', [filterMock]);
      expect(spy).toHaveBeenCalledWith('testcollection', [filterMock]);
      expect(results).toEqual('full process done.');
    } catch (error) {
      throw new Error('should not have thrown in this test!');
    }
  });
});

describe('getOne', () => {
  test('should exist', () => {
    expect(db.getOne).toBeDefined();
  });

  test('should throw if database is not initialized', async () => {
    db = new DatabaseWrapper('development');
    await db.init();
    db._isInitialized = false;
    try {
      await db.getOne('thing');
    } catch (error) {
      expect(error).toEqual(Error('dw-data: Database is not initialized.'));
    }
  });

  test.each([undefined, null, 3, true, [], {}, new Date()])(
    'should throw when id param is not a string',
    async (param) => {
      try {
        await db.init();
        await db.getOne('test', param);
      } catch (error) {
        expect(error).toEqual(Error('dw-data: getOne() method requires id to be a string.'));
      }
    }
  );

  test.each([undefined, null, 3, true, [], {}, new Date()])(
    'should throw when collection param is not a string',
    async (param) => {
      try {
        await db.init();
        await db.getOne(param, '12345');
      } catch (error) {
        expect(error).toEqual(Error('dw-data: getOne() method requires collection to be a string.'));
      }
    }
  );

  test('should call _store.getById with id string', async () => {
    const mockIdData = {
      testProp: 'testValue'
    };
    db = new DatabaseWrapper('development');
    await db.init();
    jest.spyOn(db._store, 'getById').mockImplementationOnce(() => Promise.resolve(mockIdData));
    try {
      const result = await db.getOne('test', 'test');
      expect(result).toEqual(mockIdData);
    } catch (error) {
      throw new Error('should not have been an error thrown for this test!');
    }
  });

  test('should return error if store getById fails', async () => {
    db = new DatabaseWrapper('development');
    await db.init();
    jest.spyOn(db._store, 'getById').mockImplementationOnce(() => Promise.reject(Error('some firebase reason')));
    jest.spyOn(db._logger, 'log');
    try {
      await db.getOne('test', '1234');
    } catch (error) {
      expect(db._logger.log).toHaveBeenCalled();
      expect(error).toEqual(Error('some firebase reason'));
    }
  });

});

describe('uploadFile', () => {
  test('should exist', () => {
    expect(db.uploadFile).toBeDefined();
  });

  test('should be a promise that resolves with file object if true', async () => {
    const mockResponse = {
      response: 'some response',
      fileData: 'some data'
    };
    db = new DatabaseWrapper('development');
    await db.init();
    jest.spyOn(db._store, 'uploadToBucket').mockImplementationOnce(() => Promise.resolve(mockResponse));
    try {
      const response = await db.uploadFile('https://www.google.com', '1234.pdf', 'raw');
      expect(response).toEqual(mockResponse);
    } catch (error) {
      throw new Error('should not have failed in this test, check it');
    }
  });

  test('should be a promise that rejects if error happens', async () => {
    db = new DatabaseWrapper('development');
    await db.init();
    jest.spyOn(db._store, 'uploadToBucket').mockImplementationOnce(() => Promise.reject('some error'));
    try {
      await db.uploadFile('https://www.google.com', '1234.docx', 'raw');
      throw new Error('should have failed in this test, check it');
    } catch (error) {
      expect(error).toEqual('some error');
    }
  });
});

describe('uploadLocalFile', () => {
  test('should exist', () => {
    expect(db.uploadLocalFile).toBeDefined();
  });

  test('should be a promise that resolves with file object if true', async () => {
    const mockResponse = {
      response: 'some response',
      fileData: 'some data'
    };
    db = new DatabaseWrapper('development');
    await db.init();
    jest.spyOn(db._store, 'uploadToBucket').mockImplementationOnce(() => Promise.resolve(mockResponse));
    try {
      const response = await db.uploadLocalFile('/1234.pdf', '1234.pdf', 'assembled');
      expect(response).toEqual(mockResponse);
    } catch (error) {
      throw new Error('should not have failed in this test, check it');
    }
  });

  test('should be a promise that rejects if error happens', async () => {
    db = new DatabaseWrapper('development');
    await db.init();
    jest.spyOn(db._store, 'uploadToBucket').mockImplementationOnce(() => Promise.reject('some error'));
    try {
      await db.uploadLocalFile('/1234.docx', '1234.docx', 'assembled');
      throw new Error('should have failed in this test, check it');
    } catch (error) {
      expect(error).toEqual('some error');
    }
  });
});

describe('createOne', () => {
  test('should exist', () => {
    expect(db.createOne).toBeDefined();
  });

  test('should be a promise that resolves or rejects if isInitialized is true or false', async () => {
    db = new DatabaseWrapper('development');
    await db.init();
    jest.spyOn(db._store, 'createOne').mockImplementationOnce(() => Promise.resolve('done.'));
    expect(await db.createOne('test', {
      id: '1234'
    })).toBe('done.');
    db._isInitialized = false;
    try {
      await db.createOne('test', {}, '1234');
    } catch (error) {
      expect(error).toEqual(Error('dw-data: Database is not initialized.'));
    }
  });

  test('should not give error if db is initialized', async () => {
    db = new DatabaseWrapper('development');
    await db.init();
    jest.spyOn(db._store, 'createOne').mockImplementationOnce(() => Promise.resolve('done.'));
    expect(await db.createOne('test', {
      id: '1234'
    })).toBe('done.');
  });

  test('should give error is isInitialized becomes false', async () => {
    db = new DatabaseWrapper('development');
    await db.init();
    jest.spyOn(db._store, 'createOne').mockImplementationOnce(() => Promise.resolve('done.'));
    expect(await db.createOne('test', {
      id: '1234'
    })).toBe('done.');
    db._isInitialized = false;
    try {
      await db.createOne('test', {}, '1234');
    } catch (error) {
      expect(error).toEqual(Error('dw-data: Database is not initialized.'));
    }
  });

  test('should call _store.createOne with collection, data, id', async () => {
    const mockIdData = {
      testProp: 'testValue'
    };
    db = new DatabaseWrapper('development');
    await db.init();
    const spy = jest.spyOn(db._store, 'createOne').mockImplementation(() => Promise.resolve(mockIdData));
    try {
      await db.createOne('test', {
        id: '1234'
      });
      expect(spy).toHaveBeenCalledWith('test', expect.any(Object), '1234');
    } catch (error) {
      throw new Error('should not have been an error thrown for this test!');
    }
  });

  test('should call _store.createOne with only collection and data if no id is passed in', async () => {
    const mockIdData = {
      testProp: 'testValue'
    };
    db = new DatabaseWrapper('development');
    await db.init();
    const spy = jest.spyOn(db._store, 'createOne').mockImplementation(() => Promise.resolve(mockIdData));
    try {
      await db.createOne('test', {});
      expect(spy).toHaveBeenCalledWith('test', expect.any(Object));
    } catch (error) {
      throw new Error('should not have been an error thrown for this test!');
    }
  });

  test('should throw if _store.createOne returns error from firebase', async () => {
    db = new DatabaseWrapper('development');
    await db.init();
    jest.spyOn(db._store, 'createOne').mockImplementation(() => Promise.reject('some error'));
    jest.spyOn(db._logger, 'log');
    try {
      await db.createOne('test', {});
      throw new Error('should not have been an error thrown for this test!');
    } catch (error) {
      expect(db._logger.log).toHaveBeenCalled();
      expect(error).toBe('some error')
    }
  });


});

describe('validateInitialized', () => {
  test('should exist', () => {
    expect(db.validateInitialized).toBeDefined();
  });

  test('should start false, and become true after init', async () => {
    db = new DatabaseWrapper('development');
    const invoke = () => {
      db.validateInitialized();
    }
    expect(invoke).toThrow(Error('dw-data: Database is not initialized.'));
    await db.init();
    expect(invoke).toBeTruthy();
  });

});

describe('updateOne', () => {
  test('should exist', () => {
    expect(db.updateOne).toBeDefined();
  });

  test('should throw if database is not initialized', async () => {
    db = new DatabaseWrapper('development');
    await db.init();
    db._isInitialized = false;
    try {
      await db.updateOne('thing');
      throw new Error('should not have been an error thrown for this test!');
    } catch (error) {
      expect(error).toEqual(Error('dw-data: Database is not initialized.'));
    }
  });

  test.each([undefined, null, 3, true, [], {}, new Date()])(
    'should throw when collection param is not a string',
    async (param) => {
      try {
        await db.init();
        await db.updateOne(param, {});
        throw new Error('should not have been an error thrown for this test!');
      } catch (error) {
        expect(error).toEqual(Error('dw-data: updateOne() method requires collection to be a string.'));
      }
    }
  );

  test.each([undefined, null, 3, true, [], 'test', new Date()])(
    'should throw when data param is not an object',
    async (param) => {
      try {
        await db.init();
        await db.updateOne('test', param);
        throw new Error('should not have been an error thrown for this test!');
      } catch (error) {
        expect(error).toEqual(Error('dw-data: updateOne() method requires data to be an object.'));
      }
    }
  );

  test.each([undefined, null, 3, true, [], {}, new Date()])(
    'should throw when id param is not a string',
    async (param) => {
      try {
        await db.init();
        await db.updateOne('test', {}, param);
        throw new Error('should not have been an error thrown for this test!');
      } catch (error) {
        expect(error).toEqual(Error('dw-data: updateOne() method requires id to be a string.'));
      }
    }
  );

  test('should call _store.updateOne with collection, data, and id', async () => {
    const mockData = {
      testProp: 'testValue'
    };
    db = new DatabaseWrapper('development');
    await db.init();
    const spy = jest.spyOn(db._store, 'updateOne').mockImplementation(() => Promise.resolve('done'));
    try {
      await db.updateOne('test', mockData, '1234');
      expect(spy).toHaveBeenCalledWith('test', mockData, '1234');
    } catch (error) {
      throw new Error('should not have been an error thrown for this test!');
    }
  });

  test('should return error if store getById fails', async () => {
    db = new DatabaseWrapper('development');
    await db.init();
    jest.spyOn(db._store, 'updateOne').mockImplementation(() => Promise.reject('done'));
    try {
      await db.updateOne('test', {}, true);
      throw new Error('should not have been an error thrown for this test!');
    } catch (error) {
      expect(error).toEqual(Error('dw-data: updateOne() method requires id to be a string.'));
    }
  });

  test('should return error if store _updateOne fails', async () => {
    db = new DatabaseWrapper('development');
    await db.init();
    jest.spyOn(db._store, 'updateOne').mockImplementation(() => Promise.reject('some error'));
    jest.spyOn(db._logger, 'log');
    try {
      await db.updateOne('test', {}, '1234');
      throw new Error('should not have been an error thrown for this test!');
    } catch (error) {
      expect(db._logger.log).toHaveBeenCalled();
      expect(error).toEqual('some error');
    }
  });
});

describe('deleteOne', () => {
  test('should exist', () => {
    expect(db.deleteOne).toBeDefined();
  });

  test('should throw if database is not initialized', async () => {
    db = new DatabaseWrapper('development');
    await db.init();
    db._isInitialized = false;
    try {
      await db.deleteOne('thing');
      throw new Error('should not have been an error thrown for this test!');
    } catch (error) {
      expect(error).toEqual(Error('dw-data: Database is not initialized.'));
    }
  });

  test.each([undefined, null, 3, true, [], {}, new Date()])(
    'should throw when collection param is not a string',
    async (param) => {
      try {
        await db.init();
        await db.deleteOne(param, '1234');
        throw new Error('should not have been an error thrown for this test!');
      } catch (error) {
        expect(error).toEqual(Error('dw-data: deleteOne() method requires collection to be a string.'));
      }
    }
  );

  test.each([undefined, null, 3, true, [], {}, new Date()])(
    'should throw when id param is not a string',
    async (param) => {
      try {
        await db.init();
        await db.deleteOne('test', param);
        throw new Error('should not have been an error thrown for this test!');
      } catch (error) {
        expect(error).toEqual(Error('dw-data: deleteOne() method requires id to be a string.'));
      }
    }
  );

  test('should call _store.deleteOne with collection, data, and id', async () => {
    db = new DatabaseWrapper('development');
    await db.init();
    const spy = jest.spyOn(db._store, 'deleteOne').mockImplementation(() => Promise.resolve('done'));
    try {
      await db.deleteOne('test', '1234');
      expect(spy).toHaveBeenCalledWith('test', '1234');
    } catch (error) {
      throw new Error('should not have been an error thrown for this test!');
    }
  });

  test('should return error if store deleteOne fails', async () => {
    db = new DatabaseWrapper('development');
    await db.init();
    jest.spyOn(db._store, 'deleteOne').mockImplementation(() => Promise.reject('done'));
    jest.spyOn(db._logger, 'log');
    try {
      await db.deleteOne('test', '12345');
      throw new Error('should not have been an error thrown for this test!');
    } catch (error) {
      expect(db._logger.log).toHaveBeenCalled();
      expect(error).toEqual('done');
    }
  });

});
import * as admin from 'firebase-admin';
import { Utils } from 'dw-utils';
const { Firebase } = require('../src/firebase.js');
const sinon = require('sinon');
const utils = new Utils();
let firebase = null;
let sandbox = null;

beforeEach(() => {
  sandbox = sinon.createSandbox();
});

afterEach(() => {
  sandbox.restore();
});

test('will throw if constructed without object', () => {
  expect(() => {
    new Firebase()
  }).toThrowError(Error('dw-data: constructor requires an options object with credential, databaseUrl, and optional jsonFilePath.'));
});

test('should take an options object as class constructor param', () => {
  const filePath = './__mocks__/mock-firebase-key.json';
  sandbox.stub(admin.credential, 'cert').callsFake(() => {
    return JSON.parse(require('fs').readFileSync(filePath, 'utf8'));
  });
  sandbox.stub(admin, 'firestore').callsFake(() => {
    return {
      collections: {}
    };
  });
  expect(() => {
    new Firebase({
      jsonFilePath: filePath,
      databaseUrl: 'https://www.google.com'
    })
  }).not.toThrowError();
});

test('firebase to be instance of Firebase class', () => {
  const filePath = './__mocks__/mock-firebase-key.json';
  sandbox.stub(admin.credential, 'cert').callsFake(() => {
    return JSON.parse(require('fs').readFileSync(filePath, 'utf8'));
  });
  sandbox.stub(admin, 'firestore').callsFake(() => {
    return {
      collections: {}
    };
  });
  firebase = new Firebase({
    jsonFilePath: filePath,
    databaseUrl: 'https://www.google.com'
  });
  expect(firebase).toBeInstanceOf(Firebase);
});

test('should return an object', () => {
  sandbox.stub(admin, 'firestore').callsFake(() => {
    return {
      collections: {}
    };
  });
  sandbox.stub(utils, 'isValidUrl').callsFake(() => {
    return true;
  });
  const filePath = './__mocks__/mock-firebase-key.json';
  sandbox.stub(admin.credential, 'cert').callsFake(() => {
    return JSON.parse(require('fs').readFileSync(filePath, 'utf8'));
  });
  firebase = new Firebase({
    jsonFilePath: filePath,
    credential: {},
    databaseUrl: 'https://www.google.com'
  });
  expect(typeof firebase).toBe('object');
});

test('should set options if object passed to constructor', () => {
  sandbox.stub(admin, 'firestore').callsFake(() => {
    return {
      collections: {}
    };
  });
  firebase = new Firebase({
    jsonFilePath: './__mocks__/mock-firebase-key.json',
    credential: undefined,
    databaseUrl: 'http://www.google.com'
  });
  expect(firebase.jsonFilePath).toEqual('./__mocks__/mock-firebase-key.json');
  expect(firebase.credential).toBeUndefined();
  expect(firebase.databaseUrl).toEqual('http://www.google.com');
});

test('should throw if options jsonFilePath is not a valid file', () => {
  sandbox.stub(admin, 'firestore').callsFake(() => {
    return {
      collections: {}
    };
  });
  const invoke = () => {
    firebase = new Firebase({
      jsonFilePath: 'invalidFilePath',
      databaseUrl: 'https://www.google.com'
    });
  }
  expect(invoke).toThrowError(Error('dw-data: jsonFilePath must be a valid file path.'));
});

test('should throw if something other than a string is passed as jsonFilePath', () => {
  const invoke = () => {
    firebase = new Firebase({
      jsonFilePath: {
        test: 'badParam'
      },
      databaseUrl: 'https://www.google.com'
    });
  }
  expect(invoke).toThrowError(Error('dw-data: jsonFilePath must be a string.'));
});

describe('validateFirebaseCredential', () => {
  test('should throw if validateFirebaseCredential throws on firebase admin.credential.cert method', () => {
    const filePath = './__mocks__/mock-firebase-key.json';
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return null;
    });
    firebase = new Firebase({
      jsonFilePath: filePath,
      databaseUrl: 'https://www.google.com'
    });
    firebase.jsonFilePath = null;
    const invoke = () => {
      firebase.validateFirebaseCredential({});
    }
    expect(invoke).toThrowError(Error('dw-data: firebase credential is not valid.'));
  });

  test('should return credential if it is a valid cert', () => {
    sandbox.stub(admin, 'firestore').callsFake(() => {
      return {
        collections: {}
      };
    });
    const filePath = './__mocks__/mock-firebase-key.json';
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return {};
    });
    firebase = new Firebase({
      jsonFilePath: filePath,
      databaseUrl: 'https://www.google.com'
    });
    firebase.jsonFilePath = null;
    expect(firebase.validateFirebaseCredential('shouldPassWhatever')).toEqual({});
  });

  test('should throw error if passes admin credential cert method still is not valid', () => {
    const filePath = './__mocks__/mock-firebase-key.json';
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return null;
    });
    firebase = new Firebase({
      jsonFilePath: filePath,
      databaseUrl: 'https://www.google.com'
    });
    firebase.jsonFilePath = null;
    const invoke = () => {
      firebase.validateFirebaseCredential('something');
    };
    expect(invoke).toThrowError(Error('dw-data: firebase credential is not valid.'));
  });

  test('should not throw if this.jsonFilePath is valid', () => {
    const invoke = () => {
      firebase = new Firebase({
        jsonFilePath: './__mocks__/mock-firebase-key.json',
        databaseUrl: 'https://www.google.com'
      });
    };
    expect(invoke).not.toThrowError();
  });

});

describe('validateDatabaseUrl', () => {
  test('should return url when param is a url', () => {
    sandbox.stub(admin, 'firestore').callsFake(() => {
      return {
        collections: {}
      };
    });
    const filePath = './__mocks__/mock-firebase-key.json';
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return JSON.parse(require('fs').readFileSync(filePath, 'utf8'));
    });
    firebase = new Firebase({
      jsonFilePath: filePath,
      databaseUrl: 'https://www.google.com'
    });
    expect(firebase.validateDatabaseUrl('https://www.google.com')).toBe('https://www.google.com');
  });

  test.each([{},
    [], 4, undefined, null
  ])(
    'should throw when param is not a string',
    (url) => {
      sandbox.stub(admin.credential, 'cert').callsFake(() => {
        return {};
      });
      firebase = new Firebase({
        databaseUrl: 'https://www.google.com',
        credential: {}
      });
      const invoke = () => {
        firebase.validateDatabaseUrl(url);
      };
      expect(invoke).toThrowError(Error('dw-data: validateDatabaseUrl() method requires url to be a string.'));
    }
  );

  test('should throw error when a malformed firebase database url', () => {
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return {};
    });
    sandbox.stub(utils, 'isValidUrl').callsFake(() => {
      return false;
    });
    const invoke = () => {
      firebase = new Firebase({
        databaseUrl: 'blahnoturl',
      });
    };
    expect(invoke).toThrowError(Error('dw-data: not a valid databaseUrl to a firebase database.'));
  });
});

describe('createDatabase', () => {
  test('should return an object with a .firestore method on it', () => {
    sandbox.stub(admin, 'firestore').callsFake(() => {
      return {
        collections: {}
      };
    });
    const filePath = './__mocks__/mock-firebase-key.json';
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return JSON.parse(require('fs').readFileSync(filePath, 'utf8'));
    });
    sandbox.stub(utils, 'isValidUrl').callsFake(() => {
      return true;
    });
    firebase = new Firebase({
      jsonFilePath: filePath,
      databaseUrl: 'https://www.google.com',
    });
    expect(firebase.db.collection).toBeDefined();
  });

  test('should create instance with json file if willUseJsonFile is true', () => {
    sandbox.stub(admin, 'firestore').callsFake(() => {
      return {
        collections: {}
      };
    });
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return {};
    });
    sandbox.stub(utils, 'isValidUrl').callsFake(() => {
      return true;
    });
    firebase = new Firebase({
      jsonFilePath: undefined,
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    expect(admin.initializeApp).toHaveBeenCalledWith({
      credential: {},
      databaseURL: 'https://www.google.com'
    });
  });

  test('should call require if jsonFilePath is defined', () => {
    sandbox.stub(admin, 'firestore').callsFake(() => {
      return {
        collections: {}
      };
    });
    const filePath = './__mocks__/mock-firebase-key.json';
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return JSON.parse(require('fs').readFileSync(filePath, 'utf8'));
    });
    sandbox.stub(utils, 'isValidUrl').callsFake(() => {
      return true;
    });
    firebase = new Firebase({
      jsonFilePath: filePath,
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    expect(admin.initializeApp).toHaveBeenCalledWith({
      credential: {
        "project_id": "some string",
        "client_email": "some string",
        "private_key": "some string"
      },
      databaseURL: 'https://www.google.com'
    });
  });

  test('should set storage buckets by environment when development', () => {
    sandbox.stub(admin, 'firestore').callsFake(() => {
      return {
        collections: {}
      };
    });
    const filePath = './__mocks__/mock-firebase-key.json';
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return JSON.parse(require('fs').readFileSync(filePath, 'utf8'));
    });
    sandbox.stub(utils, 'isValidUrl').callsFake(() => {
      return true;
    });
    firebase = new Firebase({
      jsonFilePath: filePath,
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    expect(admin.storage).toHaveBeenCalled();
    expect(admin.storage().bucket).toHaveBeenCalledWith('gs://district-watch-dev-json-docs');
    expect(admin.storage().bucket).toHaveBeenCalledWith('gs://district-watch-dev-raw-docs');
  });
});

test('should set storage buckets by environment when production', () => {
  sandbox.stub(admin, 'firestore').callsFake(() => {
    return {
      collections: {}
    };
  });
  sandbox.stub(admin.credential, 'cert').callsFake(() => {
    return {};
  });
  sandbox.stub(utils, 'isValidUrl').callsFake(() => {
    return true;
  });
  firebase = new Firebase({
    jsonFilePath: undefined,
    credential: {},
    databaseUrl: 'https://www.google.com',
  });
  expect(admin.storage).toHaveBeenCalled();
  expect(admin.storage().bucket).toHaveBeenCalledWith('gs://district-watch-prod-json-docs');
  expect(admin.storage().bucket).toHaveBeenCalledWith('gs://district-watch-prod-raw-docs');
});

test('should disregard this.credential if this.jsonFilePath is valid', () => {
  sandbox.stub(admin, 'firestore').callsFake(() => {
    return {
      collections: {}
    };
  });
  const filePath = './__mocks__/mock-firebase-key.json';
  sandbox.stub(admin.credential, 'cert').callsFake(() => {
    return JSON.parse(require('fs').readFileSync(filePath, 'utf8'));
  });
  sandbox.stub(utils, 'isValidUrl').callsFake(() => {
    return true;
  });
  firebase = new Firebase({
    jsonFilePath: filePath,
    credential: {},
    databaseUrl: 'https://www.google.com',
  });
  expect(admin.initializeApp).toHaveBeenCalledWith({
    credential: {
      "project_id": "some string",
      "client_email": "some string",
      "private_key": "some string"
    },
    databaseURL: 'https://www.google.com'
  });
});

describe('uploadToBucket', () => {
  beforeEach(() => {
    sandbox.stub(admin, 'firestore').callsFake(() => {
      return {
        collections: {}
      };
    });
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return null;
    });
    sandbox.stub(utils, 'isValidUrl').callsFake(() => {
      return true;
    });
  });
  test('should exist', () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    expect(firebase.uploadToBucket).toBeDefined();
  });

  test('should be a promise that resolves with an object when firebase is successful', async () => {
    const mockResponse = 'some response';
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    firebase._rawDocsStorage = {
      upload: () => {
        return Promise.resolve(mockResponse);
      }
    };
    try {
      const result = await firebase.uploadToBucket('https://www.google.com/test.png', '1234.pdf', 'raw');
      expect(result).toEqual(mockResponse);
    } catch (error) {
      throw new Error(error);
    }
  });

  test('should be a reject with error if unsuccessfull', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    firebase._rawDocsStorage = {
      upload: () => {
        return Promise.reject('error happened');
      }
    };
    try {
      await firebase.uploadToBucket('https://www.google.com/test.png', '1234.docx', 'raw');
      throw new Error('Should have thrown but did not, check this');
    } catch (error) {
      expect(error).toEqual(new Error('dw-data: failed trying to upload to bucket: error happened'));
    }
  });
});

describe('getCollection', () => {
  beforeEach(() => {
    sandbox.stub(admin, 'firestore').callsFake(() => {
      return {
        collections: {}
      };
    });
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return null;
    });
    sandbox.stub(utils, 'isValidUrl').callsFake(() => {
      return true;
    });
  });
  test('should exist', () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    expect(firebase.getCollection).toBeDefined();
  });

  test('should be a promise that resolves with an array when firebase API responds with success', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        get: () => {
          return Promise.resolve([])
        }
      };
    });
    try {
      const result = await firebase.getCollection('something');
      expect(result).toEqual([]);
    } catch (error) {
      throw new Error(error);
    }
  });

  test('should unwrap the data from firebase response and return an array', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        get: () => {
          return Promise.resolve([{
            data: () => {
              return {
                propName: 'propValue'
              }
            }
          }, {
            data: () => {
              return {
                propName2: 'propValue2'
              }
            }
          }])
        }
      };
    });
    try {
      const result = await firebase.getCollection('something');
      expect(result).toEqual([{
          propName: 'propValue'
        },
        {
          propName2: 'propValue2'
        }
      ]);
    } catch (error) {
      throw new Error(error);
    }
  });

  test('should be a promise that rejects if error from firebase API is returned', async () => {
    const firebaseAPIError = 'some specific error from firebase';
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        get: () => {
          return Promise.reject(firebaseAPIError)
        }
      }
    });
    jest.spyOn(firebase.logger, 'log');
    try {
      await firebase.getCollection('testCollection');
    } catch (error) {
      expect(firebase.logger.log).toHaveBeenCalled();
      expect(error).toEqual(Error(`dw-data: failed trying to get by collection name: ${firebaseAPIError}`));
    }
  });

  test('should throw if param is not a string', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        get: () => {
          return Promise.resolve([])
        }
      }
    });
    try {
      await firebase.getCollection({});
      throw new Error('getCollection with first param as non-string should fail.');
    } catch (error) {
      expect(error).toEqual(Error('dw-data: getCollection() method requires collection to be a string.'));
    }
    try {
      const result = await firebase.getCollection('something');
      expect(result).toEqual([]);
    } catch (error) {
      throw new Error('should have throw error because parameter is not a string.');
    }
  });
});

describe('getById', () => {
  beforeEach(() => {
    sandbox.stub(admin, 'firestore').callsFake(() => {
      return {
        collections: {}
      };
    });
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return null;
    });
    sandbox.stub(utils, 'isValidUrl').callsFake(() => {
      return true;
    });
  });

  test('should exist', () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            get: () => {
              return Promise.resolve({})
            }
          }
        }
      }
    });
    expect(firebase.getById).toBeDefined();
  });

  test('should be a promise that resolves with an object when firebase API responds with success', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    const mockData = {
      correctData: 'myData'
    };
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            get: () => {
              return Promise.resolve({
                data: () => mockData
              });
            }
          }
        }
      }
    });
    try {
      const result = await firebase.getById('something', 'someId');
      expect(result).toEqual(mockData);
    } catch (error) {
      throw new Error(error);
    }
  });

  test('should be a promise that rejects when firebase API rejects with an error', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    const firebaseAPIError = 'some error happened';
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            get: () => {
              return Promise.reject(firebaseAPIError)
            }
          }
        }
      }
    });
    jest.spyOn(firebase.logger, 'log');
    try {
      await firebase.getById('something', 'someId');
    } catch (error) {
      expect(firebase.logger.log).toHaveBeenCalled();
      expect(error).toEqual(Error(`dw-data: failed trying to get by ID: ${firebaseAPIError}`));
    }
  });

  test('should throw if collection param is not a string', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    const mockData = {
      correctData: 'myData'
    };
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            get: () => {
              return Promise.resolve({
                data: () => mockData
              });
            },
          }
        }
      }
    });
    try {
      await firebase.getById({}, 'someId');
      throw new Error('getById params as non-string should fail.');
    } catch (error) {
      expect(error).toEqual(Error('dw-data: getById() method requires collection to be a string.'));
    }
    try {
      await firebase.getById('someCollection', {});
      throw new Error('getById params as non-string should fail.');
    } catch (error) {
      expect(error).toEqual(Error('dw-data: getById() method requires id to be a string.'));
    }
    try {
      const result = await firebase.getById('somecollection', 'someId');
      expect(result).toEqual(mockData);
    } catch (error) {
      throw new Error('should have throw error because parameters are not a string.');
    }
  });

});

describe('createOne', () => {
  beforeEach(() => {
    sandbox.stub(admin, 'firestore').callsFake(() => {
      return {
        collections: {}
      };
    });
    sandbox.stub(admin.credential, 'cert').callsFake(() => {
      return null;
    });
    sandbox.stub(utils, 'isValidUrl').callsFake(() => {
      return true;
    });
  });

  test('should exist', () => {
    expect(firebase.createOne).toBeDefined();
  });

  test('should return a promise', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            update: () => {
              return Promise.resolve('done');
            },
            get: () => {
              return Promise.resolve({
                data: () => 'done'
              });
            },
            set: () => {
              return Promise.resolve({ id: '1234' });
            }
          }
        },
        add: () => {}
      }
    });
    try {
      const result = await firebase.createOne('testing', {}, '1234');
      expect(result).toBeDefined();
    } catch(error) {
      throw new Error('should not have got here, test should have resolved without throwing.');
    }
  });

  test.each(['string', [], 4, undefined, null, new Date(), true])(
    'should throw when data is not an object',
    async (data) => {
      try {
        await firebase.createOne('sometest', data);
        throw new Error('should not get in here! This test should throw an error that is caught.');
      } catch(error) {
        expect(error).toEqual(Error('dw-data: createOne() method requires data to be an object.'));
      }
    }
  );

  test.each([{}, [], 4, undefined, null, new Date(), true])(
    'should throw when collection is not a string',
    async (collection) => {
      try {
        await firebase.createOne(collection, {});
        throw new Error('should not get in here! This test should throw an error that is caught.');
      } catch(error) {
        expect(error).toEqual(Error('dw-data: createOne() method requires collection to be a string.'));
      }
    }
  );

  test('should allow id param to be optional', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            get: () => {
              return Promise.resolve({
                data: () => 'done'
              });
            },
            update: () => {
              return Promise.resolve('done');
            }
          }
        },
        add: () => {
          return Promise.resolve({ id: '1234' });
        }
      }
    });
    try {
      const result = await firebase.createOne('test', {});
      expect(result).toBeDefined();
    } catch(error) {
      throw new Error('should not have thrown error to the catch in this test.');
    }
  });

  test('should call this.setWithId if called with optional id param, and this.setWithoutId if no id param', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            update: () => {
              return Promise.resolve('done');
            },
            get: () => {
              return Promise.resolve('done');
            }
          }
        },
        add: () => {}
      }
    });
    const spy = jest.spyOn(firebase, 'setWithId').mockImplementation(() => Promise.resolve('done'));
    try {
      await firebase.createOne('testing', {}, '1234');
      expect(spy).toHaveBeenCalledWith('testing', {}, '1234');
    } catch(error) {
      throw new Error('should not have got here, test should have resolved without throwing.');
    }
  });

  test('should call createOne() error if firebase returns error', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            update: () => {
              return Promise.reject('done');
            },
            get: () => {
              return Promise.reject('done');
            }
          }
        },
        add: () => {}
      }
    });
    jest.spyOn(firebase, 'setWithId').mockImplementation(() => Promise.reject('done'));
    jest.spyOn(firebase.logger, 'log');
    try {
      await firebase.createOne('testing', {}, '1234');
      throw new Error('should not have got here, test should have resolved without throwing.');
    } catch(error) {
      expect(firebase.logger.log).toHaveBeenCalled();
      expect(error).toEqual(Error('dw-data: createOne() received an error from firebase: done'));
    }
  });

});

describe('updateOne', () => {
  test('should exist', () => {
    expect(firebase.updateOne).toBeDefined();
  });

  test('should return a promise', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            update: () => {
              return Promise.resolve('done');
            },
            get: () => {
              return Promise.resolve({
                data: () => 'done'
              });
            },
          }
        }
      }
    });
    try {
      const result = await firebase.updateOne('testing', {}, '1234');
      expect(result).toBeDefined();
    } catch(error) {
      throw new Error('should not have got here, test should have resolved without throwing.');
    }
  });

  test.each([{}, [], 4, null, new Date(), true])(
    'should throw when id is not a string',
    async (id) => {
      try {
        await firebase.updateOne('testing', {}, id);
        throw new Error('should not get in here! This test should throw an error that is caught.');
      } catch(error) {
        expect(error).toEqual(Error('dw-data: updateOne() method requires id to be a string.'));
      }
    }
  );

  test.each([{}, [], 4, null, new Date(), true])(
    'should throw when collection is not a string',
    async (collection) => {
      sandbox.stub(firebase.db, 'collection').callsFake(() => {
        return {
          doc: () => {
            return {
              update: () => {
                return Promise.resolve('done');
              }
            }
          }
        }
      });
      try {
        await firebase.updateOne(collection, '1234', {});
        throw new Error('should not get in here! This test should throw an error that is caught.');
      } catch(error) {
        expect(error).toEqual(Error('dw-data: updateOne() method requires collection to be a string.'));
      }
    }
  );

  test.each(['t', [], 4, null, new Date(), true])(
    'should throw when data is not an object',
    async (data) => {
      sandbox.stub(firebase.db, 'collection').callsFake(() => {
        return {
          doc: () => {
            return {
              update: () => {
                return Promise.resolve('done');
              }
            }
          }
        }
      });
      try {
        await firebase.updateOne('test', '1234', data);
        throw new Error('should not get in here! This test should throw an error that is caught.');
      } catch(error) {
        expect(error).toEqual(Error('dw-data: updateOne() method requires data to be an object.'));
      }
    }
  );

  test('should update valid string with id prop', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            update: () => {
              return Promise.resolve('done');
            }
          }
        }
      }
    });
    const spy = jest.spyOn(firebase.db, 'collection');
    const spy2 = jest.spyOn(firebase, 'getById').mockImplementation(() => Promise.resolve('done'));
    try {
      await firebase.updateOne('testing', {}, '1234');
      expect(spy).toHaveBeenCalledWith('testing');
      expect(spy2).toHaveBeenCalledWith('testing', '1234');
    } catch(error) {
      throw new Error(error);
    }
  });

  test('should throw getById() when firebase throws error', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            update: () => {
              return Promise.resolve('done');
            }
          }
        }
      }
    });
    jest.spyOn(firebase.db, 'collection');
    jest.spyOn(firebase, 'getById').mockImplementation(() => Promise.reject('done'));
    jest.spyOn(firebase.logger, 'log');
    try {
      await firebase.updateOne('testing', {}, '1234');
      throw new Error('Should not have got in here, fix test.');
    } catch(error) {
      expect(firebase.logger.log).toHaveBeenCalled();
      expect(error).toEqual('done');
    }
  });
});

describe('setWithoutId', () => {
  test('should exist', () => {
    expect(firebase.setWithoutId).toBeDefined();
  });

  test('should return a promise', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            get: () => {
              return Promise.resolve({
                data: () => 'done'
              });
            },
            update: () => {}
          }
        },
        add: () => {
          return Promise.resolve({ id: '1234' });
        }
      }
    });
    try {
      const result = await firebase.setWithoutId('testing', {});
      expect(result).toBeDefined();
    } catch(error) {
      throw new Error('should not have got here, test should have resolved without throwing.');
    }
  });

  test.each(['string', [], 4, null, new Date(), true])(
    'should throw when data is not an object',
    async (data) => {
      sandbox.stub(firebase.db, 'collection').callsFake(() => {
        return {
          doc: () => {
            return {
              get: () => {
                return Promise.resolve({
                  data: () => 'done'
                });
              },
            }
          }
        }
      });
      try {
        await firebase.setWithoutId('testing', data);
        throw new Error('should not get in here! This test should throw an error that is caught.');
      } catch(error) {
        expect(error).toEqual(Error('dw-data: setWithoutId() method requires data to be an object.'));
      }
    }
  );

  test.each([{}, [], 4, null, new Date(), true])(
    'should throw when collection is not a string',
    async (collection) => {
      sandbox.stub(firebase.db, 'collection').callsFake(() => {
        return {
          doc: () => {
            return {
              get: () => {
                return Promise.resolve('done');
              }
            }
          }
        }
      });
      try {
        await firebase.setWithoutId(collection, {});
        throw new Error('should not get in here! This test should throw an error that is caught.');
      } catch(error) {
        expect(error).toEqual(Error('dw-data: setWithoutId() method requires collection to be a string.'));
      }
    }
  );

  test('should call firebase add() and then this.getById()', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            update: () => {
              return Promise.resolve('done');
            },
            get: () => {}
          }
        },
        add: () => {
          return Promise.resolve({ id: '1234' });
        }
      }
    });
    const spy = jest.spyOn(firebase.db, 'collection');
    const spy2 = jest.spyOn(firebase, 'updateOne');
    const spy3 = jest.spyOn(firebase, 'getById').mockImplementation(() => Promise.resolve('done'));
    try {
      await firebase.setWithoutId('testing', {});
      expect(spy).toHaveBeenCalledWith('testing');
      expect(spy2).toHaveBeenCalledWith('testing', { id: '1234'}, '1234');
      expect(spy3).toHaveBeenCalledWith('testing', '1234');
    } catch(error) {
      throw new Error(error);
    }
  });

  test('should call throw when firebase add() returns error', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            update: () => {
              return Promise.resolve('done');
            },
            get: () => {}
          }
        },
        add: () => {
          return Promise.resolve({ id: '1234' });
        }
      }
    });
    jest.spyOn(firebase.db, 'collection');
    jest.spyOn(firebase, 'updateOne');
    jest.spyOn(firebase, 'getById').mockImplementation(() => Promise.reject('done'));
    jest.spyOn(firebase.logger, 'log');
    try {
      await firebase.setWithoutId('testing', {});
      throw new Error('Should not be here, fix test.');
    } catch(error) {
      expect(firebase.logger.log).toHaveBeenCalled();
      expect(error).toEqual('done');
    }
  });
});

describe('setWithId', () => {
  test('should exist', () => {
    expect(firebase.setWithId).toBeDefined();
  });

  test('should return a promise', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            get: () => {
              return Promise.resolve({
                data: () => 'done'
              });
            },
            set: () => {
              return Promise.resolve({ id: '1234'});
            },
            update: () => {}
          }
        }
      }
    });
    try {
      const result = await firebase.setWithId('testing', {}, '1234');
      expect(result).toBeDefined();
    } catch(error) {
      throw new Error('should not have got here, test should have resolved without throwing.');
    }
  });

  test.each([{}, [], 4, null, new Date(), true])(
    'should throw when id is not a string',
    async (id) => {
      try {
        await firebase.setWithId('testing', {}, id);
        throw new Error('should not get in here! This test should throw an error that is caught.');
      } catch(error) {
        expect(error).toEqual(Error('dw-data: setWithId() method requires id to be a string.'));
      }
    }
  );

  test.each(['string', [], 4, null, new Date(), true])(
    'should throw when data is not an object',
    async (data) => {
      try {
        await firebase.setWithId('testing', data, '1234');
        throw new Error('should not get in here! This test should throw an error that is caught.');
      } catch(error) {
        expect(error).toEqual(Error('dw-data: setWithId() method requires data to be an object.'));
      }
    }
  );

  test.each([{}, [], 4, null, new Date(), true])(
    'should throw when collection is not a string',
    async (collection) => {
      sandbox.stub(firebase.db, 'collection').callsFake(() => {
        return {
          doc: () => {
            return {
              get: () => {
                return Promise.resolve('done');
              }
            }
          }
        }
      });
      try {
        await firebase.setWithId(collection, {}, '1234');
        throw new Error('should not get in here! This test should throw an error that is caught.');
      } catch(error) {
        expect(error).toEqual(Error('dw-data: setWithId() method requires collection to be a string.'));
      }
    }
  );

  test('should call firebase add() and then this.getById()', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            update: () => {
              return Promise.resolve('done');
            },
            get: () => {},
            set: () => {
              return Promise.resolve({ id: '1234'})
            }
          }
        },
        add: () => {
          return Promise.resolve('done');
        }
      }
    });
    const spy = jest.spyOn(firebase.db, 'collection');
    const spy2 = jest.spyOn(firebase, 'updateOne');
    const spy3 = jest.spyOn(firebase, 'getById').mockImplementation(() => Promise.resolve('done'));
    
    try {
      await firebase.setWithId('testing', {}, '1234');
      expect(spy).toHaveBeenCalledWith('testing');
      expect(spy2).toHaveBeenCalledWith('testing', { id: '1234' }, '1234');
      expect(spy3).toHaveBeenCalledWith('testing', '1234');
    } catch(error) {
      throw new Error(error);
    }
  });

  test('should throw when firebase add() throws error', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            update: () => {
              return Promise.resolve('done');
            },
            get: () => {},
            set: () => {
              return Promise.resolve({ id: '1234'})
            }
          }
        },
        add: () => {
          return Promise.resolve('done');
        }
      }
    });
    jest.spyOn(firebase.db, 'collection');
    jest.spyOn(firebase, 'updateOne');
    jest.spyOn(firebase, 'getById').mockImplementation(() => Promise.reject('done'));
    jest.spyOn(firebase.logger, 'log');
    try {
      await firebase.setWithId('testing', {}, '1234');
      throw new Error('Should not be here, fix test.');
    } catch(error) {
      expect(firebase.logger.log).toHaveBeenCalled();
      expect(error).toEqual('done');
    }
  });
});

describe('query', () => {
  test('should exist', () => {
    expect(firebase.query).toBeDefined();
  });

  test('should be a promise', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    jest.spyOn(firebase, 'getCollection').mockImplementation(() => Promise.resolve('done'));
    try {
      const result = await firebase.query('testCollection');
      expect(result).toBeDefined();
    } catch(error) {
      throw new Error('should not have gotten into this catch, check the test.');
    }
  });

  test.each([{}, [], 4, null, new Date(), true])(
    'collection param should be required and a string or throw',
    async (collection) => {
      firebase = new Firebase({
        jsonFilePath: './__mocks__/mock-firebase-key.json',
        credential: {},
        databaseUrl: 'https://www.google.com',
      });
      try {
        await firebase.query(collection);
        throw new Error('should not get here in this test - it should have thrown but did not!');
      } catch(error) {
        expect(error).toEqual(Error('dw-data: query() method requires collection to be a string.'));
      }
    }
  );


  test('should call to this.getCollection if no filter param passed', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    const spy = jest.spyOn(firebase, 'getCollection').mockImplementation(() => Promise.resolve('done'));
    try {
      await firebase.query('books');
      expect(spy).toHaveBeenCalledWith('books');
    } catch(error) {
      throw new Error(error);
    }
  });

  test('should throw if filter param is malformed', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    try {
      await firebase.query('books', [{ propertyName: 'test', operand: '=='}]);
    } catch(error) {
      expect(error).toEqual(Error('dw-data: query() filter object must contain valid string properties: propertyName, operand, and propertyValue.'));
    }
    try {
      await firebase.query('books', [{ propertyName: 'test', propertyValue: 'test'}]);
    } catch(error) {
      expect(error).toEqual(Error('dw-data: query() filter object must contain valid string properties: propertyName, operand, and propertyValue.'));
    }
    try {
      await firebase.query('books', [{ propertyName: 'test'}]);
    } catch(error) {
      expect(error).toEqual(Error('dw-data: query() filter object must contain valid string properties: propertyName, operand, and propertyValue.'));
    }
    try {
      await firebase.query('books', [{}]);
    } catch(error) {
      expect(error).toEqual(Error('dw-data: query() filter object must contain valid string properties: propertyName, operand, and propertyValue.'));
    }
    try {
      await firebase.query('books', []);
    } catch(error) {
      expect(error).toEqual(Error('dw-data: query() filter param must contain at least one object defining filter.'));
    }
  });

  test('should call query.where and then query.get if filter is valid', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    const mockResults = [];
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        where: () => {
          return {
            get: () => Promise.resolve({
              forEach: () => {
                return mockResults;
              }
            })
          }
        }
      }
    });
    try {
      const results = await firebase.query('books', [{ propertyName: 'test', operand: '==', propertyValue: 'test'}]);
      expect(results).toEqual(mockResults);
    } catch(error) {
      throw new Error('should not have thrown into this catch for this test.  have a look at fixing.');
    }
  });

  test('should throw error if firebase returns error', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    const expectedError = 'some firebase error.';
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        get: () => Promise.reject(expectedError)
      }
    });
    jest.spyOn(firebase.logger, 'log');
    try {
      await firebase.query('books');
    } catch(error) {
      expect(firebase.logger.log).toHaveBeenCalled();
      expect(error).toEqual(Error(`dw-data: failed trying to get by collection name: ${expectedError}`));
    }
  });
});

describe('deleteOne', () => {
  test('should exist', () => {
    expect(firebase.deleteOne).toBeDefined();
  });

  test('should be a promise', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            delete: () => Promise.resolve('done')
          }
        }
      }
    });
    try {
      const result = await firebase.deleteOne('testCollection', '1234');
      expect(result).toBeDefined();
    } catch(error) {
      throw new Error('should not have gotten into this catch, check the test.');
    }
  });

  test.each([{}, [], 4, null, new Date(), true])(
    'collection param should be required and a string or throw',
    async (collection) => {
      firebase = new Firebase({
        jsonFilePath: './__mocks__/mock-firebase-key.json',
        credential: {},
        databaseUrl: 'https://www.google.com',
      });
      try {
        await firebase.deleteOne(collection, '1234');
        throw new Error('should not get here in this test - it should have thrown but did not!');
      } catch(error) {
        expect(error).toEqual(Error('dw-data: deleteOne() method requires collection to be a string.'));
      }
    }
  );

  test.each([{}, [], 4, null, new Date(), true])(
    'id param should be required and a string or throw',
    async (id) => {
      firebase = new Firebase({
        jsonFilePath: './__mocks__/mock-firebase-key.json',
        credential: {},
        databaseUrl: 'https://www.google.com',
      });
      try {
        await firebase.deleteOne('test', id);
        throw new Error('should not get here in this test - it should have thrown but did not!');
      } catch(error) {
        expect(error).toEqual(Error('dw-data: deleteOne() method requires id to be a string.'));
      }
    }
  );

  test('should call this.db.collection().delete() params are correct', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            delete: () => Promise.resolve('done')
          }
        }
      }
    });
    try {
      const result = await firebase.deleteOne('books', '1234');
      expect(result).toEqual('done');
    } catch(error) {
      throw new Error('Should not throw into the catch for this test, fix it.');
    }
  });

  test('should throw error if firebase has a problem', async () => {
    firebase = new Firebase({
      jsonFilePath: './__mocks__/mock-firebase-key.json',
      credential: {},
      databaseUrl: 'https://www.google.com',
    });
    const mockError = 'some firebase error';
    sandbox.stub(firebase.db, 'collection').callsFake(() => {
      return {
        doc: () => {
          return {
            delete: () => Promise.reject(mockError)
          }
        }
      }
    });
    jest.spyOn(firebase.logger, 'log');
    try {
      await firebase.deleteOne('books', '1234');
      throw new Error('Should not get here for this test - it should throw into the catch, fix it.');
    } catch(error) {
      expect(firebase.logger.log).toHaveBeenCalled();
      expect(error).toEqual(mockError);
    }
  });
});
module.exports = {
  credential: {
    cert: () => {}
  },
  firestore: () => {
    return {
      settings: () => {},
      collection: () => {}
    };
  },
  initializeApp: jest.fn(options => {

  }),
  storage: jest.fn().mockReturnValue({
    bucket: jest.fn().mockReturnValue({
      upload: () => {}
    })
  })
};
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DatabaseWrapper = undefined;

var _dwLogger = require('dw-logger');

var _firebase = require('./firebase.js');

var _dwUtils = require('dw-utils');

const utils = new _dwUtils.Utils();
/**
 * The database wrapper class for all interaction with dw-data.
 * @param { string } environment  the instance environment, development or production.
 * @throws Will throw an error if environment is not 'development' or 'production'.
 */
/**
 * District Watch Data module.
 * @module dw-data
 * @desc Provides database abstraction for CRUD operations to the District Watch persistent datastore instances.
 */
class DatabaseWrapper {
  constructor(environment) {
    if (utils.getType(environment) !== 'string') {
      throw new Error('dw-data: constructor requires an environment string param.');
    }
    if (environment !== 'development' && environment !== 'test' && environment !== 'production' && environment !== 'staging') {
      throw new Error('dw-data: constructor environment must be \'development\', \'staging\', \'test\' or \'production\'.');
    }
    this._isInitialized = false;
    this._environment = utils.getEnvironment(environment);
    this._store = null;
    this._logger = new _dwLogger.Logger();
  }

  /** Initialize the dw-data module.
   * @returns {Promise} Promise object represents successful or failed initialization.
   * @example
   * 
   *   import { DatabaseWrapper } from 'dw-data';
   *   const db = new DatabaseWrapper();
   * 
   *   //es5
   *   db.init().then(() => {
   *     //now the database instance is initialized and usable.
   *   }).catch(error => {
   *     //oops, there was an error initializing!
   *   });
   * 
   *   //es6
   *   try {
   *     await db.init();
   *   } catch(error) {
   *     // oops, something went wrong
   *   }
   *   //now the database instance is initialized and usable.
   *   
   */
  async init() {
    try {
      if (this._environment === 'development') {
        this._store = new _firebase.Firebase({
          jsonFilePath: './district-watch-dev-firebase-adminsdk.json',
          databaseUrl: 'https://district-watch-dev.firebaseio.com/'
        });
      } else {
        this._store = new _firebase.Firebase({
          jsonFilePath: null,
          credential: {
            'project_id': process.env.FIREBASE_PROJECT_ID,
            'client_email': process.env.FIREBASE_CLIENT_EMAIL,
            'private_key': process.env.FIREBASE_PRIVATE_KEY.replace(/\\n/g, '\n')
          },
          databaseUrl: process.env.FIREBASE_DATABASE_URL
        });
      }
      this._isInitialized = true;
      this._logger.log('star', 'connected to District Watch data.');
      return 'connection to database successful.';
    } catch (error) {
      this._logger.log('debug', 'index > init() threw', error);
      throw error;
    }
  }

  /** Query items by collection and optional filter criteria.
   * @param {string} collection The name of the collection to get.
   * @param { Object[] } [filter] An optional array of objects describing the queries to make to get a subsection of a collection.
   * @param { string } filter[].propertyName The property name to query by.
   * @param { string } filter[].operand The operand to query by, only supports: '==', '>=', '<='.
   * @param { string } filter[].propertyValue The property value for the propertyName.
   * @throws Will throw if database is not initialized, or param type is incorrect.
   * @returns {Promise} Promise object resolves with results or rejects with error message.
   * @example
   * 
   *   //given your instance is on a variable called 'db' 
   *   db.query('books', [
   *       {
   *         propertyName: 'author',
   *         operand: '==',
   *         propertyValue: 'Jane Austen'
   *       }
   *     ]).then(results => {
   *     // do something with the array of results.
   *   });
   */
  async query(collection, filter) {
    this.validateInitialized();
    utils.validateString('dw-data', collection, 'query', 'collection');
    try {
      if (filter && utils.validateFilter('dw-data', filter, 'query')) {
        return await this._store.query(collection, filter);
      } else {
        return await this._store.getCollection(collection);
      }
    } catch (error) {
      this._logger.log('debug', 'index > query() threw', error);
      throw error;
    }
  }

  /** Query single item by id.
   * @param {string} collection The collection to retrieve the item from.
   * @param { string } id The item's id.
   * @throws Will throw if database is not initialized, or param type is incorrect.
   * @returns {Promise} Promise object resolves with item as an object or rejects with error message.
   * @example
   * 
   *   //given your instance is on a variable called 'db' 
   *   db.getOne('books', '1234567').then(result => {
   *     // do something with the json object result.
   *   });
   */
  async getOne(collection, id) {
    this.validateInitialized();
    utils.validateString('dw-data', collection, 'getOne', 'collection');
    utils.validateString('dw-data', id, 'getOne', 'id');
    try {
      return await this._store.getById(collection, id);
    } catch (error) {
      this._logger.log('debug', 'index > getOne() threw', error);
      throw error;
    }
  }

  /** Create a single item.
   * @param {string} collection The collection to create the item in.
   * @param { Object } data A JSON object of the data for the item.
   * @param { string } data.[id] Optional id for the new item's key and the value for an 'id' property.
   * @throws Will throw if database is not initialized, or param type is incorrect.
   * @returns {Promise} Promise object resolves with item as an object or rejects with error message.
   * @example
   * 
   *   //given your instance is on a variable called 'db' 
   *   db.createOne('books', { title: 'My Awesome Book'}, author: 'Billy Bob' }, '1234567').then(result => {
   *     // do something with the json object result.
   *   });
   */
  async createOne(collection, data) {
    this.validateInitialized();
    utils.validateString('dw-data', collection, 'createOne', 'collection');
    utils.validateObject('dw-data', data, 'createOne', 'data');
    try {
      data.createdOn = data.createdOn ? data.createdOn : Date.now();
      data.lastModified = Date.now();
      if (data.id && utils.validateString('dw-data', data.id, 'createOne', 'id')) {
        return await this._store.createOne(collection, data, data.id);
      } else {
        return await this._store.createOne(collection, data);
      }
    } catch (error) {
      this._logger.log('debug', 'index > createOne() threw', error);
      throw error;
    }
  }

  /** Upload a file via url to a specific storage repository by name.
   * @param {string} url The url to the file.
   * @param {string} fileName The file name in the repository.
   * @param {string} locationName The name of the storage repository, only 'raw', 'json', 'assembled' supported.
   * @throws Will throw an error if url is not the correct data type or there is an error in the firebase sdk/infrastructure.
   * @returns {Promise} Promise object resolves with an object of file data or rejects with error message.
   * @example
   * 
   *   //given your instance is on a variable called 'db' 
   *   const response = await db.uploadFile('http://www.africau.edu/images/default/sample.pdf', 'sample2.pdf', 'raw');
   */
  async uploadFile(url, fileName, locationName) {
    if (!utils.isValidUrl(url)) {
      throw new Error('dw-data: uploadFile() method needs a valid url for the url param.');
    }
    if (!utils.isValidUploadFileType(fileName)) {
      throw new Error('dw-data: uploadFile() method needs a valid file type in order to upload: pdf, docx, doc, or txt');
    }
    utils.validateBucketName('dw-data', locationName, 'uploadFile');
    try {
      return await this._store.uploadToBucket(url, fileName, locationName);
    } catch (error) {
      this._logger.log('debug', 'index > uploadFile() threw', error);
      throw error;
    }
  }

  /** Upload a local file via relative path to a specific storage repository by name.
   * @param {string} url The relative local path to the file.
   * @param {string} fileName The file name to be created in the repository.
   * @param {string} locationName The name of the storage repository, only 'raw', 'json', 'assembled' supported.
   * @throws Will throw an error if url is not the correct data type or there is an error in the firebase sdk/infrastructure.
   * @returns {Promise} Promise object resolves with an object of file data or rejects with error message.
   * @example
   * 
   *   //given your instance is on a variable called 'db' 
   *   const response = await db.uploadLocalFile('/sample.pdf', 'sample.pdf', 'assembled');
   */
  async uploadLocalFile(url, fileName, locationName) {
    utils.validateString('dw-data', url, 'uploadLocalFile', 'url');
    if (!utils.isValidUploadFileType(fileName)) {
      throw new Error('dw-data: uploadFile() method needs a valid file type in order to upload: pdf, docx, doc, or txt');
    }
    utils.validateBucketName('dw-data', locationName, 'uploadFile');
    try {
      return await this._store.uploadToBucket(url, fileName, locationName, true);
    } catch (error) {
      this._logger.log('debug', 'index > uploadFile() threw', error);
      throw error;
    }
  }

  /** Download a file by name from a specific storage repository.
   * @param {string} fileName The name of the file to download.
   * @param {string} locationName The name of the storage repository to download from, must be 'raw' or 'json'.
   * @throws Will throw an error url is not the correct data type or there is an error in the firebase sdk/infrastructure.
   * @returns {Promise} Promise object resolves with an object of file data or rejects with error message.
   * @example 
   *    
   *    //given your instance is on a variable called 'db' 
   *    const response = await db.downloadFile('sample.jsonoutput-1-to-2.json', 'json');
   */
  async downloadFile(fileName, locationName) {
    if (!utils.isValidDownloadFileType(fileName)) {
      throw new Error("dw-data: downloadFile() method needs a valid file type in order to download: json");
    }
    utils.validateBucketName("dw-data", locationName, "downloadFile");
    try {
      return await this._store.downloadFromBucket(fileName, locationName);
    } catch (error) {
      this._logger.log('debug', 'index > downloadFile() threw', error);
      throw error;
    }
  }

  /** Get a list of all files in a storage repository.
   * @param {string} locationName The name of the storage repostory.
   * @throws Will throw an error if locationName is not the correct data type or there is an error in the firebase sdk/infrastructure.
   * @returns {Promise} Promise object resolves with an object of file data or rejects with error message.
   * @example
   *
   *  //given your instance is on a variable called 'db'
   *  const response = await db.listAllFiles('json');
   */
  async listAllFiles(locationName) {
    utils.validateBucketName("dw-data", locationName, "listAllFiles");
    try {
      return await this._store.listFilesFromBucket(locationName);
    } catch (error) {
      this._logger.log('debug', 'index > listAllFiles() threw', error);
      throw error;
    }
  }

  /** Delete a single item.
   * @param {string} collection The collection to delete item from.
   * @param {string} id Id for the item to delete.
   * @throws Will throw if database is not initialized, or param type is incorrect.
   * @returns {Promise} Promise object resolves with confirmation string or rejects with error message.
   * @example
   * 
   *   //given your instance is on a variable called 'db' 
   *   db.deleteOne('books', '1234').then(result => {
   *     console.log(result); // logs 'done'
   *   });
   */
  async deleteOne(collection, id) {
    this.validateInitialized();
    utils.validateString('dw-data', collection, 'deleteOne', 'collection');
    utils.validateString('dw-data', id, 'deleteOne', 'id');
    try {
      return await this._store.deleteOne(collection, id);
    } catch (error) {
      this._logger.log('debug', 'index > deleteOne() threw', error);
      throw error;
    }
  }

  /** Updates a single item.
   * @param {string} collection The collection to create the item in.
   * @param {object} data A JSON object of the data for the item.
   * @param {string} id Id for the new item's key and the value for an 'id' property.
   * @throws Will throw if database is not initialized, or param type is incorrect.
   * @returns {Promise} Promise object resolves with item as an object or rejects with error message.
   * @example
   * 
   *   //given your instance is on a variable called 'db' 
   *   db.updateOne('books', { title: 'My Awesome Book'}, author: 'Billy Bob' }, '1234567').then(result => {
   *     // do something with the json object result.
   *   });
   */
  async updateOne(collection, data, id) {
    this.validateInitialized();
    utils.validateString('dw-data', collection, 'updateOne', 'collection');
    utils.validateObject('dw-data', data, 'updateOne', 'data');
    utils.validateString('dw-data', id, 'updateOne', 'id');
    try {
      return await this._store.updateOne(collection, data, id);
    } catch (error) {
      this._logger.log('debug', 'index > updateOne() threw', error);
      throw error;
    }
  }

  /** Validates if database connection is initialized and ready to use.
   * @throws Will throw if database is not initialized.
   * @returns {boolean} True if is initialized, otherwise throws.
   * @example
   * 
   *   //given your instance is on a variable called 'db' 
   *   const isInitialized = db.validateInitialized();
   */
  validateInitialized() {
    if (this._isInitialized === false) {
      throw new Error('dw-data: Database is not initialized.');
    }
    return true;
  }
}
exports.DatabaseWrapper = DatabaseWrapper;
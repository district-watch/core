# District Watch Data

A database wrapper for the District Watch data persistence. Currently in front of Google Cloud Firestore. This is intended to be used as a npm package dependency for any District Watch microservice that needs database access

> NOTE: Development mode requires `district-watch-dev-firebase-adminsdk.json` security key to be in the root of your local project directory.

## Install

Add to the project's `package.json` dependencies, referencing the gitlab repository directly (may need ssh access):

```javascript
  ...
  "dependencies": {
    "dw-data": "git+ssh://git@gitlab.com:district-watch/dw-data.git#1.0.3"  // note the tag for package version...
  }
  ...
```

## Usage

```javascript
const { DatabaseWrapper } = require("dw-data"); // <--- CommonJS, or
import { DatabaseWrapper } from "dw-data"; // <--- ES2015

// Initialize with 'development', which requires you to have `district-watch-dev-firebase-adminsdk.json` key in the root of your project OR with 'production' which expects some environment variables with the production keys.
const db = new DatabaseWrapper("development");

db.init().then(async () => {
  const result = await db.getOne("books", "damTDIBOO9gmpZcRsXkf");
  // result is item = { id: 'damTDIBOO9gmpZcRsXkf', title: 'My Awesome Book', author: 'John Smith' }
  const result = await db.query("books");
  // result is entire collection = [{...},{...},{...}]
  const result = await db.query("books", [
    { propertyName: "author", operand: "==", propertyValue: "John Smith" }
  ]);
  // result is filtered collection set = [{...},{...}]
  const result = await db.createOne("books", {
    id: "damTDIBOO9gmpZcRsXkf",
    title: "My Awesome Book",
    author: "Jimmy Boy"
  });
  // result is new item, with passed-in id = { id: 'damTDIBOO9gmpZcRsXkf', title: 'My Awesome Book', author: 'Jimmy Boy' }
  const result = await db.createOne("books", {
    title: "My Second Book",
    author: "Jimmy Boy"
  });
  // result is new item, with passed-in id = { id: 'someAlphaNumericGuid', title: 'My Second Book', author: 'Jimmy Boy' }
  const result = await db.updateOne(
    "books",
    { title: "The bestest Book", author: "Jimmy Boy" },
    "someAlphaNumericGuid"
  );
  // result is updated item, with passed-in id = { id: 'someAlphaNumericGuid', title: 'The bestest Book', author: 'Jimmy Boy' }
  const result = await db.deleteOne("damTDIBOO9gmpZcRsXkf");
});
```

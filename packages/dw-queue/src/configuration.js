export default {
  // arguments used to establish a connection to a broker
  connection: {
    uri: process.env.CLOUDAMQP_URL
  },

  // define the exchange
  exchanges: [
    {
      name: 'district-watch-retrieve-exchange',
      type: 'direct',
      autoDelete: false,
      durable: true,
      noBatch: true,
      limit: 1
    },
    {
      name: 'district-watch-parse-exchange',
      type: 'direct',
      autoDelete: false,
      durable: true,
      noBatch: true,
      limit: 1
    },
    {
      name: 'district-watch-assemble-exchange',
      type: 'direct',
      autoDelete: false,
      durable: true,
      noBatch: true,
      limit: 1
    },
    {
      name: 'district-watch-index-exchange',
      type: 'direct',
      autoDelete: false,
      durable: true,
      noBatch: true,
      limit: 1
    }
  ],

  // setup the queues
  queues: [
    {
      name: 'dw-retrieve-tasks-queue',
      durable: true,
      subscribe: true,
      noBatch: true,
      limit: 1
    },
    {
      name: 'dw-parse-tasks-queue',
      durable: true,
      subscribe: true,
      noBatch: true,
      limit: 1
    },
    {
      name: 'dw-assemble-tasks-queue',
      durable: true,
      subscribe: true,
      noBatch: true,
      limit: 1
    },
    {
      name: 'dw-index-tasks-queue',
      durable: true,
      subscribe: true,
      noBatch: true,
      limit: 1
    }
  ],

  // binds exchange and queues to one another
  bindings: [
    {
      exchange: 'district-watch-retrieve-exchange',
      target: 'dw-retrieve-tasks-queue',
      keys: ["retrieve"]
    },
    {
      exchange: 'district-watch-parse-exchange',
      target: 'dw-parse-tasks-queue',
      keys: ["parse"]
    },
    {
      exchange: 'district-watch-assemble-exchange',
      target: 'dw-assemble-tasks-queue',
      keys: ["assemble"]
    },
    {
      exchange: 'district-watch-index-exchange',
      target: 'dw-index-tasks-queue',
      keys: ["index"]
    }
  ]
}
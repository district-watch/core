/**
 * District Watch Queue module.
 * @module dw-queue
 * @author Brian Kennedy <bpkennedy@gmail.com>
 * @version 1.0.0
 * @desc Provides a message broker pub/sub abstraction for District Watch applications and apis.
 */
import _ from './env'; // eslint-disable-line no-unused-vars
import rabbot from 'rabbot';
import uuidv4 from 'uuid/v4';
import config from './configuration';
import { Utils } from 'dw-utils';
import { Logger } from 'dw-logger';

/**
 * The transport wrapper class (currently of rabbitMQ) for all message broker interactions.
 */
export class Queue {
  constructor() {
    this.rabbot = rabbot;
    this.config = config;
    this.utils = new Utils();
    this.logger = new Logger();
  }

  /** Initialize the dw-queue module.
   * @returns {Promise} Promise object represents successful or failed initialization.
   * @example
   * 
   *   import { Queue } from 'dw-queue';
   *   const queue = new Queue();
   * 
   *   //es5
   *   queue.initializeConnection().then(() => {
   *     //now the queue is initialized.
   *   }).catch(error => {
   *     //oops, there was an error initializing!
   *   });
   * 
   *   //es6
   *   try {
   *     await queue.initializeConnection();
   *   } catch(error) {
   *     // oops, something went wrong
   *   }
   *   //now the queue is initialized and usable.
   *   
   */
  async initializeConnection() {
    await this.rabbot.configure(this.config);
  }

 /**
  * This callback takes a single parameter, which is the message to be handled. Must call .ack() on each msg in order to get the next one.
  *
  * @callback messageHandler
  * @param { Object } msg The message object to be handled.
  *
  */

  /** Subscribe to a message queue.
   * @param {string} queueName The name of the queue, only accepts queue name of dw-parse-tasks-queue, dw-index-tasks-queue, dw-retrieve-tasks-queue, or dw-assemble-tasks-queue.
   * @param {messageHandler} cb The callback that handles the response.
   * @throws Will throw param data types are incorrect or non-existent queue name is used.
   * @returns {function} function instance
   * @example
   * 
   *   queue.subscribe('dw-parse-tasks-queue', (msg) => {
   *     try {
   *       console.log(msg); // this is the message to do stuff on.
   *       msg.ack();  // ack when you are done.
   *     } catch(error) {
   *       msg.nack(); // you can handle errors as well, to let the exchange know the message should be retried at a later point.
   *     }
   *   });
   */
  subscribe(queueName, callback) {
    this.utils.validateString('dw-queue', queueName, 'subscribe', 'queueName');
    this.utils.validateFunction('dw-queue', callback, 'subscribe', 'callback');
    if (queueName !== 'dw-parse-tasks-queue' && queueName !== 'dw-index-tasks-queue' && queueName !== 'dw-assemble-tasks-queue' && queueName !== 'dw-retrieve-tasks-queue') {
      const error = new Error('dw-queue: subscribe requires queueName string to be either dw-parse-tasks-queue, dw-index-tasks-queue, dw-retrieve-tasks-queue, or dw-assemble-tasks-queue.');
      this.logger.error(error);
      throw error;
    }
    this.rabbot.handle({ queue: queueName }, callback);
    return this.rabbot.startSubscription(queueName);
  }

  /** Unsubscribe to a message queue.
   * @param {string} queueName The name of the queue, only accepts queue name of dw-parse-tasks-queue, dw-index-tasks-queue, or dw-assemble-tasks-queue.
   * @throws Will throw param data types are incorrect or non-existent queue name is used.
   * @example
   * 
   *   queue.unsubscribe('dw-parse-tasks-queue');
   *
   */
  unsubscribe(queueName) {
    this.utils.validateString('dw-queue', queueName, 'unsubscribe', 'queueName');
    if (queueName !== 'dw-parse-tasks-queue' && queueName !== 'dw-index-tasks-queue' && queueName !== 'dw-assemble-tasks-queue') {
      const error = new Error('dw-queue: unsubscribe requires queueName string to be either dw-parse-tasks-queue, dw-index-tasks-queue, or dw-assemble-tasks-queue.');
      this.logger.error(error);
      throw error;
    }
    this.rabbot.stopSubscription(queueName);
  }

  /** Publish a message to the queue.
   * @param {string} exchangeName The name of the exchange, only accepts names of district-watch-parse-exchange, district-watch-index-exchange, or district-watch-assemble-exchange.
   * @param { Object } message A JSON object of the message to publish.
   * @param { string } id An optional correlationId for the message, used for tracking across queues.
   * @throws Will throw param data types are incorrect or non-existent exchange name is used.
   * @returns {Promise} Promise object resolves with a function or rejects with error message.
   * @example
   * 
   *   await queue.publish('district-watch-parse-exchange', { thing: 'someProp1' }, '1234-34456-4234');
   *
   */
  async publish(exchangeName, message, id) {
    this.utils.validateString('dw-queue', exchangeName, 'publish', 'exchangeName');
    this.utils.validateObject('dw-queue', message, 'publish', 'message');
    if (id) {
      this.utils.validateString('dw-queue', id, 'publish', 'id');
    }
    if (exchangeName !== 'district-watch-parse-exchange' && exchangeName !== 'district-watch-index-exchange' && exchangeName !== 'district-watch-assemble-exchange'  && exchangeName !== 'district-watch-retrieve-exchange') {
      const error = new Error('dw-queue: publish requires exchangeName string to be either district-watch-index-exchange, district-watch-parse-exchange, district-watch-retrieve-exchange, or district-watch-assemble-exchange.');
      this.logger.error(error);
      throw error;
    }
    await this.rabbot.publish(exchangeName, {
      body: message,
      correlationId: id ? id : uuidv4(),
      routingKey: this.getRoutingKey(exchangeName),
      type: 'direct',
      persistent: true,
      contentType: 'application/json'
    });
  }

  getRoutingKey(exchangeName) {
    if (exchangeName === 'district-watch-parse-exchange') {
      return 'parse';
    } else if (exchangeName === 'district-watch-assemble-exchange') {
      return 'assemble';
    } else if (exchangeName === 'district-watch-retrieve-exchange') {
      return 'retrieve';
    } else {
      return 'index';
    }
  }

  closeConnection() {
    this.rabbot.closeAll();
    this.rabbot = rabbot;
  }

}

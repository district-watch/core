import { Queue } from '../src/index';
const queue = new Queue();

beforeAll(async () => {
  await queue.initializeConnection();
});

afterAll(async () => {
  //cleanup
  await queue.closeConnection();
});

test('should initialize asynchronously and be closable', async () => {
  try {  
    expect(queue).toBeDefined();
  } catch(error) {
    throw new Error('should not have thrown when starting the package, fix it.');
  }
});

test('should throw error if two named queues are not used', async (done) => {
  const invoke = () => {
    queue.subscribe('dw-xxxxxx', (msg) => {
      msg.ack();
    });
  }
  expect(invoke).toThrow(Error('dw-queue: subscribe requires queueName string to be either dw-parse-tasks-queue, dw-index-tasks-queue, dw-retrieve-tasks-queue, or dw-assemble-tasks-queue.'));
  done();
});

test('should be able to subscribe to a queue and publish a message to it then hear the message come in', async (done) => {
  let responses = [];
  let properties = [];
  try {
    queue.subscribe('dw-parse-tasks-queue', (msg) => {
      responses.push(msg.body);
      properties.push(msg.properties);
      expect(msg.properties.correlationId).toBeDefined();
      msg.ack();
    });
    
    await queue.publish('district-watch-parse-exchange', { thing: 'someProp1' });
    await queue.publish('district-watch-parse-exchange', { thing: 'someProp2' });
    await queue.publish('district-watch-parse-exchange', { thing: 'someProp3' }, '1234');

    expect(responses.length).toEqual(3);
    expect(responses[0].thing).toEqual('someProp1');
    expect(responses[1].thing).toEqual('someProp2');
    expect(responses[2].thing).toEqual('someProp3');
    expect(properties[2].correlationId).toEqual('1234');
    done();
  } catch(error) {
    console.log(error); // eslint-disable-line no-console
    throw new Error('should not have thrown when publishing to queue and subscribing, fix it.');
  }
});
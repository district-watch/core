# District Watch Website

This is meant to give a bit of information to people - there will be a separate web application for the web portion of how District Watch will work (as well as iOS and Android apps)

## Development

1. Run `npm run start` to run webpack and be served on port `8080`.
2. Other commands are the same as the normal dw-tools.

## Deploy

Just add [ci job: dw-website] to the commit message.

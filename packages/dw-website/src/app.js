import firebase from "firebase";
import ReactModal from 'react-modal';
import { Formik } from "formik";
import React from "react";
import { Link } from "react-router";
import {
  Code,
  CustomerQuote, CustomerQuotes,
  Footer,
  Hero,
  HorizontalSplit,
  ImageList, ImageListItem,
  Navbar, NavItem,
  Page,
  PricingPlan, PricingTable,
  Section,
  SignupInline, SignupModal,
  Team,
  TeamMember,
} from "neal-react";
import fire from './fire';

const brandName = "District Watch";
const brand = <span>{brandName}</span>;
let showModal = true;

const businessAddress = (
  <address>
    <strong>{brandName}</strong><br/>
    St. Louis, MO 63033<br/>
    +1 (929) 446-3033
  </address>
);

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      showModal: false,
      subscriberEmail: '',
      subscriberName: '',
      subscriberBeta: false,
      isSpinning: null
    };
  }
  postSubscriber(values, { setSubmitting }) {
    this.setState({ isSpinning: true });
    const newSubscriberKey = fire.database().ref('subscribers').push().key;
    fire.database().ref(`subscribers/${newSubscriberKey}`).update({
      id: newSubscriberKey,
      email: values.email,
      name: values.name,
      isSubscribed: true,
      isBeta: values.beta,
      subscribedDate: firebase.database.ServerValue.TIMESTAMP
    }, (error) => {
      if (error) {
        console.log('firebase subscriber post error: ');
        console.log(error);
      }
      fire.database().ref(`subscribers/${newSubscriberKey}`).once('value').then(snapshot => {
        const newSubscriber = snapshot.val();
        this.setState({ isSpinning: false });
        this.setState({
          subscriberEmail: newSubscriber.email,
          subscriberName: newSubscriber.name,
          subscriberBeta: newSubscriber.isBeta
        });
        this.handleOpenModal().bind(this);
      });
    });
  };

  handleOpenModal(person) {
    this.setState({ showModal: true });
  }

  handleCloseModal() {
    this.setState({
      showModal: false,
      subscriberEmail: '',
      subscriberName: '',
      subscriberBeta: false
    });
  }

  isSpinning() {
    const element = this.state.isSpinning ? (<i className="fa fa-spinner spinnit"></i>) : null; 
    return element;
  }

  render() {
    return (
    <Page>
      <div className="logo">
        <img src="images/logoFinalMaybe.png" alt="District Watch logo"/>
      </div>
      <Navbar brand={brand}>
        {/* This is hiding the hamburger manu until we have need of a nav... */}
        {/* <NavItem><Link to="Home" className="nav-link">Home</Link></NavItem> */}
      </Navbar>

      <Hero backgroundImage="images/districts.png"
        className="text-xs-center hero">
        <h1 className="display-4">Change Your City</h1>
        <p className="lead">Now you can figure out what's going on in your municipality and how your council is voting.  District Watch makes it simple to keep your neighborhood safer, more rewarding, more inclusive, and more united.</p>
        {/* <p>
          <a href="https://github.com/dennybritz/neal-react" target="_blank" rel="noopener noreferrer" className="btn btn-white">
            Sign Up
          </a>
        </p> */}
      </Hero>

      <Section className="subhero">
        <h2 className="subhero-header">iOS and Android apps</h2>
        <ImageList centered>
          <ImageListItem src="images/iPhone.png" alt="District Watch iPhone app"/>
          <ImageListItem src="images/AndroidPhone.png" alt="District Watch Android app"/>
        </ImageList>
      </Section>

      <Section className="features">
        <h2 className="subhero-header">Transform your Community</h2>
        <HorizontalSplit padding="md">
          <div className="split-item">
            <div className="item-image">
              <img src="images/map.png" alt="Find your municipality's publicly available documents and information."/>
            </div>
            <p className="lead">Find Local Information</p>
            <p>District Watch collects public records from your municipality and puts them in one searchable place for you to stay informed.</p>
          </div>
          <div className="split-item">
            <div className="item-image">
              <img src="images/location.png" alt="View the voting record of your municipality's elected councilmen and aldermen."/>
            </div>
            <p className="lead">Track Elected Officials</p>
            <p>When council members or aldermen vote, District Watch captures their track record so that you can make the vote that is right for you come election time.</p>
          </div>
        </HorizontalSplit>
      </Section>

      <Section heading="Sign up for News" className="gray">
        <p>District Watch isn't available yet, but we're working quickly to release.  Want to keep updated on our progress?  Sign up for news, announcements, and to join the beta!</p>
        <Formik
          initialValues={{ email: '', name: '', beta: false }}
          validate={values => {
            let errors = {};
            if (!values.email) {
              errors.email = 'Required';
            } else if (
              !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
            ) {
              errors.email = 'Invalid email address';
            }
            return errors;
          }}
          onSubmit={this.postSubscriber.bind(this)}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            /* and other goodies */
          }) => (
            <div className="neal-signup-inline">
              <form onSubmit={handleSubmit} className="form-inline row">
                <div className="col-xs-12 col-lg-3 form-group">
                  <label className="sr-only" for="name">Name</label>
                    <input
                      type="text"
                      name="name"
                      required=""
                      placeholder="Name"
                      className="form-control"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.name}
                    />
                    {errors.name && touched.name && errors.name}
                </div>
                <div className="col-xs-12 col-lg-3 form-group">
                  <label className="sr-only" for="email">Email</label>
                    <input
                      type="email"
                      name="email"
                      required=""
                      placeholder="Email"
                      className="form-control"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.email}
                    />
                    {errors.email && touched.email && errors.email}
                </div>
                <div className="col-xs-12 col-lg-3 form-group form-check">
                  <input
                    className="form-check-input"
                    name="beta"
                    type="checkbox"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    values={values.beta}/>
                  <label className="form-check-label" for="beta">Signup for Beta?</label>
                </div>
                <div className="col-xs-12 col-lg-2 form-group signup-panel">
                  {this.isSpinning()}
                  <button className="btn btn-primary btn-ghost" type="submit" disabled={isSubmitting}>
                    Sign Up
                  </button>
                </div>
              </form>
            </div>
          )}
        </Formik>
      </Section>

      {/* <Section>
        <h2 className="subhero-header">The Team</h2>
        <Team>
          <TeamMember name="Brian Kennedy" title="Founder" imageUrl="images/brian.jpeg">
          Happy programmer, tryhard entrepreneur, continual innovator
          </TeamMember>
          <TeamMember name="Stacie Weffelmeyer" title="Founder" imageUrl="images/stacie.jpeg">
          Interface magnifique, user experience czar
          </TeamMember>
        </Team>
      </Section> */}

      <Footer brandName={brandName}
        facebookUrl={'https://fb.me/distrwatch'}
        twitterUrl={'https://twitter.com/WatchDistrict'}
        githubUrl={'https://gitlab.com/district-watch'}
        email={'thedistrictwatch@gmail.com'}
        address={businessAddress}>
      </Footer>
      <ReactModal 
           ariaHideApp={false}
           isOpen={this.state.showModal}
           onRequestClose={this.handleCloseModal.bind(this)}
           shouldCloseOnOverlayClick={true}
           contentLabel="Thanks for Signing Up!"
        >
          <i className="fa fa-check-circle-o background-check"></i>
          <i className="fa fa-times close-icon" onClick={this.handleCloseModal.bind(this)}></i>
          <h3 className="subscriber-header">All set!</h3>
          <div className="modal-body">
            <p>Thanks for signing up{ (this.state.subscriberBeta ? ' and joining the beta' : '') }, {this.state.subscriberName}! We sent you a confirmation email.</p>
            <p>You can <a href="https://join.slack.com/t/districtwatch/shared_invite/enQtNDM2NTI3NjQzOTQxLWJhY2E3YTg1ZDZjNmI0OGJjZmVjMjZiOTEzOWIzY2Y2ODcyNmMxOWUxMjg2NDZjYWQwZGQ4YTg4YWNiZmJmOGI" target="_blank" rel="noopener noreferrer">join the community on Slack</a> to ask questions, make suggestions, or just chat with other like-minded folk.</p>
            <button onClick={this.handleCloseModal.bind(this)} className="btn btn-primary btn-ghost btn-modal" type="submit">
              Done
            </button>
          </div>
      </ReactModal>
    </Page>
    );
  }
}
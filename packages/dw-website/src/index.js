import React from "react";
import SamplePage from "./app";
import ReactDOM from "react-dom";
import { Router, IndexRoute, Route, browserHistory } from "react-router";
import { App } from "neal-react";

export default class SampleApp extends React.Component {
  render() {
    return (
      <App
        googleAnalyticsKey="UA-126261423-1"
        history={ browserHistory }>
        { this.props.children }
      </App>
    );
  }
}

ReactDOM.render((
  <Router history={ browserHistory }>
    <Route path="/" component={ SampleApp } history={ browserHistory }>
      <IndexRoute name="home" component={ SamplePage }/>
      <Route path="*" component={ SamplePage }/>
    </Route>
  </Router>
), document.getElementById("main"));

module.hot.accept();
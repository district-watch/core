import firebase from "firebase";

var config = {
  apiKey: "AIzaSyBevkMtVy_ErM8K2L1W6DXW5hzyFEO-_pI",
  authDomain: "district-watch-website.firebaseapp.com",
  databaseURL: "https://district-watch-website.firebaseio.com",
  projectId: "district-watch-website",
  storageBucket: "district-watch-website.appspot.com",
  messagingSenderId: "494368503369"
};
var fire = firebase.initializeApp(config);
export default fire;

const baseConfig = require("dw-tools/eslint.config.js");

module.exports = {
  ...baseConfig,
  globals: {
    ...baseConfig.globals,
    window: true,
    document: true
  },
  extends: [
    "eslint:recommended",
    "plugin:react/recommended"
  ],
  settings: {
    react: {
      createClass: "createReactClass",
      pragma: "React",
      version: "16.4.1"
    }
  }
};

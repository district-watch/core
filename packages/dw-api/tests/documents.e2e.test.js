const { Utils } = require('dw-utils');
const utils = new Utils();
const request = require('supertest');
const app = require('../src/app');

describe('documents', () => {
  test('GET should be 200, return data, of type array', async () => {
    try {
      //create test item
      const newTestItem = await request(app).post('/api/v1/documents').send({
        fileName: 'testFile.pdf',
        publicUrl: 'http://www.someUrl.com/testFile.pdf'
      }).set('Accept', 'application/json');
      const data = newTestItem.body;

      //assert
      const response = await request(app).get('/api/v1/documents').set('Accept', 'application/json');
      expect(response.statusCode).toEqual(200);
      expect(utils.getType(response.body)).toEqual('array');
      expect(response.body.length).toBeGreaterThanOrEqual(1);

      //cleanup
      await request(app).del(`/api/v1/documents/${data.id}`).send({}).set('Accept', 'application/json');
    } catch(error) {
      throw new Error('should not have got an error trying to get all documents in e2e test, check it!');
    }
  });

  test('GET by id should be 200, return data type of object', async () => {
    try {
      //create test item
      const newTestItem = await request(app).post('/api/v1/documents').send({
        fileName: 'testFile.pdf',
        publicUrl: 'http://www.someUrl.com/testFile.pdf'
      }).set('Accept', 'application/json');
      const data = newTestItem.body;

      //assert
      const response = await request(app).get(`/api/v1/documents/${data.id}`).set('Accept', 'application/json');
      expect(response.statusCode).toEqual(200);
      expect(utils.getType(response.body)).toEqual('object');
      expect(response.body.fileName).toEqual('testFile.pdf');
      expect(response.body.publicUrl).toEqual('http://www.someUrl.com/testFile.pdf');

      //cleanup
      await request(app).del(`/api/v1/documents/${data.id}`).send({}).set('Accept', 'application/json');
    } catch(error) {
      throw new Error('should not have got an error trying to get document by id, check it!');
    }
  });

  test('GET should be 200 and accept a query via the body to perform db queries', async () => {
    try {
      //create test items
      await request(app).post('/api/v1/documents').send({
        fileName: 'testFile.pdf',
        publicUrl: 'http://www.someUrl.com/testFile.pdf'
      }).set('Accept', 'application/json');
      await request(app).post('/api/v1/documents').send({
        fileName: 'testFile.pdf',
        publicUrl: 'http://www.someUrl.com/testFile.pdf'
      }).set('Accept', 'application/json');
      await request(app).post('/api/v1/documents').send({
        fileName: 'testFile.pdf',
        publicUrl: 'http://www.someUrl.com/testFile.pdf'
      }).set('Accept', 'application/json');

      //assert
      const response = await request(app).get(`/api/v1/documents`).send({ query: [{propertyName: 'fileName', operand: '==', propertyValue: 'testFile.pdf'}] }).set('Accept', 'application/json');
      const data = response.body;
      expect(response.statusCode).toEqual(200);
      expect(utils.getType(data)).toEqual('array');
      expect(data.length).toEqual(3);

      //cleanup
      await request(app).del(`/api/v1/documents/${data[0].id}`).send({}).set('Accept', 'application/json');
      await request(app).del(`/api/v1/documents/${data[1].id}`).send({}).set('Accept', 'application/json');
      await request(app).del(`/api/v1/documents/${data[2].id}`).send({}).set('Accept', 'application/json');
    } catch(error) {
      throw new Error('should not have got an error trying to query for documents in e2e test, check it!');
    }
  });

  test('POST with required schema fields should return 201', async () => {
    try {
      //create test item and assert
      const response = await request(app).post('/api/v1/documents').send({
        fileName: 'testFile.pdf',
        publicUrl: 'http://www.someUrl.com/testFile.pdf'
      }).set('Accept', 'application/json');
      const data = response.body;
      expect(data.id).toBeDefined();
      expect(data.fileName).toEqual('testFile.pdf');
      expect(data.publicUrl).toEqual('http://www.someUrl.com/testFile.pdf');
      expect(response.statusCode).toEqual(201);

      //cleanup
      await request(app).del(`/api/v1/documents/${data.id}`).send({}).set('Accept', 'application/json');
    } catch(error) {
      throw new Error('should not have got an error trying to post to documents in e2e test, check it!');
    }
  });

  test('PUT with required schema fields should return 200', async () => {
    try {
      //create test item and assert
      const testItem = await request(app).post('/api/v1/documents').send({
        fileName: 'testFile.pdf',
        publicUrl: 'http://www.someUrl.com/testFile.pdf'
      }).set('Accept', 'application/json');
      const data = testItem.body;
      expect(data.fileName).toEqual('testFile.pdf');

      //assert
      const updatedItem = await request(app).put(`/api/v1/documents/${data.id}`).send({
        fileName: 'testFile2.pdf',
      }).set('Accept', 'application/json');
      const updatedData = updatedItem.body;
      expect(updatedData.id).toBeDefined();
      expect(updatedData.fileName).toEqual('testFile2.pdf');
      expect(updatedItem.statusCode).toEqual(200);

      //cleanup
      await request(app).del(`/api/v1/documents/${data.id}`).send({}).set('Accept', 'application/json');
    } catch(error) {
      throw new Error('should not have got an error trying to put to a single document by id in e2e test, check it!');
    }
  });

  test('DELETE with required schema fields should return 200', async () => {
    try {
      //create test item
      const createNew = await request(app).post('/api/v1/documents').send({
        fileName: 'testFile.pdf',
        publicUrl: 'http://www.someUrl.com/testFile.pdf'
      }).set('Accept', 'application/json');
      const data = createNew.body;

      //assert and cleanup
      const response = await request(app).del(`/api/v1/documents/${data.id}`).send({}).set('Accept', 'application/json');
      expect(response.body._writeTime).toBeDefined();
      expect(response.statusCode).toEqual(200);
    } catch(error) {
      throw new Error('should not have got an error trying to delete an document by id in e2e test, check it!');
    }
  });
});

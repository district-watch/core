import swagger from '../src/swagger.js';

describe('export', () => {
  test('should be defined and a function', () => {
    expect(swagger).toBeDefined();
    expect(typeof swagger).toEqual('function');
  });

  test('should require an express app object, by checking if name property is \'app\'', () => {
    const nonAppMock = {
      get: (route, callback) => {
        callback();
      }
    };
    const invoke = () => {
      swagger(nonAppMock);
    }
    expect(invoke).toThrow(Error('dw-api: swaggerSetup() method requires app.name to be a string.'));
  });

  test('app should call get, making two routes for swagger and docs', () => {
    const appMock = {
      name: 'app',
      get: () => {}
    };
    const spy = jest.spyOn(appMock, 'get');
    const invoke = () => {
      swagger(appMock, 'development');
    }
    expect(invoke).not.toThrowError();
    expect(spy).toHaveBeenCalled();
    expect(spy).toHaveBeenCalledTimes(2);
  });
});
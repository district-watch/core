import connectDatabase from '../src/db.js';

describe('export', () => {
  test('should be defined and a function', () => {
    expect(connectDatabase).toBeDefined();
    expect(typeof connectDatabase).toEqual('function');
  });

});
const {
  Utils
} = require('dw-utils');
const utils = new Utils();
const request = require('supertest');
const app = require('../src/app');

describe('officials', () => {
  test('GET should be 200, return data, of type array', async () => {
    try {
      //create test item
      const newTestItem = await request(app).post('/api/v1/officials').send({
        firstName: 'Brian',
        lastName: 'Kennedy',
        email: 'test@email.com',
        status: 'encumbent',
        phoneNumber: '314-799-3577'
      }).set('Accept', 'application/json');
      const data = newTestItem.body;

      //assert
      const response = await request(app).get('/api/v1/officials').set('Accept', 'application/json');
      expect(response.statusCode).toEqual(200);
      expect(utils.getType(response.body)).toEqual('array');
      expect(response.body.length).toBeGreaterThanOrEqual(1);

      //cleanup
      await request(app).del(`/api/v1/officials/${data.id}`).send({}).set('Accept', 'application/json');
    } catch (error) {
      throw new Error('should not have got an error trying to get all officials in e2e test, check it!');
    }
  });

  test('GET by id should be 200, return data type of object', async () => {
    try {
      //create test item
      const newTestItem = await request(app).post('/api/v1/officials').send({
        firstName: 'Hawkridge',
        lastName: 'Zeppelin',
        email: 'bestbuds@email.com',
        status: 'encumbent',
        phoneNumber: '314-799-3577'
      }).set('Accept', 'application/json');
      const data = newTestItem.body;

      //assert
      const response = await request(app).get(`/api/v1/officials/${data.id}`).set('Accept', 'application/json');
      expect(response.statusCode).toEqual(200);
      expect(utils.getType(response.body)).toEqual('object');
      expect(response.body.firstName).toEqual('Hawkridge');
      expect(response.body.lastName).toEqual('Zeppelin');
      expect(response.body.email).toEqual('bestbuds@email.com');

      //cleanup
      await request(app).del(`/api/v1/officials/${data.id}`).send({}).set('Accept', 'application/json');
    } catch (error) {
      throw new Error('should not have got an error trying to get official by id, check it!');
    }
  });

  test('GET should be 200 and accept a query via the body to perform db queries', async () => {
    try {
      //create test items
      await request(app).post('/api/v1/officials').send({
        firstName: 'Quaffle',
        lastName: 'Kennedy',
        email: 'test@email.com',
        status: 'encumbent',
        phoneNumber: '314-799-3577'
      }).set('Accept', 'application/json');
      await request(app).post('/api/v1/officials').send({
        firstName: 'Quaffle',
        lastName: 'Kennedy',
        email: 'bill@email.com',
        status: 'encumbent',
        phoneNumber: '314-798-3577'
      }).set('Accept', 'application/json');
      await request(app).post('/api/v1/officials').send({
        firstName: 'Quaffle',
        lastName: 'Kennedy',
        email: 'test@email.com',
        status: 'encumbent',
        phoneNumber: '314-799-3577'
      }).set('Accept', 'application/json');

      //assert
      const response = await request(app).get(`/api/v1/officials`).send({
        query: [{
          propertyName: 'firstName',
          operand: '==',
          propertyValue: 'Quaffle'
        }]
      }).set('Accept', 'application/json');
      const data = response.body;
      expect(response.statusCode).toEqual(200);
      expect(utils.getType(data)).toEqual('array');
      expect(data.length).toEqual(3);

      //cleanup
      await request(app).del(`/api/v1/officials/${data[0].id}`).send({}).set('Accept', 'application/json');
      await request(app).del(`/api/v1/officials/${data[1].id}`).send({}).set('Accept', 'application/json');
      await request(app).del(`/api/v1/officials/${data[2].id}`).send({}).set('Accept', 'application/json');
    } catch (error) {
      throw new Error('should not have got an error trying to query for officials in e2e test, check it!');
    }
  });

  test('POST with required schema fields should return 201', async () => {
    try {
      //create test item and assert
      const response = await request(app).post('/api/v1/officials').send({
        firstName: 'Brian',
        lastName: 'Kennedy',
        email: 'test@email.com',
        status: 'encumbent',
        phoneNumber: '314-799-3577'
      }).set('Accept', 'application/json');
      const data = response.body;
      expect(data.id).toBeDefined();
      expect(data.lastName).toEqual('Kennedy');
      expect(data.status).toEqual('encumbent');
      expect(response.statusCode).toEqual(201);

      //cleanup
      await request(app).del(`/api/v1/officials/${data.id}`).send({}).set('Accept', 'application/json');
    } catch (error) {
      throw new Error('should not have got an error trying to post to officials in e2e test, check it!');
    }
  });

  test('PUT with required schema fields should return 200', async () => {
    try {
      //create test item and assert
      const testItem = await request(app).post('/api/v1/officials').send({
        firstName: 'Brian',
        lastName: 'Kennedy',
        email: 'test@email.com',
        status: 'encumbent',
        phoneNumber: '314-799-3577'
      }).set('Accept', 'application/json');
      const data = testItem.body;
      expect(data.firstName).toEqual('Brian');

      //assert
      const updatedItem = await request(app).put(`/api/v1/officials/${data.id}`).send({
        firstName: 'Beezelbub',
      }).set('Accept', 'application/json');
      const updatedData = updatedItem.body;
      expect(updatedData.id).toBeDefined();
      expect(updatedData.firstName).toEqual('Beezelbub');
      expect(updatedItem.statusCode).toEqual(200);

      //cleanup
      await request(app).del(`/api/v1/officials/${data.id}`).send({}).set('Accept', 'application/json');
    } catch (error) {
      throw new Error('should not have got an error trying to put to a single official by id in e2e test, check it!');
    }
  });

  test('DELETE with required schema fields should return 200', async () => {
    try {
      //create test item
      const createNew = await request(app).post('/api/v1/officials').send({
        firstName: 'Brian',
        lastName: 'Kennedy',
        email: 'test@email.com',
        status: 'encumbent',
        phoneNumber: '314-799-3577'
      }).set('Accept', 'application/json');
      const data = createNew.body;

      //assert and cleanup
      const response = await request(app).del(`/api/v1/officials/${data.id}`).send({}).set('Accept', 'application/json');
      expect(response.body._writeTime).toBeDefined();
      expect(response.statusCode).toEqual(200);
    } catch (error) {
      throw new Error('should not have got an error trying to delete an official by id in e2e test, check it!');
    }
  });
});
'use strict';

var _env = require('./env');

var _env2 = _interopRequireDefault(_env);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _compression = require('compression');

var _compression2 = _interopRequireDefault(_compression);

var _helmet = require('helmet');

var _helmet2 = _interopRequireDefault(_helmet);

var _dwLogger = require('dw-logger');

var _officials = require('./routes/officials');

var _officials2 = _interopRequireDefault(_officials);

var _documents = require('./routes/documents');

var _documents2 = _interopRequireDefault(_documents);

var _swagger = require('./swagger');

var _swagger2 = _interopRequireDefault(_swagger);

var _db = require('./db');

var _db2 = _interopRequireDefault(_db);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// eslint-disable-line no-unused-vars
const logger = new _dwLogger.Logger();
logger.log('info', `Environment Variable is ${process.env.NODE_ENV}`);

let app = (0, _express2.default)();
app.disable('x-powered-by');

// initialize morgan http logging
app.use((0, _morgan2.default)('dev', {
  skip: function (req, res) {
    return res.statusCode < 400;
  },
  stream: process.stderr
}));

app.use((0, _morgan2.default)('dev', {
  skip: function (req, res) {
    return res.statusCode >= 400;
  },
  stream: process.stdout
}));

// enable cors
app.use((0, _cors2.default)());

// enabling gzip compression of responses: https://github.com/expressjs/compression
app.use((0, _compression2.default)());

// http/https basic security: https://github.com/helmetjs/helmet
app.use((0, _helmet2.default)());

app.use(_bodyParser2.default.json());
app.use(_bodyParser2.default.urlencoded({
  extended: false
}));
app.use(_express2.default.static(_path2.default.join(__dirname, '../public')));

// setup swagger configuration and docs route
(0, _swagger2.default)(app, process.env.NODE_ENV);

// setup database connection
(0, _db2.default)().then(db => {

  // setup api routes
  (0, _officials2.default)({ app, db });
  (0, _documents2.default)({ app, db });

  // Catch 404 and forward to error handler
  app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // Error handler
  app.use((err, req, res, next) => {
    // eslint-disable-line no-unused-vars
    //handle Joi validation errors
    if (err.isJoi) {
      res.status(400).json({
        name: 'ValidationError',
        message: err.details.message
      });
    } else {
      res.status(err.status || 500).json({
        message: err.message
      });
    }
  });
});

module.exports = app;
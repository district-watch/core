'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _dwLogger = require('dw-logger');

class OfficialsController {

  constructor(db) {
    this.db = db;
    this.logger = new _dwLogger.Logger();
  }

  async listAll(req, res) {
    try {
      let officials = [];
      if (req.body.query) {
        officials = await this.db.query('officials', req.body.query);
      } else {
        officials = await this.db.query('officials');
      }
      res.status(200).send(officials);
    } catch (error) {
      this.logger.error(error);
      res.status(400).send(error);
    }
  }

  async listOne(req, res) {
    try {
      const official = await this.db.getOne('officials', req.params.id);
      res.status(200).send(official);
    } catch (error) {
      this.logger.error(error);
      res.status(400).send(error);
    }
  }

  async createOne(req, res) {
    try {
      const newOfficial = await this.db.createOne('officials', req.body);
      res.status(201).send(newOfficial);
    } catch (error) {
      this.logger.error(error);
      res.status(400).send(error);
    }
  }

  async updateOne(req, res) {
    try {
      const existingOfficial = await this.db.updateOne('officials', req.body, req.params.id);
      res.status(200).send(existingOfficial);
    } catch (error) {
      this.logger.error(error);
      res.status(400).send(error);
    }
  }

  async deleteOne(req, res) {
    try {
      const removedOfficial = await this.db.deleteOne('officials', req.params.id);
      res.status(200).send(removedOfficial);
    } catch (error) {
      this.logger.error(error);
      res.status(400).send(error);
    }
  }
}

exports.default = OfficialsController;
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _swaggerJsdoc = require('swagger-jsdoc');

var _swaggerJsdoc2 = _interopRequireDefault(_swaggerJsdoc);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _dwUtils = require('dw-utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const utils = new _dwUtils.Utils();

const swaggerSetup = (app, environment) => {
  utils.validateString('dw-api', app.name, 'swaggerSetup', 'app.name');
  utils.validateString('dw-api', environment, 'swaggerSetup', 'environment');
  const host = environment => {
    if (environment === 'development') {
      return 'localhost:8080';
    } else if (environment === 'staging') {
      return 'dw-api-staging.herokuapp.com';
    } else if (environment === 'production') {
      return 'dw-api-prod.herokuapp.com';
    }
  };

  // -- setup up swagger-jsdoc --
  const swaggerDefinition = {
    info: {
      title: 'District Watch API',
      version: '1.0.0',
      description: 'Application Programming Interface for District Watch.'
    },
    host: host(environment),
    basePath: '/api/v1/'
  };
  const options = {
    swaggerDefinition,
    apis: [_path2.default.resolve(__dirname, 'routes/officials.js'), _path2.default.resolve(__dirname, 'routes/documents.js')]
  };
  const swaggerSpec = (0, _swaggerJsdoc2.default)(options);

  // -- routes for docs and generated swagger spec --
  app.get('/swagger.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
  });

  app.get('/api/v1/docs', (req, res) => {
    res.sendFile(_path2.default.join(__dirname, '../public/docs.html'));
  });
};

exports.default = swaggerSetup;
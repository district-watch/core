'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _celebrate = require('celebrate');

var _officialsController = require('../controllers/officialsController');

var _officialsController2 = _interopRequireDefault(_officialsController);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const QueryFilterSchema = _celebrate.Joi.object().keys({
  propertyName: _celebrate.Joi.string().required(),
  operand: _celebrate.Joi.string().required(),
  propertyValue: _celebrate.Joi.string().required()
});

exports.default = ({ app, db }) => {
  let router = (0, _express.Router)();

  // setup route handler controller
  const officialsController = new _officialsController2.default(db);

  // Base api route
  app.use('/api/v1', router);

  // swagger model definitions
  /**
  * @swagger
  * definitions:
  *   Query:
  *     properties:
  *       propertyName:
  *         type: "string"
  *       operand:
  *         type: "string"
  *       propertyValue:
  *         type: "string"
  *   Official:
  *     properties:
  *       office:
  *         type: enum
  *         enum:
  *           - mayor
  *           - alderman
  *       firstName:
  *         type: "string"
  *       lastName:
  *         type: "string"
  *       middleName:
  *         type: "string"
  *       suffix:
  *         type: "string"
  *       email:
  *         type: "string"
  *       imageUrl:
  *         type: "string"
  *       status:
  *         type: enum
  *         enum:
  *           - encumbent
  *           - candidate
  *           - retired
  *       primaryDepartment:
  *         type: enum
  *         enum:
  *           - city council
  *           - mayor
  *       municipality:
  *         type: "string"
  *       ward:
  *         type: "string"
  *       phoneNumber:
  *         type: "string"
  *       dateElected:
  *         type: "number"
  */

  /**
  * @swagger
  * /officials:
  *   get:
  *     summary: Get Officials
  *     description: Query a list of officials
  *     tags:
  *       - officials
  *     produce:
  *       - application/json
  *     parameters:
  *       - query: array of query filter objects for querying by
  *         description: array of query filter objects for querying by
  *         in: body
  *         name: query
  *         required: false
  *         schema:
  *           type: array
  *           items:
  *             $ref: '#/definitions/Query'
  *     responses:
  *       200:
  *         description: an array of Official objects
  *         schema:
  *           type: array
  *           items:
  *             $ref: "#/definitions/Official"
  */
  router.get('/officials', (0, _celebrate.celebrate)({
    body: _celebrate.Joi.object().keys({
      query: _celebrate.Joi.array().items(QueryFilterSchema).optional()
    })
  }), (req, res, next) => {
    officialsController.listAll(req, res, next);
  });

  /**
  * @swagger
  * /officials/{id}:
  *   get:
  *     summary: Get Official
  *     description: Get a single official by id
  *     tags:
  *       - officials
  *     produce:
  *       - application/json
  *     parameters:
  *       - id: id
  *         description: official's id
  *         in: query
  *         name: id
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Official'
  *     responses:
  *       200:
  *         description: an Official object
  *         schema:
  *           $ref: "#/definitions/Official"
  */
  router.get('/officials/:id', (0, _celebrate.celebrate)({
    params: _celebrate.Joi.object().keys({
      id: _celebrate.Joi.string().required()
    })
  }), (req, res, next) => {
    officialsController.listOne(req, res, next);
  });

  /**
  * @swagger
  * /officials/{id}:
  *   delete:
  *     summary: Delete Official
  *     description: Delete a single official by id
  *     tags:
  *       - officials
  *     produce:
  *       - application/json
  *     parameters:
  *       - id: id
  *         description: official's id
  *         in: query
  *         name: id
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Official'
  *     responses:
  *       200:
  *         description: an object with timestamp of deletion
  *         schema:
  *           $ref: "#/definitions/Official"
  */
  router.delete('/officials/:id', (0, _celebrate.celebrate)({
    params: _celebrate.Joi.object().keys({
      id: _celebrate.Joi.string().required()
    })
  }), (req, res, next) => {
    officialsController.deleteOne(req, res, next);
  });

  /**
  * @swagger
  * /officials:
  *   post:
  *     summary: Create Official
  *     description: Create a single official
  *     tags:
  *       - officials
  *     produce:
  *       - application/json
  *     parameters:
  *       - office: office of official
  *         description: office of official, only mayor or alderman allowed
  *         in: body
  *         name: office
  *         enum:
  *           - mayor
  *           - alderman
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - firstName: first name of official
  *         description: first name of official
  *         in: body
  *         name: firstName
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - lastName: last name of official
  *         description: last name of official
  *         in: body
  *         name: lastName
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - middleName: middle name of official
  *         description: middle name of official
  *         in: body
  *         name: middleName
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - suffix: suffix name of official
  *         description: suffix name of official
  *         in: body
  *         name: suffix
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - email: email address of official
  *         description: email address of official
  *         in: body
  *         name: email
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - imageUrl: profile image url address name of official
  *         description: profile image url address name of official
  *         in: body
  *         name: imageUrl
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - status: status of official
  *         description: status of official, only encumbent, candidate, retired allowed
  *         in: body
  *         name: status
  *         type: string
  *         enum:
  *           - encumbent
  *           - candidate
  *           - retired
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - primaryDepartment: primary role in government
  *         description: primary role in government, only city council or mayor allowed
  *         in: body
  *         name: primaryDepartment
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - municipality: municipality of official
  *         description: municipality of official
  *         in: body
  *         name: municipality
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - ward: ward of official
  *         description: ward of official
  *         in: body
  *         name: ward
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - phoneNumber: phone number of official
  *         description: phone number of official
  *         in: body
  *         name: phoneNumber
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - dateElected: date official was elected to office
  *         description: date official was elected to office
  *         in: body
  *         name: dateElected
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *     responses:
  *       201:
  *         description: an Official object
  *         schema:
  *           $ref: "#/definitions/Official"
  */
  router.post('/officials', (0, _celebrate.celebrate)({
    body: _celebrate.Joi.object().keys({
      office: _celebrate.Joi.string().valid(['mayor', 'alderman']).optional(),
      firstName: _celebrate.Joi.string().required(),
      lastName: _celebrate.Joi.string().required(),
      middleName: _celebrate.Joi.string().optional(),
      suffix: _celebrate.Joi.string().optional(),
      email: _celebrate.Joi.string().required(),
      imageUrl: _celebrate.Joi.string().optional(),
      status: _celebrate.Joi.string().valid(['encumbent', 'candidate', 'retired']).required(),
      primaryDepartment: _celebrate.Joi.string().valid(['city council', 'mayor']).optional(),
      municipality: _celebrate.Joi.string().optional(),
      ward: _celebrate.Joi.string().optional(),
      phoneNumber: _celebrate.Joi.string().required(),
      dateElected: _celebrate.Joi.date().timestamp().optional()
    })
  }), (req, res, next) => {
    officialsController.createOne(req, res, next);
  });

  /**
  * @swagger
  * /officials:
  *   put:
  *     summary: Update Official
  *     description: Updates a single official
  *     tags:
  *       - officials
  *     produce:
  *       - application/json
  *     parameters:
  *       - id: id
  *         description: official's id
  *         in: query
  *         name: id
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - office: office of official
  *         description: office of official, only mayor or alderman allowed
  *         in: body
  *         name: office
  *         enum:
  *           - mayor
  *           - alderman
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - firstName: first name of official
  *         description: first name of official
  *         in: body
  *         name: firstName
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - lastName: last name of official
  *         description: last name of official
  *         in: body
  *         name: lastName
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - middleName: middle name of official
  *         description: middle name of official
  *         in: body
  *         name: middleName
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - suffix: suffix name of official
  *         description: suffix name of official
  *         in: body
  *         name: suffix
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - email: email address of official
  *         description: email address of official
  *         in: body
  *         name: email
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - imageUrl: profile image url address name of official
  *         description: profile image url address name of official
  *         in: body
  *         name: imageUrl
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - status: status of official
  *         description: status of official, only encumbent, candidate, retired allowed
  *         in: body
  *         name: status
  *         type: string
  *         enum:
  *           - encumbent
  *           - candidate
  *           - retired
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - primaryDepartment: primary role in government
  *         description: primary role in government, only city council or mayor allowed
  *         in: body
  *         name: primaryDepartment
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - municipality: municipality of official
  *         description: municipality of official
  *         in: body
  *         name: municipality
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - ward: ward of official
  *         description: ward of official
  *         in: body
  *         name: ward
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - phoneNumber: phone number of official
  *         description: phone number of official
  *         in: body
  *         name: phoneNumber
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - dateElected: date official was elected to office
  *         description: date official was elected to office
  *         in: body
  *         name: dateElected
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *     responses:
  *       201:
  *         description: an Official object
  *         schema:
  *           $ref: "#/definitions/Official"
  */
  router.put('/officials/:id', (0, _celebrate.celebrate)({
    params: _celebrate.Joi.object().keys({
      id: _celebrate.Joi.string().required()
    }),
    body: _celebrate.Joi.object().keys({
      office: _celebrate.Joi.string().valid(['mayor', 'alderman']).optional(),
      firstName: _celebrate.Joi.string().optional(),
      lastName: _celebrate.Joi.string().optional(),
      middleName: _celebrate.Joi.string().optional(),
      suffix: _celebrate.Joi.string().optional(),
      email: _celebrate.Joi.string().optional(),
      imageUrl: _celebrate.Joi.string().optional(),
      status: _celebrate.Joi.string().valid(['encumbent', 'candidate', 'retired']).optional(),
      primaryDepartment: _celebrate.Joi.string().valid(['city council', 'mayor']).optional(),
      municipality: _celebrate.Joi.string().optional(),
      ward: _celebrate.Joi.string().optional(),
      phoneNumber: _celebrate.Joi.string().optional(),
      dateElected: _celebrate.Joi.date().timestamp().optional()
    })
  }), (req, res, next) => {
    officialsController.updateOne(req, res, next);
  });
};
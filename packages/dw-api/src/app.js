import _ from './env'; // eslint-disable-line no-unused-vars
import express from 'express';
import path from 'path';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import cors from 'cors';
import compression from 'compression';
import helmet from 'helmet';
import {
  Logger
} from 'dw-logger';
import setupOfficialsRoute from './routes/officials';
import setupDocumentsRoute from './routes/documents';
import setupSearchRoute from './routes/search';
import swaggerSetup from './swagger';
import connectDatabase from './db';

const logger = new Logger();
logger.log('info', `Environment Variable is ${process.env.NODE_ENV}`);

let app = express();
app.disable('x-powered-by');

// initialize morgan http logging
app.use(morgan('dev', {
  skip: function (req, res) {
    return res.statusCode < 400
  },
  stream: process.stderr
}));

app.use(morgan('dev', {
  skip: function (req, res) {
    return res.statusCode >= 400
  },
  stream: process.stdout
}));

// enable cors
app.use(cors());

// enabling gzip compression of responses: https://github.com/expressjs/compression
app.use(compression());

// http/https basic security: https://github.com/helmetjs/helmet
app.use(helmet());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(express.static(path.join(__dirname, '../public')));

// setup swagger configuration and docs route
swaggerSetup(app, process.env.NODE_ENV);

// setup database connection
connectDatabase().then(db => {

  // setup api routes
  setupOfficialsRoute({ app, db });
  setupDocumentsRoute({ app, db });
  setupSearchRoute({ app });

  // Catch 404 and forward to error handler
  app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // Error handler
  app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
    //handle Joi validation errors
    if (err.isJoi) {
      res.status(400).json({
        name: 'ValidationError',
        message: err.details.message
      });
    } else {
      res.status(err.status || 500).json({
        message: err.message
      });
    }
  });

});

module.exports = app;
import { Logger } from 'dw-logger';

class DocumentsController {

  constructor(db) {
    this.db = db;
    this.logger = new Logger();
  }

  async listAll(req, res) {
    try {
      let documents = [];
      if (req.body.query) {
        documents = await this.db.query('documents', req.body.query);
      } else {
        documents = await this.db.query('documents');
      }
      res.status(200).send(documents);
    } catch(error) {
      this.logger.error(error);
      res.status(400).send(error);
    }
  }

  async listOne(req, res) {
    try {
      const document = await this.db.getOne('documents', req.params.id);
      res.status(200).send(document);
    } catch(error) {
      this.logger.error(error);
      res.status(400).send(error);
    }
  }

  async createOne(req, res) {
    try {
      const newDocument = await this.db.createOne('documents', req.body);
      res.status(201).send(newDocument);
    } catch(error) {
      this.logger.error(error);
      res.status(400).send(error);
    }
  }

  async updateOne(req, res) {
    try {
      const existingDocument = await this.db.updateOne('documents', req.body, req.params.id);
      res.status(200).send(existingDocument);
    } catch(error) {
      this.logger.error(error);
      res.status(400).send(error);
    }
  }

  async deleteOne(req, res) {
    try {
      const removedDocument = await this.db.deleteOne('documents', req.params.id);
      res.status(200).send(removedDocument);
    } catch(error) {
      this.logger.error(error);
      res.status(400).send(error);
    }
  }
}

export default DocumentsController

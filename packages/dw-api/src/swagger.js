import swaggerJSDoc from 'swagger-jsdoc';
import path from 'path';
import {
  Utils
} from 'dw-utils';
const utils = new Utils();

const swaggerSetup = (app, environment) => {
  utils.validateString('dw-api', app.name, 'swaggerSetup', 'app.name');
  utils.validateString('dw-api', environment, 'swaggerSetup', 'environment');
  const host = (environment) => {
    if (environment === 'development') {
      return 'localhost:8080';
    } else if (environment === 'staging') {
      return 'dw-api-staging.herokuapp.com';
    } else if (environment === 'production') {
      return 'dw-api-prod.herokuapp.com';
    }
  }

  // -- setup up swagger-jsdoc --
  const swaggerDefinition = {
    info: {
      title: 'District Watch API',
      version: '1.0.0',
      description: 'Application Programming Interface for District Watch.',
    },
    host: host(environment),
    basePath: '/api/v1/',
  };
  const options = {
    swaggerDefinition,
    apis: [
      path.resolve(__dirname, 'routes/officials.js'),
      path.resolve(__dirname, 'routes/documents.js')
    ],
  };
  const swaggerSpec = swaggerJSDoc(options);

  // -- routes for docs and generated swagger spec --
  app.get('/swagger.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
  });

  app.get('/api/v1/docs', (req, res) => {
    res.sendFile(path.join(__dirname, '../public/docs.html'));
  });
}

export default swaggerSetup;
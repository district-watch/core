import { Router } from 'express';
import { celebrate, Joi } from 'celebrate';
import OfficialsController from '../controllers/officialsController';

const QueryFilterSchema = Joi.object().keys({
  propertyName: Joi.string().required(),
  operand: Joi.string().required(),
  propertyValue: Joi.string().required()
});

export default ({ app, db }) => {
  let router = Router();
  
  // setup route handler controller
  const officialsController = new OfficialsController(db);

  // Base api route
  app.use('/api/v1', router);

  // swagger model definitions
  /**
  * @swagger
  * definitions:
  *   Query:
  *     properties:
  *       propertyName:
  *         type: "string"
  *       operand:
  *         type: "string"
  *       propertyValue:
  *         type: "string"
  *   Official:
  *     properties:
  *       office:
  *         type: enum
  *         enum:
  *           - mayor
  *           - alderman
  *       firstName:
  *         type: "string"
  *       lastName:
  *         type: "string"
  *       middleName:
  *         type: "string"
  *       suffix:
  *         type: "string"
  *       email:
  *         type: "string"
  *       imageUrl:
  *         type: "string"
  *       status:
  *         type: enum
  *         enum:
  *           - encumbent
  *           - candidate
  *           - retired
  *       primaryDepartment:
  *         type: enum
  *         enum:
  *           - city council
  *           - mayor
  *       municipality:
  *         type: "string"
  *       ward:
  *         type: "string"
  *       phoneNumber:
  *         type: "string"
  *       dateElected:
  *         type: "number"
  */


  /**
  * @swagger
  * /officials:
  *   get:
  *     summary: Get Officials
  *     description: Query a list of officials
  *     tags:
  *       - officials
  *     produce:
  *       - application/json
  *     parameters:
  *       - query: array of query filter objects for querying by
  *         description: array of query filter objects for querying by
  *         in: body
  *         name: query
  *         required: false
  *         schema:
  *           type: array
  *           items:
  *             $ref: '#/definitions/Query'
  *     responses:
  *       200:
  *         description: an array of Official objects
  *         schema:
  *           type: array
  *           items:
  *             $ref: "#/definitions/Official"
  */
  router.get('/officials', celebrate({
    body: Joi.object().keys({
      query: Joi.array().items(QueryFilterSchema).optional()
    })
  }), (req, res, next) => {
    officialsController.listAll(req, res, next);
  });

  /**
  * @swagger
  * /officials/{id}:
  *   get:
  *     summary: Get Official
  *     description: Get a single official by id
  *     tags:
  *       - officials
  *     produce:
  *       - application/json
  *     parameters:
  *       - id: id
  *         description: official's id
  *         in: query
  *         name: id
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Official'
  *     responses:
  *       200:
  *         description: an Official object
  *         schema:
  *           $ref: "#/definitions/Official"
  */ 
  router.get('/officials/:id', celebrate({
    params: Joi.object().keys({
      id: Joi.string().required()
    })
  }), (req, res, next) => {
    officialsController.listOne(req, res, next);
  });

  /**
  * @swagger
  * /officials/{id}:
  *   delete:
  *     summary: Delete Official
  *     description: Delete a single official by id
  *     tags:
  *       - officials
  *     produce:
  *       - application/json
  *     parameters:
  *       - id: id
  *         description: official's id
  *         in: query
  *         name: id
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Official'
  *     responses:
  *       200:
  *         description: an object with timestamp of deletion
  *         schema:
  *           $ref: "#/definitions/Official"
  */
  router.delete('/officials/:id', celebrate({
    params: Joi.object().keys({
      id: Joi.string().required()
    })
  }),(req, res, next) => {
    officialsController.deleteOne(req, res, next);
  });

  /**
  * @swagger
  * /officials:
  *   post:
  *     summary: Create Official
  *     description: Create a single official
  *     tags:
  *       - officials
  *     produce:
  *       - application/json
  *     parameters:
  *       - office: office of official
  *         description: office of official, only mayor or alderman allowed
  *         in: body
  *         name: office
  *         enum:
  *           - mayor
  *           - alderman
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - firstName: first name of official
  *         description: first name of official
  *         in: body
  *         name: firstName
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - lastName: last name of official
  *         description: last name of official
  *         in: body
  *         name: lastName
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - middleName: middle name of official
  *         description: middle name of official
  *         in: body
  *         name: middleName
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - suffix: suffix name of official
  *         description: suffix name of official
  *         in: body
  *         name: suffix
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - email: email address of official
  *         description: email address of official
  *         in: body
  *         name: email
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - imageUrl: profile image url address name of official
  *         description: profile image url address name of official
  *         in: body
  *         name: imageUrl
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - status: status of official
  *         description: status of official, only encumbent, candidate, retired allowed
  *         in: body
  *         name: status
  *         type: string
  *         enum:
  *           - encumbent
  *           - candidate
  *           - retired
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - primaryDepartment: primary role in government
  *         description: primary role in government, only city council or mayor allowed
  *         in: body
  *         name: primaryDepartment
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - municipality: municipality of official
  *         description: municipality of official
  *         in: body
  *         name: municipality
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - ward: ward of official
  *         description: ward of official
  *         in: body
  *         name: ward
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - phoneNumber: phone number of official
  *         description: phone number of official
  *         in: body
  *         name: phoneNumber
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - dateElected: date official was elected to office
  *         description: date official was elected to office
  *         in: body
  *         name: dateElected
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *     responses:
  *       201:
  *         description: an Official object
  *         schema:
  *           $ref: "#/definitions/Official"
  */
  router.post('/officials', celebrate({
    body: Joi.object().keys({
      office: Joi.string().valid(['mayor', 'alderman']).optional(),
      firstName: Joi.string().required(),
      lastName: Joi.string().required(),
      middleName: Joi.string().optional(),
      suffix: Joi.string().optional(),
      email: Joi.string().required(),
      imageUrl: Joi.string().optional(),
      status: Joi.string().valid(['encumbent', 'candidate', 'retired']).required(),
      primaryDepartment: Joi.string().valid(['city council', 'mayor']).optional(),
      municipality: Joi.string().optional(),
      ward: Joi.string().optional(),
      phoneNumber: Joi.string().required(),
      dateElected: Joi.date().timestamp().optional()
    })
  }),(req, res, next) => {
    officialsController.createOne(req, res, next);
  });

  /**
  * @swagger
  * /officials:
  *   put:
  *     summary: Update Official
  *     description: Updates a single official
  *     tags:
  *       - officials
  *     produce:
  *       - application/json
  *     parameters:
  *       - id: id
  *         description: official's id
  *         in: query
  *         name: id
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - office: office of official
  *         description: office of official, only mayor or alderman allowed
  *         in: body
  *         name: office
  *         enum:
  *           - mayor
  *           - alderman
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - firstName: first name of official
  *         description: first name of official
  *         in: body
  *         name: firstName
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - lastName: last name of official
  *         description: last name of official
  *         in: body
  *         name: lastName
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - middleName: middle name of official
  *         description: middle name of official
  *         in: body
  *         name: middleName
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - suffix: suffix name of official
  *         description: suffix name of official
  *         in: body
  *         name: suffix
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - email: email address of official
  *         description: email address of official
  *         in: body
  *         name: email
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - imageUrl: profile image url address name of official
  *         description: profile image url address name of official
  *         in: body
  *         name: imageUrl
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - status: status of official
  *         description: status of official, only encumbent, candidate, retired allowed
  *         in: body
  *         name: status
  *         type: string
  *         enum:
  *           - encumbent
  *           - candidate
  *           - retired
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - primaryDepartment: primary role in government
  *         description: primary role in government, only city council or mayor allowed
  *         in: body
  *         name: primaryDepartment
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - municipality: municipality of official
  *         description: municipality of official
  *         in: body
  *         name: municipality
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - ward: ward of official
  *         description: ward of official
  *         in: body
  *         name: ward
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - phoneNumber: phone number of official
  *         description: phone number of official
  *         in: body
  *         name: phoneNumber
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *       - dateElected: date official was elected to office
  *         description: date official was elected to office
  *         in: body
  *         name: dateElected
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Official'
  *     responses:
  *       201:
  *         description: an Official object
  *         schema:
  *           $ref: "#/definitions/Official"
  */
  router.put('/officials/:id', celebrate({
    params: Joi.object().keys({
      id: Joi.string().required()
    }),
    body: Joi.object().keys({
      office: Joi.string().valid(['mayor', 'alderman']).optional(),
      firstName: Joi.string().optional(),
      lastName: Joi.string().optional(),
      middleName: Joi.string().optional(),
      suffix: Joi.string().optional(),
      email: Joi.string().optional(),
      imageUrl: Joi.string().optional(),
      status: Joi.string().valid(['encumbent', 'candidate', 'retired']).optional(),
      primaryDepartment: Joi.string().valid(['city council', 'mayor']).optional(),
      municipality: Joi.string().optional(),
      ward: Joi.string().optional(),
      phoneNumber: Joi.string().optional(),
      dateElected: Joi.date().timestamp().optional()
    })
  }),(req, res, next) => {
    officialsController.updateOne(req, res, next);
  });
};

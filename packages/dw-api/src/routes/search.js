import proxy from 'http-proxy-middleware';
import btoa from 'btoa';

const options = {
  target: process.env.BONSAI_URL,
  changeOrigin: true,
  onProxyReq: (proxyReq, req) => {
    proxyReq.setHeader(
      'Authorization',
      `Basic ${btoa(`${process.env.BONSAI_USERNAME}:${process.env.BONSAI_SECRET}`)}`
    );
    /* transform the req body back from text */
    const { body } = req;
    if (body) {
      if (typeof body === 'object') {
        proxyReq.write(JSON.stringify(body));
      } else {
        proxyReq.write(body);
      }
    }
  }
}

export default ({ app }) => {
  // Base api route
  app.use('/api/v1/search', proxy(options));
};
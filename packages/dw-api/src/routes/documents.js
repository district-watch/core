import { Router } from 'express';
import { celebrate, Joi } from 'celebrate';
import DocumentsController from '../controllers/documentsController';

const QueryFilterSchema = Joi.object().keys({
  propertyName: Joi.string().required(),
  operand: Joi.string().required(),
  propertyValue: Joi.string().required()
});

export default ({ app, db }) => {
  let router = Router();
  
  // setup route handler controller
  const documentsController = new DocumentsController(db);

  // Base api route
  app.use('/api/v1', router);

  // swagger model definitions
  /**
  * @swagger
  * definitions:
  *   Query:
  *     properties:
  *       propertyName:
  *         type: "string"
  *       operand:
  *         type: "string"
  *       propertyValue:
  *         type: "string"
  *   Document:
  *     properties:
  *       id:
  *         type: "string"
  *       fileName:
  *         type: "string"
  *       phase:
  *         type: enum
  *         enum:
  *           - retrieve
  *           - parse
  *           - assemble
  *           - index
  *           - complete
  *       status:
  *         type: enum
  *         enum:
  *           - downloading
  *           - uploaded
  *           - error retrieving
  *           - parsing
  *           - parsed
  *           - error parsing
  *           - assembling
  *           - assembled
  *           - error assembling
  *           - indexing
  *           - complete
  *           - error indexing
  *       publicUrl:
  *         type: "string"
  *       lastModified:
  *         type: "number"
  *       createdOn:
  *         type: "number"
  */


  /**
  * @swagger
  * /documents:
  *   get:
  *     summary: Get Documents
  *     description: Query a list of documents
  *     tags:
  *       - documents
  *     produce:
  *       - application/json
  *     parameters:
  *       - query: array of query filter objects for querying by
  *         description: array of query filter objects for querying by
  *         in: body
  *         name: query
  *         required: false
  *         schema:
  *           type: array
  *           items:
  *             $ref: '#/definitions/Query'
  *     responses:
  *       200:
  *         description: an array of Documents objects
  *         schema:
  *           type: array
  *           items:
  *             $ref: "#/definitions/Document"
  */
  router.get('/documents', celebrate({
    body: Joi.object().keys({
      query: Joi.array().items(QueryFilterSchema).optional()
    })
  }), (req, res, next) => {
    documentsController.listAll(req, res, next);
  });

  /**
  * @swagger
  * /documents/{id}:
  *   get:
  *     summary: Get Document
  *     description: Get a single document by id
  *     tags:
  *       - documents
  *     produce:
  *       - application/json
  *     parameters:
  *       - id: id
  *         description: document's id
  *         in: query
  *         name: id
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Document'
  *     responses:
  *       200:
  *         description: an Document object
  *         schema:
  *           $ref: "#/definitions/Document"
  */ 
  router.get('/documents/:id', celebrate({
    params: Joi.object().keys({
      id: Joi.string().required()
    })
  }), (req, res, next) => {
    documentsController.listOne(req, res, next);
  });

  /**
  * @swagger
  * /documents/{id}:
  *   delete:
  *     summary: Delete Document
  *     description: Delete a single document by id
  *     tags:
  *       - documents
  *     produce:
  *       - application/json
  *     parameters:
  *       - id: id
  *         description: document's id
  *         in: query
  *         name: id
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Document'
  *     responses:
  *       200:
  *         description: an object with timestamp of deletion
  *         schema:
  *           $ref: "#/definitions/Document"
  */
  router.delete('/documents/:id', celebrate({
    params: Joi.object().keys({
      id: Joi.string().required()
    })
  }),(req, res, next) => {
    documentsController.deleteOne(req, res, next);
  });

  /**
  * @swagger
  * /documents:
  *   post:
  *     summary: Create Document
  *     description: Create a single document
  *     tags:
  *       - documents
  *     produce:
  *       - application/json
  *     parameters:
  *       - fileName: name of document
  *         description: name of document with file extension
  *         in: body
  *         name: fileName
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Document'
  *       - publicUrl: public url of the document
  *         description: public url of the document
  *         in: body
  *         name: publicUrl
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Document'
  *     responses:
  *       201:
  *         description: a Document object
  *         schema:
  *           $ref: "#/definitions/Document"
  */
  router.post('/documents', celebrate({
    body: Joi.object().keys({
      fileName: Joi.string().required(),
      publicUrl: Joi.string().required()
    })
  }),(req, res, next) => {
    documentsController.createOne(req, res, next);
  });

  /**
  * @swagger
  * /documents:
  *   put:
  *     summary: Update Document
  *     description: Updates a single document
  *     tags:
  *       - documents
  *     produce:
  *       - application/json
  *     parameters:
  *       - id: id
  *         description: document's id
  *         in: query
  *         name: id
  *         required: true
  *         schema:
  *           $ref: '#/definitions/Document'
  *       - fileName: name of document
  *         description: name of document with file extension
  *         in: body
  *         name: fileName
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Document'
  *       - publicUrl: public url of the document
  *         description: public url of the document
  *         in: body
  *         name: publicUrl
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Document'
  *       - phase: phase of Document
  *         description: phase of Document, only retrieve, parse, assemble, index, complete allowed
  *         in: body
  *         name: phase
  *         enum:
  *           - retrieve
  *           - parse
  *           - assemble
  *           - index
  *           - complete
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Document'
  *       - status: status of Document
  *         description: status of Document, only downloading, uploaded, parsing, parsed, assembling, assembled, indexing, complete allowed
  *         in: body
  *         name: status
  *         enum:
  *           - downloading
  *           - uploaded
  *           - error retrieving
  *           - parsing
  *           - parsed
  *           - error parsing
  *           - assembling
  *           - assembled
  *           - error assembling
  *           - indexing
  *           - complete
  *           - error indexing
  *         required: false
  *         schema:
  *           $ref: '#/definitions/Document'
  *     responses:
  *       201:
  *         description: a Document object
  *         schema:
  *           $ref: "#/definitions/Document"
  */
  router.put('/documents/:id', celebrate({
    params: Joi.object().keys({
      id: Joi.string().required()
    }),
    body: Joi.object().keys({
      phase: Joi.string().valid(['retrieve', 'parse', 'assemble', 'index', 'complete']).optional(),
      status: Joi.string().valid(['downloading', 'uploaded', 'parsing', 'parsed', 'assembling', 'assembled', 'indexing', 'complete', 'error retrieving', 'error parsing', 'error assembling', 'error indexing']).optional(),
      fileName: Joi.string().optional(),
      publicUrl: Joi.string().optional()
    })
  }),(req, res, next) => {
    documentsController.updateOne(req, res, next);
  });
};

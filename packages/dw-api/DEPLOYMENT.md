## Heroku
Heroku uses [Yarn](https://yarnpkg.com) to install and run npm scripts by default (if it detects a `yarn.lock` file). Unfortunately Yarn has an [outstanding issue](https://github.com/yarnpkg/yarn/issues/761) that makes it hard to build production using it, so we'll need to use `npm` instead.

Create a `.slugignore` file at the root of your repo and add `yarn.lock`:

```sh
touch .slugignore
echo 'yarn.lock' >> .slugignore
```

Then check the file into git. This will instruct Heroku to use `npm` for all builds and commands ([relevant documentation](https://devcenter.heroku.com/articles/nodejs-support#build-behavior)).


Relevant documentation on Heroku: [https://devcenter.heroku.com/articles/getting-started-with-nodejs](https://devcenter.heroku.com/articles/getting-started-with-nodejs)
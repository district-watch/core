# District Watch API

This is the node api responsible for :

- scraping/crawling content sources, queueing up their indexing as tasks in a queue, and inserting into the database and search engine
- querying and posting data to the database for client applications that users use

### Features:

- [Express.js](https://expressjs.com/) as the web framework.
- Automatic polyfill requires based on environment with [babel-preset-env](https://github.com/babel/babel-preset-env).
- Linting with [ESLint](http://eslint.org/).
- Testing with [Jest](https://facebook.github.io/jest/).
- [Quick deployment guide](DEPLOYMENT.md) for Heroku.

## Develop

Begin development server on localhost:

```sh
yarn dev
```

This will launch a [nodemon](https://nodemon.io/) process for automatic server restarts when your code changes.

### Testing

Testing is powered by [Jest](https://facebook.github.io/jest/). This project also uses [supertest](https://github.com/visionmedia/supertest) for routing smoke testing.

Start the test runner in watch mode with:

```sh
yarn test-watch
```

You can also generate coverage with:

```sh
yarn test-coverage
```

Or run a single time with:

```sh
yarn test
```

### Linting

```sh
yarn lint
```

### Deployment and Release

Deployment is specific to hosting platform/provider but generally:

```sh
yarn compile
```

will compile your `/src` into `/dist`, and

```sh
yarn start
```

will run `compile` (via the `prestart` hook) and start the compiled application from the `/dist` folder. You can find small guides for Heroku in [the deployment](DEPLOYMENT.md) document.

## FAQ

**Why are you using `babel-register` instead of `babel-node`?**

`babel-node` contains a small "trap", it loads Babel's [polyfill](https://babeljs.io/docs/usage/polyfill/) by default. This means that if you use something that needs to be polyfilled, it'll work just fine in development (because `babel-node` polyfills it automatically) but it'll break in production because it needs to be explicitely included in Babel's CLI which handles the final build.

In order to avoid such confusions, `babel-register` is a more sensible approach in keeping the development and production runtimes equal. By using [babel-preset-env](https://github.com/babel/babel-preset-env) only code that's not supported by the running environment is transpiled and any polyfills required are automatically inserted.

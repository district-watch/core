# District Watch

Track municipal elected official voting record and bill search.

## Overview

This code repository is a [monorepo](https://medium.com/@maoberlehner/monorepos-in-the-wild-33c6eb246cb9) for all the related District Watch packages and services. Each package is living in their own space in `/packages` and is independently built, tested, linted, and deployable. The tool [Lerna](https://github.com/lerna/lerna) is being used to organize and manage these packages living together but separately.

## Packages

dw-search
dw-parser
dw-tools
dw-utils
dw-cloud-functions
dw-data
dw-queue
dw-website
dw-api
dw-logger
dw-courier

## Install

1. Clone the repo
2. From the root of the project run `npm run install`. **Do not run `npm install` in the subdirectories, this causes issues with Lerna's symlinking**

#### To run tests

`npm run test` (running from the root will run against all packages, from a specific package just runs it's own tests)

#### To run lint

`npm run lint` (running from the root will run against all packages, from a specific package just runs it's own linting)

#### Adding an external dependency (public package)

Say you are trying to add lodash to an app called fruit. Run `lerna add --scope fruit lodash`. That will add lodash as a dependency to just the 'fruit' package. To add lodash to all packages, run `lerna add lodash`.

**Remember** after installing a new package this way, you must do another `npm run clean-lerna` and then `npm run install` to ensure that lerna bootstraps the new dependency and hoists it correctly.

#### Update package version

`npm run update-version`

> Note: you may get prompted to pick a new version for untouched packages. This should not happen.

## Working in a package/project

Each project/package can be found inside of `/packages`. Here are some of the commands to use during development:

```javascript
"clean-lerna": "lerna clean --yes && rimraf ./packages/**/dist && rimraf ./packages/**/node_modules && rimraf ./packages/**/package-lock.json && rimraf ./packages/**/yarn.lock && rimraf ./package-lock.json && rimraf ./yarn.lock rimraf ./node_modules",
"install": "lerna exec yarn && npm run hoist && npm run compile",
"hoist": "lerna bootstrap --hoist",
"compile": "lerna run compile",
"clean": "lerna run clean",
"lint": "lerna run lint",
"test": "lerna run test",
"update-version": "lerna publish --skip-git --skip-npm --force-publish '*'",
"docs": "rm -fr pages; mkdir -p pages; ./node_modules/.bin/jsdoc --pedantic -c ./conf.json -d pages  --readme ./README.md --package ./package.json packages/**/src/*.js"
```

As you can see these all call the `task` file from the `root/scripts/task` location. Review that shell script to see what each of these does. The goal is a uniform set of tasks and configuration for any new project.

## Deployment

Currently, the gitlab CI runner is watching for the presence of a regexp-findable commit message. The format is like:
`some comment message and [ci job: dw-logger dw-data dw-parser-api]`. What that does is deploy.

### Notes and FAQ:

- Do **NOT** do `npm install` inside the managed package directories. [See notes here...](https://github.com/lerna/lerna/issues/1229)
- Had to use chmod to make the shell scripts executable on mac. `chmod u+x ./scripts/task.sh`.
- `npm run clean-lerna` will nuke all node_modules, /dist, package-lock.json, yarn.lock files in all managed packages and the root. This is essentially the "get me totally clean" task

#### API documentation

There is api documentation based on [swagger-jsdoc](https://github.com/Surnet/swagger-jsdoc) ([jsdoc](https://github.com/jsdoc3/jsdoc) comments on javascript code), and rendered using [ReDoc](https://github.com/Rebilly/ReDoc). Currently, you need to be running locally and then visit `http://localhost:8080/api/v1/docs` to view it. As you update/add route definitions, please ensure you keep the inline swagger comments up to date so that documentation isn't out of sync with the implementation.

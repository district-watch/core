<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>JSDoc: Source: dw-data/src/firebase.js</title>

    <script src="scripts/prettify/prettify.js"> </script>
    <script src="scripts/prettify/lang-css.js"> </script>
    <!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link type="text/css" rel="stylesheet" href="styles/prettify-tomorrow.css">
    <link type="text/css" rel="stylesheet" href="styles/jsdoc-default.css">
</head>

<body>

<div id="main">

    <h1 class="page-title">Source: dw-data/src/firebase.js</h1>

    



    
    <section>
        <article>
            <pre class="prettyprint source linenums"><code>import * as admin from 'firebase-admin';
import { Logger } from 'dw-logger';
import { Utils } from 'dw-utils';
const utils = new Utils();

/**
 * The firebase adapter class handles creation of the firebase cloud datastore connection.
 */
export class Firebase {
  /**
   * @param { Object } options configuration object for the firebease connection.
   * @param { string } [options.jsonFilePath] local path to a json file (the firebase project security key)
   * @param { object } [options.credential]  the representation of a firebase project security key (for environment variables)
   * @param { string } options.databaseUrl  the url to the firebase cloud database
   * @throws Will throw if no object is passed to the constructor.
   */
  constructor({
    jsonFilePath,
    credential,
    databaseUrl
  } = {}) {
    if (arguments.length === 0) {
      throw new Error('dw-data: constructor requires an options object with credential, databaseUrl, and optional jsonFilePath.');
    } else {
      this.jsonFilePath = this.validateFile(jsonFilePath);
      this.credential = this.validateFirebaseCredential(credential);
      this.databaseUrl = this.validateDatabaseUrl(databaseUrl);
      this._admin = admin;
      this._rawDocsStorage = null;
      this._jsonDocsStorage = null;
      this.db = this.createDatabase();
      this.logger = new Logger();
    }
  }

  /** Create the database connection from either json file or credential object.
   * @returns {Object} firebase admin object where firebase SDK commands can be executed.
   */
  createDatabase() {
    if (this.jsonFilePath) {
      const credentialFromFile = JSON.parse(require('fs').readFileSync(this.jsonFilePath, 'utf8'));
      this.createAdminInstance(credentialFromFile, this.databaseUrl);
    } else {
      this.createAdminInstance(this.credential, this.databaseUrl);
    }
    const firestore = admin.firestore();
    firestore.settings({timestampsInSnapshots: true});
    return firestore;
  }

  /** Uses Firebase API to instantiate the database instance and storage buckets.
   * @param { object } credential credential object for the connection.
   * @param { string } url url to the firebase database.
   */
  createAdminInstance(credential, url) {
    let storageDomain = null;
    if (this.jsonFilePath) {
      storageDomain = `district-watch-dev`;
      admin.initializeApp({
        credential: admin.credential.cert({
          project_id: credential.project_id,
          client_email: credential.client_email,
          private_key: credential.private_key
        }),
        databaseURL: url
      });
    } else {
      storageDomain = `district-watch-prod`;
      admin.initializeApp({
        credential: credential,
        databaseURL: url
      });
    }
    const storage = this._admin.storage();
    this._rawDocsStorage = storage.bucket(`gs://${storageDomain}-raw-docs`);
    this._jsonDocsStorage = storage.bucket(`gs://${storageDomain}-json-docs`);
  }

  /** Get all items of firebase collection.
   * @param {string} collection The name of the collection to get.
   * @throws Will throw an error if collection is not a string or firebase SDK experiences some issue.
   * @returns {Promise} Promise object resolves with array of objects or rejects with error message.
   */
  async getCollection(collection) {
    utils.validateString('dw-data', collection, 'getCollection', 'collection');
    try {
      const results = [];
      const snapshot = await this.db.collection(collection).get();
      snapshot.forEach(doc => {
        results.push(doc.data());
      });
      return results;
    } catch (error) {
      this.logger.log('debug', 'firebase > getCollection() threw', error);
      throw new Error(`dw-data: failed trying to get by collection name: ${error}`);
    }
  }

  /** Upload a file to storage bucket via remote url
   * @param {string} url The url to the file.
   * @param {string} fileName the filename with extension of the file to upload, only pdf, docx, doc, or txt allowed.
   * @param {string} bucketName The name of the bucket to upload to, must be 'raw' or 'json'.
   * @throws Will throw an error url is not the correct data type or there is an error in the firebase sdk/infrastructure.
   * @returns {Promise} Promise object resolves with an object of file data or rejects with error message.
   */
  async uploadToBucket(url, fileName, bucketName) {
    if (!utils.isValidUrl(url)) {
      throw new Error('dw-data: uploadToBucket() method needs a valid url for the url param.');
    }
    if (!utils.isValidUploadFileType(fileName)) {
      throw new Error('dw-data: uploadToBucket() method needs a valid file type in order to upload: pdf, docx, doc, or txt');
    }
    utils.validateBucketName('dw-data', bucketName, 'uploadToBucket');
    try {
      if (bucketName === 'raw') {
        return await this._rawDocsStorage.upload(url, { destination: fileName });
      } else if (bucketName === 'json') {
        return await this._jsonDocsStorage.upload(url, { destination: fileName });
      }
    } catch (error) {
      this.logger.log('debug', 'firebase > uploadToBucket() threw', error);
      throw new Error(`dw-data: failed trying to upload to bucket: ${error}`);
    }
  }


  /** Get item by ID from a firebase collection.
   * @param {string} collection The name of the collection to get.
   * @param {string} id The id of the item to get.
   * @throws Will throw an error if collection is not a string or firebase SDK experiences some issue.
   * @returns {Promise} Promise object resolves with object or rejects with error message.
   */
  async getById(collection, id) {
    utils.validateString('dw-data', collection, 'getById', 'collection');
    utils.validateString('dw-data', id, 'getById', 'id');
    try {
      return await this.db.collection(collection).doc(id).get().then(doc => doc.data());
    } catch (error) {
      this.logger.log('debug', 'firebase > getById() threw', error);
      throw new Error(`dw-data: failed trying to get by ID: ${error}`);
    }
  }

  /** Create item in a firebase collection
   * @param {string} collection The name of the collection to create item in.
   * @param {object} data An object representing the item to create.
   * @param {string} [id] Specify an id. Default will auto-create one for you if not specified.
   * @throws Will throw an error if collection is not a string, data is not an object, optional id is not a string, or firebase SDK experiences some issue.
   * @returns {Promise} Promise object resolves with new created object or rejects with error message.
   */
  async createOne(collection, data, id) {
    utils.validateString('dw-data', collection, 'createOne', 'collection');
    utils.validateObject('dw-data', data, 'createOne', 'data');
    try {
      if (id &amp;&amp; utils.validateString('dw-data', id, 'createOne', 'id')) {
        return await this.setWithId(collection, data, id);
      } else {
        return await this.setWithoutId(collection, data);
      }
    } catch(error) {
      this.logger.log('debug', 'firebase > createOne() threw', error);
      throw new Error(`dw-data: createOne() received an error from firebase: ${error}`);
    }
  }

  /** Set item into firebase collection with known id
   * @param {string} collection The name of the collection to create item in.
   * @param {object} data An object representing the item to create.
   * @param {string} id Specify an id.  Will be set as both key and an id property of the object.
   * @throws Will throw an error if collection is not a string, data is not an object, id is not a string, or firebase SDK experiences some issue.
   * @returns {Promise} Promise object resolves with new created object or rejects with error message.
   */
  async setWithId(collection, data, id) {
    utils.validateString('dw-data', collection, 'setWithId', 'collection');
    utils.validateObject('dw-data', data, 'setWithId', 'data');
    utils.validateString('dw-data', id, 'setWithId', 'id');
    try {
      await this.db.collection(collection).doc(id).set(data);
      return await this.updateOne(collection, { id }, id);
    } catch(error) {
      this.logger.log('debug', 'firebase > setWithId() threw', error);
      throw error;
    }
  }

  /** Set item into firebase collection, generating new ID
   * @param {string} collection The name of the collection to create item in.
   * @param {object} data An object representing the item to create.
   * @throws Will throw an error if collection is not a string, data is not an object, or firebase SDK experiences some issue.
   * @returns {Promise} Promise object resolves with new created object or rejects with error message.
   */
  async setWithoutId(collection, data) {
    utils.validateString('dw-data', collection, 'setWithoutId', 'collection');
    utils.validateObject('dw-data', data, 'setWithoutId', 'data');
    try {
      const docRef = await this.db.collection(collection).add(data);
      return await this.updateOne(collection, { id: docRef.id }, docRef.id);
    } catch(error) {
      this.logger.log('debug', 'firebase > setWithoutId() threw', error);
      throw error;
    }
  }

  /** Download a file from storage bucket via fileName and bucketName
   * @param {string} fileName The name of the file to download.
   * @param {string} bucketName The name of the bucket to download from, must be 'raw' or 'json'.
   * @throws Will throw an error if params are not the correct data type or there is an error in the firebase sdk/infrastructure.
   * @returns {Promise} Promise object resolves with an object of file data or rejects with error message.
   */
  async downloadFromBucket(fileName, bucketName) {
    if (!utils.isValidDownloadFileType(fileName)) {
      throw new Error('dw-data: downloadFromBucket() method needs a valid file type in order to download: json');
    }
    utils.validateBucketName('dw-data', bucketName, 'downloadFromBucket');
    try {
      const storageRef = bucketName === 'raw' ? this._rawDocsStorage : this._jsonDocsStorage;
      const data = await storageRef.file(fileName).download();
      if (data) {
        const utfData = await data.toString("utf-8");
        if (utfData) {
          return utfData;
        } else {
          this.logger.log(`firebase > downloadFromBucket() could not convert data to utf-8 for ${fileName}`);
        }
      } else {
        this.logger.log(`firebase > downloadFromBucket() found no data found for ${fileName}`);
      }
    } catch (error) {
      throw new Error(`dw-data: failed trying to download ${fileName} from bucket: ${error}`);
    }
  }

  /** Get a list of all files in a storage bucket by name.
   * @param {string} bucketName The name of the bucket.
   * @throws Will throw an error if bucketName is not the correct data type or there is an error in the firebase sdk/infrastructure.
   * @returns {Promise} Promise object resolves with an object of file data or rejects with error message.
   */
  async listFilesFromBucket(bucketName) {
    utils.validateBucketName('dw-data', bucketName, 'listFilesFromBucket');
    try {
      const storageRef = bucketName === 'raw' ? this._rawDocsStorage : this._jsonDocsStorage;
      const [files] = await storageRef.getFiles();
      return files;
    } catch (error) {
      throw new Error(`dw-data: failed trying to getFiles from bucket: ${error}`);
    }
  }

  /** Updates a firebase item
   * @param {string} collection The name of the collection.
   * @param {string} id Specify an id of item to update.
   * @throws Will throw an error if collection is not a string, id is not a string, or firebase SDK experiences some issue.
   * @returns {Promise} Promise object resolves with new created object or rejects with error message.
   */
  async updateOne(collection, data, id) {
    utils.validateString('dw-data', collection, 'updateOne', 'collection');
    utils.validateObject('dw-data', data, 'updateOne', 'data');
    utils.validateString('dw-data', id, 'updateOne', 'id');
    try {
      await this.db.collection(collection).doc(id).update(data);
      return await this.getById(collection, id);
    } catch(error) {
      this.logger.log('debug', 'firebase > updateOne() threw', error);
      throw error;
    }
  }

  /** Deletes a single firebase item
   * @param {string} collection The name of the collection.
   * @param {string} id Specify an id of item to delete.
   * @throws Will throw an error if collection is not a string, id is not a string, or firebase SDK experiences some issue.
   * @returns {Promise} Promise object resolves with a confirmation string or rejects with error message.
   */
  async deleteOne(collection, id) {
    utils.validateString('dw-data', collection, 'deleteOne', 'collection');
    utils.validateString('dw-data', id, 'deleteOne', 'id');
    try {
      return await this.db.collection(collection).doc(id).delete();
    } catch(error) {
      this.logger.log('debug', 'firebase > deleteOne() threw', error);
      throw error;
    }
  }

  /** Queries a firebase collection, optionally by a specified filter
   * @param {string} collection The name of the collection to query.
   * @param { Object[] } [filter] An optional array of objects describing the queries to make to get a subsection of a collection.
   * @param { string } filter[].propertyName The property name to query by.
   * @param { string } filter[].operand The operand to query by, only supports: '==', '>=', '&lt;='.
   * @param { string } filter[].propertyValue The property value for the propertyName.
   * @throws Will throw an error if collection is not a string, filter param is malformed, or firebase SDK experiences some issue.
   * @returns {Promise} Promise object resolves with results or rejects with error message.
   */
  async query(collection, filter) {
    utils.validateString('dw-data', collection, 'query', 'collection');
    try {
      if (filter &amp;&amp; utils.validateFilter('dw-data', filter, 'query', 'filter')) {
        let query = this.db.collection(collection);
        filter.forEach(filter => {
          query = query.where(filter.propertyName, filter.operand, filter.propertyValue);
        });
        return await query.get().then(snapshot => {
          const hits = [];
          snapshot.forEach(doc => {
            hits.push(doc.data());
          });
          return hits;
        });
      } else {
        return await this.getCollection(collection);
      }
    } catch(error) {
      this.logger.log('debug', 'firebase > query() threw', error);
      throw error;
    }
  }

  /** Validates that the url to database is a valid string.
   * @param { string } url url to the firebase database.
   * @throws Will throw if url is not valid.
   * @returns { string } valid url string.
   */
  validateDatabaseUrl(url) {
    utils.validateString('dw-data', url, 'validateDatabaseUrl', 'url');
    if (utils.isValidUrl(url)) {
      return url;
    }
    throw new Error('dw-data: not a valid databaseUrl to a firebase database.');
  }

  /** Validates that the credential of a firebase cert is valid.
   * @param { Object } cert firebase security key credential object
   * @param { string } cert.projectId string of the firebase project id.
   * @param { string } cert.clientEmail string of the firebase project client email address.
   * @param { string } cert.privateKey string of the firebase project private key.
   * @throws Will throw if cert is not valid.
   * @returns { object } valid credential object.
   */
  validateFirebaseCredential(cert) {
    if (cert &amp;&amp; !this.jsonFilePath) {
      const validCert = admin.credential.cert({
        project_id: cert.project_id,
        client_email: cert.client_email,
        private_key: cert.private_key
      });
      if (validCert) {
        return validCert;
      }
      throw new Error('dw-data: firebase credential is not valid.');
    }
  }

  /** Validates that the jsonFilePath is a valid path on the local file system.
   * @param { string } path local path to the file.
   * @throws Will throw if path is undefined, not a valid string, or file is not found at specified local path.
   * @returns { string } valid local file path.
   */
  validateFile(path) {
    if (!path) {
      return;
    } else if (typeof path !== 'string') {
      throw new Error('dw-data: jsonFilePath must be a string.');
    } else {
      if (utils.isValidFilePath(path)) {
        return path;
      } else {
        throw new Error('dw-data: jsonFilePath must be a valid file path.');
      }
    }
  }
}</code></pre>
        </article>
    </section>




</div>

<nav>
    <h2><a href="index.html">Home</a></h2><h3>Modules</h3><ul><li><a href="module-dw-data.html">dw-data</a></li><li><a href="module-dw-logger.html">dw-logger</a></li><li><a href="module-dw-parser.html">dw-parser</a></li><li><a href="module-dw-queue.html">dw-queue</a></li><li><a href="module-dw-search.html">dw-search</a></li><li><a href="module-dw-utils.html">dw-utils</a></li></ul><h3>Classes</h3><ul><li><a href="Elasticsearch.html">Elasticsearch</a></li><li><a href="Firebase.html">Firebase</a></li><li><a href="module-dw-data.DatabaseWrapper.html">DatabaseWrapper</a></li><li><a href="module-dw-logger.Logger.html">Logger</a></li><li><a href="module-dw-parser.Parser.html">Parser</a></li><li><a href="module-dw-queue.Queue.html">Queue</a></li><li><a href="module-dw-search.Search.html">Search</a></li><li><a href="module-dw-utils.Utils.html">Utils</a></li></ul>
</nav>

<br class="clear">

<footer>
    Documentation generated by <a href="https://github.com/jsdoc3/jsdoc">JSDoc 3.5.5</a> on Mon Oct 29 2018 21:25:35 GMT-0500 (CDT)
</footer>

<script> prettyPrint(); </script>
<script src="scripts/linenumber.js"> </script>
</body>
</html>
